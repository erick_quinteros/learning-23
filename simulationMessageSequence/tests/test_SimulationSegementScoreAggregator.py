import unittest
from unittest.mock import Mock
from unittest.mock import patch
import os
import sys
import pandas as pd

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from common.unitTest.pyunittestUtils import unitTestParent
from simulationMessageSequence.SimulationSegementScoreAggregator import SimulationScoreAggregator
from common.DataAccessLayer.DataAccessLayer import *


class TestSimulationScoreAggregator(unitTestParent):

    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.logging")
    def test__init__(self, mock_logging):
        #function inputs
        sim_run_uid = "unit-test"
        mso_build_uid = "unit-test"
        simulation_logger_name = "SIMULATION UNIT TEST"

        #expected outputs
        referencesim_run_uid = sim_run_uid
        referencemso_build_uid = mso_build_uid

        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator(sim_run_uid, mso_build_uid, simulation_logger_name)
        self.assertEqual(referencesim_run_uid, SimulationScoreAggregatorInstance.sim_run_uid)
        self.assertEqual(referencemso_build_uid, SimulationScoreAggregatorInstance.mso_build_uid)

    # @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    # @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    # @patch("simulationMessageSequence.SimulationSegementScoreAggregator.logging")
    # @patch("simulationMessageSequence.SimulationSegementScoreAggregator.any")
    # def test__aggregate_AccountUIDfalse(self, mock_len, mock_any, mock_MessageAccessor, mock_SegmentAccessor):
    #     #tests for when the dataframe does not contain the account UID
    #     #function inputs
    #     # mock_SegmentAccessor().get_accounts_for_segment_uid.return_value = []
    #     # mock_any.return_value = False
    #     mock_SegmentAccessor().get_accounts_for_segment_uid.return_value = ["unit-test"]
    #     account_message_seq_df = {"probability" : [0,1], "messageSeqUID": ["unit-test1", "unit-test2"], "accountUID": ["unit-test1", "unit-test2"]}
    #     mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=account_message_seq_df)
    #     mock_any.return_value = False

    #     #expected outputs
    #     #testing outputs
    #     SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
    #     SimulationScoreAggregatorInstance._SimulationScoreAggregator__aggregate_account_scores_for_segment("test")
    #     mock_SegmentAccessor().write_simulation_segment_result.assert_called_with("unit-test", "unit-test", "test", "unit-test2", 0.5)

    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.any")
    def test__aggregate_AccountUIDtrue_mmessageUIDfalse(self, mock_any, mock_MessageAccessor, mock_SegmentAccessor):
        #tests for when the dataframe does contain the account UID
        #but "if message_seq_uid" returns false
        #function inputs
        mock_SegmentAccessor().get_accounts_for_segment_uid.return_value = ["unit-test1"]
        account_message_seq_df = {"probability" : [0,1], "messageSeqUID": ["unit-test1", "unit-test2"], "accountUID": ["unit-test1", "unit-test2"]}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=account_message_seq_df)
        mock_any.return_value = False

        #expected outputs
        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        SimulationScoreAggregatorInstance._SimulationScoreAggregator__aggregate_account_scores_for_segment("test")
        mock_SegmentAccessor().write_simulation_segment_result.assert_called()

    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.any")
    def test__aggregate_AccountUIDtrue_mmessageUIDtrue_maxScoreGreater(self, mock_any, mock_MessageAccessor, mock_SegmentAccessor):
        #tests for when the dataframe does contain the account UID
        #but "if message_seq_uid" returns True
        #And "max_score < score" returns true
        #function inputs
        mock_SegmentAccessor().get_accounts_for_segment_uid.return_value = ["unit-test1"]
        account_message_seq_df = {"probability" : [-2,-2], "messageSeqUID": ["unit-test1", "unit-test2"], "accountUID": ["unit-test1", "unit-test2"]}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=account_message_seq_df)
        mock_any.return_value = True

        #expected outputs
        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        SimulationScoreAggregatorInstance._SimulationScoreAggregator__aggregate_account_scores_for_segment("test")
        mock_SegmentAccessor().write_simulation_segment_result.assert_called_with("unit-test", "unit-test", "test", "unit-test1", -4.0)


    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.any")
    def test__aggregate_AccountUIDtrue_mmessageUIDtrue_maxScoreLess(self, mock_any, mock_MessageAccessor, mock_SegmentAccessor):
        #tests for when the dataframe does contain the account UID
        #but "if message_seq_uid" returns true
        #And "max_score < score" returns false (max_score > score)
        #function inputs
        mock_SegmentAccessor().get_accounts_for_segment_uid.return_value = ["unit-test1"]
        account_message_seq_df = {"probability" : [0,1], "messageSeqUID": ["unit-test1", "unit-test2"], "accountUID": ["unit-test1", "unit-test2"]}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=account_message_seq_df)
        mock_any.return_value = True

        #expected outputs
        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        SimulationScoreAggregatorInstance._SimulationScoreAggregator__aggregate_account_scores_for_segment("test")
        mock_SegmentAccessor().write_simulation_segment_result.assert_called_with("unit-test", "unit-test", "test", "unit-test1", 0.5)

    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.logging")
    def test_calculate_segment_segmentUIDfalse(self, mock_logging, mock_SegmentAccessor):
        #tests for when no segment_uids exist
        #function inputs
        mock_SegmentAccessor().get_segment_uids.return_value = False
        #expected outputs
        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        SimulationScoreAggregatorInstance.calculate_segment_message_sequence()
        mock_SegmentAccessor.assert_called()
        mock_SegmentAccessor().get_segment_uids.assert_called()

    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    def test_calculate_segment_segmentUIDtrue_exceptException(self, mock_MessageAccessor, mock_SegmentAccessor):
        #tests for when no segment_uids exist
        #but exception Exception raised
        #function inputs
        d = {"accountUID": ["unit-test"], "messageSeqUID": ["mess1"], "probability": ["0.5"]}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=d)
        mock_SegmentAccessor().get_segment_uids.return_value = ["unit-test1", "unit-test2"]
        d = {"segmentUID": ["unit-test1"], "accountUID": ["unit-test"]}
        mock_SegmentAccessor().get_account_segment_df.return_value = pd.DataFrame(data=d)

        #expected outputs
        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        with self.assertLogs(logger, level="ERROR") as logOut:
            SimulationScoreAggregatorInstance.calculate_segment_message_sequence()
        errorStr = "ERROR:SIMULATION UNIT TEST:Exception while aggregating segment sequences- reduction operation 'argmax' not allowed for this dtype"
        self.assertIn(errorStr, logOut.output)


    # # **** incomplete test case
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    def test_calculate_segment_segmentUIDtrue_exceptValueError(self, mock_MessageAccessor, mock_SegmentAccessor):
    #     #tests for when no segment_uids exist
    #     #but exception ValueError raised
    #     #function inputs
        d = {"accountUID": ["unit-test-none1", "unit-test-none2"], "messageSeqUID": ["mess1", "mess2"], "probability": [0.5, 0.3]}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=d)
        mock_SegmentAccessor().get_segment_uids.return_value = ["unit-test1", "unit-test2"]
        d = {"segmentUID": ["unit-test1", "unit-test2"], "accountUID": ["unit-test", "unit-test"]}
        mock_SegmentAccessor().get_account_segment_df.return_value = pd.DataFrame(data=d)
    #     # **** need to force one of the functions to return ValueError to check that exception
    #
    #     #expected outputs
    #     #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        with self.assertLogs(logger, level="ERROR") as logOut:
            SimulationScoreAggregatorInstance.calculate_segment_message_sequence()
    #     # **** update the line below for the error message for ValueError
        errorStr = "ERROR:SIMULATION UNIT TEST:Value error while aggregating segment sequences- attempt to get argmax of an empty sequence"
        self.assertIn(errorStr, logOut.output)

    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    # @patch("common.DataAccessLayer.DataAccessLayer.pd.Series.empty")
    def test_calculate_segment_segmentUIDtrue_try_accountUIDempty(self, mock_MessageAccessor, mock_SegmentAccessor):
        #tests for when no segment_uids exist
        #and "try" case runs
        #but account_uids is empty
        #function inputs
        d = {"accountUID": ["unit-test"], "messageSeqUID": ["mess1"], "probability": ["0.5"]}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=d)
        mock_SegmentAccessor().get_segment_uids.return_value = ["unit-test1"]
        d = {"segmentUID": ["wrong-segUID"], "accountUID": ["unit-test"]}
        mock_SegmentAccessor().get_account_segment_df.return_value = pd.DataFrame(data=d)
        # mock_empty.return_value = False

        #expected outputs
        referenceLog = ['INFO:SIMULATION UNIT TEST:Aggregating Account Scores for Segments...', 'INFO:SIMULATION UNIT TEST:Fetched Account Message Sequence Dataframe', 'INFO:SIMULATION UNIT TEST:Fetching Segment-Account Dataframe', 'INFO:SIMULATION UNIT TEST:Aggregating for Segment- unit-test1', 'INFO:SIMULATION UNIT TEST:Aggregating Account Scores for Segment Completed!']

        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        with self.assertLogs(logger, level="INFO") as logOut:
            SimulationScoreAggregatorInstance.calculate_segment_message_sequence()
        self.assertEqual(referenceLog, logOut.output)

    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    @patch("common.DataAccessLayer.DataAccessLayer.pd.Series.mean")
    @patch("common.DataAccessLayer.DataAccessLayer.pd.Series.loc")
    def test_calculate_segment_segmentUIDtrue_try_accountUIDfull_accountSeqDFempty(self, mock_loc, mock_mean, mock_MessageAccessor, mock_SegmentAccessor):
        #tests for when no segment_uids exist
        #and "try" case runs
        #and account_uids is populated
        #but account_message_seq_df is empty
        #function inputs
        mock_SegmentAccessor().get_segment_uids.return_value = ["unit-test1"]
        account_message_seq_df = {"accountUID": [], "messageSeqUID": [], "probability": []}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=account_message_seq_df)
        account_uids_df = {"segmentUID": ["unit-test1"], "accountUID": ["unit-test"]}
        mock_SegmentAccessor().get_account_segment_df.return_value = pd.DataFrame(data=account_uids_df)
        # mock_empty.return_value = False

        #expected outputs
        referenceLog = ['INFO:SIMULATION UNIT TEST:Aggregating Account Scores for Segments...', 'INFO:SIMULATION UNIT TEST:Fetched Account Message Sequence Dataframe', 'INFO:SIMULATION UNIT TEST:Fetching Segment-Account Dataframe', 'INFO:SIMULATION UNIT TEST:Aggregating for Segment- unit-test1', 'INFO:SIMULATION UNIT TEST:Aggregating Account Scores for Segment Completed!']

        #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        with self.assertLogs(logger, level="INFO") as logOut:
            SimulationScoreAggregatorInstance.calculate_segment_message_sequence()
        self.assertEqual(referenceLog, logOut.output)
        assert not mock_mean.called
        assert not mock_loc.called

    # **** incomplete test case
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.SegmentAccessor")
    @patch("simulationMessageSequence.SimulationSegementScoreAggregator.MessageAccessor")
    def test_calculate_segment_segmentUIDtrue_try_accountUIDfull_accountSeqDFfull(self, mock_MessageAccessor, mock_SegmentAccessor):
    #     #tests for when no segment_uids exist
    #     #and "try" case runs
    #     #and account_uids is populated
    #     #and account_message_seq_df is populated
    #     #function inputs
        mock_SegmentAccessor().get_segment_uids.return_value = ["unit-test1"] #segment_uids
        account_message_seq_df = {"accountUID": ["unit-test"], "messageSeqUID": ["mess1"], "probability": [0.5]}
        mock_MessageAccessor().get_finalized_sequence_and_score_for_accounts.return_value = pd.DataFrame(data=account_message_seq_df)
        account_uids_df = {"segmentUID": ["unit-test1", "unit-test2"], "accountUID": ["unit-test", "unit-test"]}
        mock_SegmentAccessor().get_account_segment_df.return_value = pd.DataFrame(data=account_uids_df)
    #
    #     #expected outputs
        referenceLog = ['INFO:SIMULATION UNIT TEST:Aggregating Account Scores for Segments...', 'INFO:SIMULATION UNIT TEST:Fetched Account Message Sequence Dataframe', 'INFO:SIMULATION UNIT TEST:Fetching Segment-Account Dataframe','INFO:SIMULATION UNIT TEST:Aggregating for Segment- unit-test1','INFO:SIMULATION UNIT TEST:Aggregating Account Scores for Segment Completed!']
    #
    #     #testing outputs
        SimulationScoreAggregatorInstance = SimulationScoreAggregator("unit-test", "unit-test", "SIMULATION UNIT TEST")
        with self.assertLogs(logger, level="INFO") as logOut:
            SimulationScoreAggregatorInstance.calculate_segment_message_sequence()
        self.assertEqual(referenceLog, logOut.output)
