package LearningSparkExtension

import org.apache.spark.sql
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.countDistinct
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.DataFrame
import org.apache.spark.ml.feature.QuantileDiscretizer
import org.apache.spark.sql.types
import org.apache.spark.sql.DataFrameNaFunctions


object DataWrangler {

  def dropColumnsWithLessThanUnqiueValues(dataFrame: DataFrame, uniqueThreshold: Int) : DataFrame = {

      val uniqueValueCountDf = dataFrame.select(dataFrame.columns.map(c => countDistinct(col(c)).alias(c)): _*);
      var updatedDataFrame = dataFrame
      var valueCount: Long = 0
      for (column <- uniqueValueCountDf.columns)
      {
        valueCount = uniqueValueCountDf.select(column).take(1)(0)(0).asInstanceOf[Long]

        if(uniqueThreshold > valueCount)
          {
              println("Deleting column: " + column + " with unique values: " + valueCount)
              updatedDataFrame = updatedDataFrame.drop(column)
          }
      }

      return (updatedDataFrame)
  }

  def createBinsForNumericalFeatures(dataFrame: DataFrame, uniqueValueThreshold: Int, lessBucketCount: Int, moreBucketCount: Int, binColSuffix: String) : DataFrame = {

        // Initialize Bucket Count
        var bucketCount = 1

        // Initialize updated dataframe to return
        var updated_df = dataFrame

        // Get the data types of the columns in the data frame to identify numeric columns
        var columnTypes = dataFrame.dtypes

        // List of numeric data type for the spark data frame
        val numericTypeList = List[java.lang.String](types.FloatType.toString, types.IntegerType.toString, types.LongType.toString, types.ShortType.toString, types.DoubleType.toString)

        // Iterate over columns in the data frame
        columnTypes.foreach{
            case(name, dtype) => {
               if(numericTypeList.contains(dtype))
                {
                    // Get unique value count for the column
                    val distinctValCount = dataFrame.select(name).distinct.count

                    // Generate new bins column name with appending "_bin" to the orignal column name
                    val resultBinColName = name + binColSuffix

                    // QuantileDiscretizer for creating buckets
                    val discretizer = new QuantileDiscretizer()
                                              .setInputCol(name)
                                              .setOutputCol(resultBinColName)
                                              .setRelativeError(0.05)

                    // Replacement map for bin float to bin name
                    var replacementMap = Map("" -> "")

                    // if distinct value is greater than quantile threshold create 4 buckets else 2
                    if(distinctValCount > quantileBucketUniqueValueThreshold)
                    {
                        // Set bucket count to more
                        bucketCount = moreBucketCount
                    }
                    else
                    {
                        // Set bucket count to less
                        bucketCount = lessBucketCount
                    }

                    // Set number of  buckets to discretizer
                    discretizer.setNumBuckets(bucketCount)

                    // Quantile values based on number of buckets
                    var quantileValues = BigDecimal(0.0).toDouble to BigDecimal(1.0).toDouble by BigDecimal(1.0/bucketCount).toDouble toArray

                    // Get the values in the array
                    var quantileArray  = dataFrame.stat.approxQuantile(name, quantileValues , 0.05)

                    // Iterate over the buckets to generate name replacement map for the buckets
                    for (index <- 0 to bucketCount-1)
                    {
                        println(s"Index = $index ")
                        var lowerBound = quantileArray(index)
                        var upperBound = quantileArray(index+1)
                        var replaceString = s"[$lowerBound, $upperBound)"
                        println(s"RString $replaceString")
                        replacementMap += (s"$index.0" -> replaceString)
                    }

                    // Create the buckets
                    updated_df = discretizer.fit(updated_df).transform(updated_df)

                    // Convert the bin column to string
                    updated_df = updated_df.withColumn(resultBinColName, col(resultBinColName).cast(sql.types.StringType))

                    // Replace bin values with quantile lower bound and upper bound indication
                    updated_df = updated_df.na.replace(resultBinColName, replacementMap.toMap)
                }
            }
        }

        return(updated_df)
  }

  def test_dropColumnsWithLessThanUnqiueValues(args: Array[String]): DataFrame = {
    // Testing the functions in learning extension
    println("Testing method - dropColumnsWithLessThanUnqiueValues")
	  val spark = SparkSession.builder.appName("TestLearningExtension").getOrCreate()
    val testDF = spark.read.option("header", true).csv("test/drop_columns_test.csv")
    val testResultDF = dropColumnsWithLessThanUnqiueValues(testDF, 3)
    testResultDF.show()
    spark.stop()
    return(testResultDF)
  }
}
