CREATE TABLE `TimeToEngageAlgorithm` (
  `tteAlgorithmId` int(11) NOT NULL AUTO_INCREMENT,
  `tteAlgorithmName` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `tteAlgorithmDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `externalId` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tteAlgorithmId`),
  UNIQUE KEY `TimeToEngageAlgorithm_unique_name` (`tteAlgorithmName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

