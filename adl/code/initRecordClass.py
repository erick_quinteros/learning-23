from awsglue.transforms import *

from pyspark.sql.functions import *
from utils import Utils
import logging
from pyspark.sql import Window
import pyspark
logger = None

# create a glue Context and connect that with spark
class InitRecordClass:
    def __init__(self, glue_context, customer, s3_destination, deltaTable):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session
        self.DeltaTable = deltaTable

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")
        pass

    def run(self, batch, upsetDate, s3_source):
        try:
            s3_recordclasses_destination = self.s3_destination + 'data/silver/recordclasses'
            values_pd = self.spark.read.csv(self.s3_destination + 'data/silver/recordclassCsv', header='true').toPandas()
            values = dict(zip(values_pd.recordcalssId, values_pd.recordclass))
            # print(values)
            df_emails = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/emails')
            df_visits = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/visits/')
            if not batch:
                df_emails = df_emails.filter(df_emails.updatedAt > upsetDate)
                df_visits = df_visits.filter(df_visits.updatedAt > upsetDate)
            productinteractiontype = self.utils.data_load_parquet(self.glueContext, s3_source + "productinteractiontype")
            product = df_emails.join(productinteractiontype, "productInteractionTypeId", "inner")\
                .groupBy("interactionId","productInteractionTypeId","productInteractionTypeName")\
                .agg(concat_ws(',', collect_list("emailMessageId")).alias("messageId"),
                             concat_ws(',', collect_list("emailMessageTopicId")).alias("messageTopicId"),
                             concat_ws(',', collect_list("messageReaction")).alias("messageReaction"),
                             concat_ws(',', collect_list("physicalMessageDesc")).alias("physicalMessageDesc"),
                             concat_ws(',', collect_list("physicalMessageUID")).alias("physicalMessageUID"),
                             concat_ws(',', collect_list("quantity")).alias("quantity"),
                             concat_ws(',', collect_list("productId")).alias("productIdList"),
                             concat_ws(',', collect_list("accountId")).alias("accountIdList"))\
                .union(
                (df_visits.join(productinteractiontype, "productInteractionTypeId", "inner")
                    .groupBy("interactionId","productInteractionTypeId","productInteractionTypeName")
                    .agg( concat_ws(',', collect_list("messageReaction")).alias("messageReaction"),
                             concat_ws(',', collect_list("physicalMessageUID")).alias("physicalMessageUID"),
                             concat_ws(',', collect_list("quantity")).alias("quantity"),
                             concat_ws(',', collect_list("productId")).alias("productIdList"),
                             concat_ws(',', collect_list("accountId")).alias("accountIdList")
                 ))
            .withColumn("messageId", lit("")).withColumn("messageTopicId", lit("")).withColumn("physicalMessageDesc", lit(""))
            .select("interactionId","productInteractionTypeId","productInteractionTypeName", "messageId"
                    , "messageTopicId", "messageReaction","physicalMessageDesc","physicalMessageUID","quantity","productIdList","accountIdList")
            )
            product.createOrReplaceTempView("product")
            logger.info("df_product_final cached")
            ########
            w1 = Window.partitionBy("interactionId", "repId").orderBy(desc("interactionmonth"))

            df_emails = df_emails.withColumn("filterDuplicates", row_number().over(w1)).filter(
                "filterDuplicates=1").select(["*"])
            df_emails = df_emails.drop(df_emails.filterDuplicates)
            df_emails.createOrReplaceTempView("emails")
            logger.info("df_emails cahched")

            df_visits = df_visits.withColumn("filterDuplicates", row_number().over(w1)).filter(
                "filterDuplicates=1").select(["*"])
            df_visits = df_visits.drop(df_visits.filterDuplicates)
            df_visits.createOrReplaceTempView("visits")
            logger.info("df_visits cached")
            # df_product_final = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/product_final')
            # df_product_final.createOrReplaceTempView("product")
            # logger.info("df_product_final cached")


            ###########


            df_suggestions = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/suggestions_final')
            if not batch: df_suggestions = df_suggestions.filter(df_suggestions.suggestedDate > upsetDate)
            df_suggestions.createOrReplaceTempView("suggestions")
            logger.info("df_suggestions cached")

            # df_dse = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/dse_score')
            # df_dse.createOrReplaceTempView("dse_score")
            # logger.info("df_dse cached")

            value_tablename = {}
            list_email = ['COALESCE(fs.suggestedyear,fe.interactionyear) as allEffectiveyear',
                          'COALESCE(fs.suggestedmonth,fe.interactionmonth) as allEffectivemonth',
                          'COALESCE(fs.suggestedday,fe.interactionday) as allEffectiveday',
                          "productIdList",
                          "accountIdList",
                          'fe.interactionId',
                          'fe.externalId',
                          'fe.timeZoneId as interactionTimeZoneId',
                          'fe.interactionyear',
                          'fe.interactionmonth',
                          'fe.interactionday',
                          'fe.startDateTime interactionStartDateTime',
                          'fe.startDateLocal as interactionStartDateLocal',
                          'fe.interactionTypeId',
                          'fe.interactionTypeName',
                          'fe.repActionTypeId',
                          'fe.repActionTypeName',
                          'fe.duration as interactionDuration',
                          'fe.wasCreatedFromSuggestion as interactionWasCreatedFromSuggestion',
                          'fe.isCompleted as interactionIsCompleted',
                          'fe.isDeleted as interactionIsDeleted',
                          'fe.createdAt as interactionCreatedAt',
                          'fe.updatedAt as interactionUpdatedAt',
                          'fe.repId as interactionRepId',
                          'fe.accountId as interactionAccountId',
                          'fe.facilityId as interactionFacilityId',
                          'fe.interactionAccountIsdeleted',
                          'fe.rawCsId',
                          'fe.emailSentDate as emailEmailSentDate',
                          'fe.captureDatetime as emailCaptureDatetime',
                          'fe.accountuid as emailAccountuid',
                          'fe.accountEmail as emailAccountEmail',
                          'fe.emailConfigValues as emailEmailConfigValues',
                          'fe.emailSubject as emailEmailSubject',
                          'fe.emailContent as emailEmailContent',
                          'fe.emailParsedContent',
                          'fe.opened as emailOpened',
                          'fe.openCount as emailOpenCount',
                          'fe.clickCount as emailClickCount',
                          'fe.productDisplay as emailProductDisplay',
                          'fe.product as emailProduct',
                          'fe.senderEmail as emailSenderEmail',
                          'fe.status as emailStatus',
                          'fe.emailMessageId',
                          'fe.emailMessageChannelId',
                          'fe.emailMessageTopicId',
                          'fe.emailMessageTopicName',
                          'fe.emailMessagename',
                          'fe.emailMessagedescription',
                          'fe.call2 as emailCall2',
                          'null as callCallDatetime',
                          'null as callCallDate',
                          'null as callAccountuid',
                          'null as callAddressLine1',
                          'null as callState',
                          'null as callCity',
                          'null as callZip4',
                          'null as callZip',
                          'null as callAddressLine2',
                          'null as callNextCallNotes',
                          'null as callCallType',
                          'null as callAttendeeType',
                          'null as callDetailedProducts',
                          'null as callSubmittedByMobile',
                          'fpf.productInteractionTypeId',
                          'fpf.productInteractionTypeName',
                          'fpf.messageId',
                          'fpf.messageTopicId',
                          'fpf.messageReaction',
                          'fpf.physicalMessageDesc',
                          'fpf.physicalMessageUID',
                          'fpf.quantity',
                          'fs.suggestionReferenceId',
                          'fs.suggestionUID',
                          'fs.suggestedDate',
                          'fs.channel as suggestionChannel',
                          'fs.actionTaken as suggestionActionTaken',
                          'fs.dismissReason as suggestionDismissReason',
                          'fs.interactionraw as suggestionInteractionraw',
                          'fs.interactionId as suggestionInteractionId',
                          'fs.repId as suggestionRepId',
                          'fs.detailRepActionTypeId as suggestionDetailRepActionTypeId',
                          'fs.detailRepActionName as suggestionDetailRepActionName',
                          'fs.messageId as suggestionMessageId',
                          'fs.messageName as suggestionMessageName',
                          'repTeamId as suggestionRepTeamId',
                          'fs.productId as suggestionProductId',
                          'fs.accountId as suggestionAccountId',
                          'fs.suggestedyear',
                          'fs.suggestedmonth',
                          'fs.suggestedday'
                          ]

            join_list_email = ''
            for each in list_email:
                join_list_email = join_list_email + ',' + each

            value_tablename['emails_1'] = '''
            from
              emails fe 
               inner join
                  product fpf 
                  on fe.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fe.interactionId = fs.interactionId 
            where
               fe.isCompleted = 1 
               and fs.interactionId is null'''

            value_tablename['emails_2'] = '''
            from
              emails fe 
               inner join
                  product fpf 
                  on fe.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fe.interactionId = fs.interactionId 
            where
               fe.isCompleted = 0 
               and fs.interactionId is null'''

            value_tablename['emails_3'] = '''
            from
              emails fe 
               inner join
                  product fpf 
                  on fe.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fe.interactionId = fs.interactionId 
            where
               fe.isCompleted = 1 
               and fs.interactionId is not null'''

            value_tablename['emails_4'] = '''
            from
              emails fe 
               inner join
                  product fpf 
                  on fe.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fe.interactionId = fs.interactionId 
            where
               fe.isCompleted = 0 
               and fs.interactionId is not null'''

            value_tablename['emails_5'] = '''
            from
               suggestions fs 
               left join
                  emails fe 
                  on fe.interactionId = fs.interactionId 
               left join
                  product fpf 
                  on fe.interactionId = fpf.interactionId 
            where
               (
                  fs.interactionId is null 
                  or fs.interactionraw is null
               )
               and fs.detailRepActionTypeId = 8'''
            i = 1
            spark_str = 'select distinct ' + str(i) + ' as recordclassId, \'' + values[
                str(i)] + '\' as recordclass ' + join_list_email + ' ${emails_' + str(i) + '}'
            logger.info(spark_str)
            spark_str = self.utils.sql_to_template(spark_str, value_tablename)
            logger.info(spark_str)
            df_r = self.spark.sql(spark_str)
            # logger.info(spark_str)
            df_r = df_r.select([col(c).cast("string") for c in df_r.columns])
            # s3_recordclass = s3_recordclasses_destination + "/rcid1/"
            #df_r.write.format("delta").mode("overwrite").partitionBy("recordclassId", "All_effectiveyearyear",
            #                                                         "All_effectiveyearmonth", "All_effectiveyearday").save(
            if batch:
                df_r.repartition(40).write.format("delta").mode("overwrite").partitionBy("recordclassId","allEffectiveyear").save(s3_recordclasses_destination)
            else:
                #self.utils.upsertRecordClass(df_r, s3_recordclasses_destination, self.DeltaTable)
                updates = df_r

            for i in range(2, 6):
                spark_str = 'select distinct ' + str(i) + ' as recordclassId, \'' + values[
                    str(i)] + '\' as recordclass ' + join_list_email + ' ${emails_' + str(i) + '}'
                logger.info(spark_str)
                spark_str = self.utils.sql_to_template(spark_str, value_tablename)
                logger.info(spark_str)
                df_r1 = self.spark.sql(spark_str)
                logger.info(spark_str)
                df_r1 = df_r1.select([col(c).cast("string") for c in df_r1.columns])
                # s3_recordclass = s3_recordclasses_destination + "/rcid" + str(i) + '/'
                if batch:
                    df_r1.repartition(40).write.format("delta").mode("append").partitionBy("recordclassId","allEffectiveyear").save(s3_recordclasses_destination)
                else:
                    updates = updates.union(df_r1)
                    #self.utils.upsertRecordClass(df_r1, s3_recordclasses_destination, self.DeltaTable)
                logger.info('record class saved at destination '+ str(s3_recordclasses_destination))
                # logger.info('\n')

            # --------------------------------------------------------------------------------------------------------------------------
            # load next 5 record classes [ related to visits]

            value_tablename = {}
            list_visit = [
                'COALESCE(fs.suggestedyear,fv.interactionyear) as allEffectiveyear',
                'COALESCE(fs.suggestedmonth,fv.interactionmonth) as allEffectivemonth',
                'COALESCE(fs.suggestedday,fv.interactionday) as allEffectiveday',
                "productIdList",
                "accountIdList",
                'fv.interactionId',
                'fv.externalId',
                'fv.timeZoneId as interactionTimeZoneId',
                'fv.interactionyear',
                'fv.interactionmonth',
                'fv.interactionday',
                'fv.startDateTime interactionStartDateTime',
                'fv.startDateLocal as interactionStartDateLocal',
                'fv.interactionTypeId',
                'fv.interactionTypeName',
                'fv.repActionTypeId',
                'fv.repActionTypeName',
                'fv.duration as interactionDuration',
                'fv.wasCreatedFromSuggestion as interactionWasCreatedFromSuggestion',
                'fv.isCompleted as interactionIsCompleted',
                'fv.isDeleted as interactionIsDeleted',
                'fv.createdAt as interactionCreatedAt',
                'fv.updatedAt as interactionUpdatedAt',
                'fv.repId as interactionRepId',
                'fv.accountId as interactionAccountId',
                'fv.facilityId as interactionFacilityId',
                'fv.interactionAccountIsdeleted',
                'fv.rawCsId',
                'null as emailEmailSentDate',
                'null as emailCaptureDatetime',
                'null as emailAccountuid',
                'null as emailAccountEmail',
                'null as emailEmailConfigValues',
                'null as emailEmailSubject',
                'null as emailEmailContent',
                'null as emailParsedContent',
                'null as emailOpened',
                'null as emailOpenCount',
                'null as emailClickCount',
                'null as emailProductDisplay',
                'null as emailProduct',
                'null as emailSenderEmail',
                'null as emailStatus',
                'null as emailMessageId',
                'null as emailMessageChannelId',
                'null as emailMessageTopicId',
                'null as emailMessageTopicName',
                'null as emailMessagename',
                'null as emailMessagedescription',
                'null as emailCall2',
                'fv.callDatetime as callCallDatetime',
                'fv.callDate as callCallDate',
                'fv.accountuid as callAccountuid',
                'fv.addressLine1 as callAddressLine1',
                'fv.state as callState',
                'fv.city as callCity',
                'fv.zip4 as callZip4',
                'fv.zip as callZip',
                'fv.addressLine2 as callAddressLine2',
                'fv.nextCallNotes as callNextCallNotes',
                'fv.callType as callCallType',
                'fv.attendeeType as callAttendeeType',
                'fv.detailedProducts as callDetailedProducts',
                'fv.submittedByMobile as callSubmittedByMobile',
                'fpf.productInteractionTypeId',
                'fpf.productInteractionTypeName',
                'fpf.messageId',
                'fpf.messageTopicId',
                'fpf.messageReaction',
                'fpf.physicalMessageDesc',
                'fpf.physicalMessageUID',
                'fpf.quantity',
                'fs.suggestionReferenceId',
                'fs.suggestionUID',
                'fs.suggestedDate',
                'fs.channel as suggestionChannel',
                'fs.actionTaken as suggestionActionTaken',
                'fs.dismissReason as suggestionDismissReason',
                'fs.interactionraw as suggestionInteractionraw',
                'fs.interactionId as suggestionInteractionId',
                'fs.repId as suggestionRepId',
                'fs.detailRepActionTypeId as suggestionDetailRepActionTypeId',
                'fs.detailRepActionName as suggestionDetailRepActionName',
                'fs.messageId as suggestionMessageId',
                'fs.messageName as suggestionMessageName',
                'fs.repTeamId as suggestionRepTeamId',
                'fs.productId as suggestionProductId',
                'fs.accountId as suggestionAccountId',
                'fs.suggestedyear',
                'fs.suggestedmonth',
                'fs.suggestedday'

            ]

            join_list_visit = ''
            for each in list_visit:
                join_list_visit = join_list_visit + ',' + each

            value_tablename['visits_6'] = '''
            from
            visits fv 
               inner join
                  product fpf 
                  on fv.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fv.interactionId = fs.interactionId 
            where
               fv.isCompleted = 1 
               and fs.interactionId is null'''

            value_tablename['visits_7'] = '''
            from
            visits fv 
               inner join
                  product fpf 
                  on fv.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fv.interactionId = fs.interactionId 
            where
               fv.isCompleted = 0 
               and fs.interactionId is null'''

            value_tablename['visits_8'] = '''
            from
            visits fv 
               inner join
                  product fpf 
                  on fv.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fv.interactionId = fs.interactionId 
            where
               fv.isCompleted = 1 
               and fs.interactionId is not null'''

            value_tablename['visits_9'] = '''
            from
            visits fv 
               inner join
                  product fpf 
                  on fv.interactionId = fpf.interactionId 
               left join
                  suggestions fs 
                  on fv.interactionId = fs.interactionId 
            where
               fv.isCompleted = 0
               and fs.interactionId is not null'''

            value_tablename['visits_10'] = '''
            from
               suggestions fs 
               left join
                  visits fv 
                  on fv.interactionId = fs.interactionId 
               left join
                  product fpf 
                  on fv.interactionId = fpf.interactionId 
            where
               (
                  fs.interactionId is null 
                  or fs.interactionraw is null
               )
               and fs.detailRepActionTypeId = 4'''

            for i in range(6, 11):
                spark_str = 'select distinct ' + str(i) + ' as recordclassId, \'' + values[
                    str(i)] + '\' as recordclass ' + join_list_visit + ' ${visits_' + str(i) + '}'
                spark_str = self.utils.sql_to_template(spark_str, value_tablename)
                # logger.info('\n')
                df_v1 = self.spark.sql(spark_str)
                logger.info(spark_str)
                df_v1 = df_v1.select([col(c).cast("string") for c in df_v1.columns])
                # s3_recordclass = s3_recordclasses_destination + "/rcid" + str(i) + '/'
                # df_v1.write.mode('overwrite').parquet(s3_recordclass)
                if batch:
                    df_v1.repartition(40).write.format("delta").mode("append").partitionBy("recordclassId","allEffectiveyear").save(s3_recordclasses_destination)
                else:
                    updates = updates.union(df_v1)
                    # updates.createOrReplaceTempView("rc")
                    # self.utils.upsertRecordClass(df_v1, s3_recordclasses_destination, self.DeltaTable)
                # df_r = df_r.union(df_r1)
                logger.info('record class saved at destination '+ str(s3_recordclasses_destination))
                # logger.info('\n')
            if not batch:
                updates.persist(pyspark.StorageLevel.MEMORY_ONLY)
                logger.info("init recordclass size "+ str(updates.count()))
                broadcast(updates)
                updates.createOrReplaceTempView("rc")
                # self.utils.upsertRecordClass(updates, s3_recordclasses_destination, self.DeltaTable)

            # df_r.write.format("delta").partitionBy("recordclassId").save(s3_recordclasses_destination)
        except Exception as e:
            logger.error("Error in initRecordClass: {}".format(e))
            raise