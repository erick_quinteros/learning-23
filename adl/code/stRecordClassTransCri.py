from awsglue.transforms import *
from pyspark.sql.functions import *
from pyspark.sql import SQLContext

from pyspark.sql.types import *
from pyspark.sql import Row, functions as F
from pyspark.sql.window import Window
from pyspark.sql.functions import when, lit, col
import pandas as pd
from pyspark.sql.types import IntegerType
from pyspark.sql.functions import col, expr, when
from utils import Utils
import logging
import pyspark
import sys

logger = None
# Needs to be read with a an argument from the job

class StRecordClassTransCri:
    def __init__(self, glue_context, customer, s3_destination, target, deltaTable):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session
        self.target = target
        self.DeltaTable = deltaTable

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")
        pass

    def run(self, batch):
        try:
            target = self.target
            # TODO: this is hardcoded for the duplicates introduced by strategy target productId.
            # add productIdList, and accountIdList in V23

            sourcetable = "recordclasses_dse"
            # sourcetable = "recordclasses"
            df_rc = self.utils.dataLoadDelta(self.s3_destination + "data/silver/" + sourcetable)
            df_rc = df_rc.withColumn('allEffectivedate', to_date(df_rc.allEffectivedate, 'yyyy-MM-dd'))
            if batch:
                df_rc.createOrReplaceTempView("rc")
            reps_accounts = df_rc.select("interactionRepAccount").distinct()
            logger.info("rc cached")
            # else:
            #     reps_accounts = self.spark.sql("select distinct Interaction_rep_account from rc")
            #     logger.info("reps_accounts was loaded from previous step")
            #     df_rc = df_rc.join(reps_accounts, "Interaction_rep_account").select(df_rc['*'])

            # get all the rep accounts from the dataset

            logger.info("reps_accounts count " + str(reps_accounts.count()))
            # get all months
            months = df_rc.filter(df_rc.interactionYearMonth.between(201101, 204001)).select(
                "interactionYearMonth").distinct().orderBy(["interactionYearMonth"], ascending=[1])
            master = reps_accounts.select('interactionRepAccount').distinct().crossJoin(
                months.select('interactionYearMonth')).distinct()
            # master=master.withColumn("Interaction_YearMonth", master["Interaction_YearMonth"].cast(IntegerType()))

            # ----------------------------------------------------------------------------------------------------------------------------
            # metric one
            # Email Open score

            # For each Rep_Account, calculate the total Email sent Windows F.count(recordclass)
            # For each Rep_Account, Month sum the Email Open using Windows F.sum(Email_open)

            emails = df_rc.where((col("emailStatus").isin({"Delivered_vod", "Sent_vod"})) & (
                        col("interactionTypeId") == "11")).select("recordclass", "interactionTypeId", "emailOpened",
                                                                  "interactionRepId", "interactionAccountId",
                                                                  "interactionRepAccount", "emailProduct",
                                                                  "interactionmonth", "interactionyear",
                                                                  "interactionYearMonth", "interactionStartDateLocal",
                                                                  F.count("recordclass").over(
                                                                      Window.partitionBy("interactionRepAccount",
                                                                                         "interactionYearMonth")).alias(
                                                                      "sentYearMonth"), F.sum("emailOpened").over(
                    Window.partitionBy("interactionRepAccount").orderBy('interactionYearMonth').rangeBetween(
                        Window.unboundedPreceding, 0)).alias("openTotal").cast(IntegerType()))

            # Before calculating the Email Open Score Choose last entry in the month as final value
            w1 = Window.partitionBy("interactionRepAccount", "interactionYearMonth").orderBy(
                F.desc("interactionStartDateLocal"))
            emails2 = emails.withColumn("rn", F.row_number().over(w1)).filter("rn=1").select("recordclass",
                                                                                             "interactionTypeId",
                                                                                             "emailOpened",
                                                                                             "interactionRepId",
                                                                                             "interactionAccountId",
                                                                                             "interactionRepAccount",
                                                                                             "emailProduct",
                                                                                             "interactionmonth",
                                                                                             "interactionyear",
                                                                                             "interactionYearMonth",
                                                                                             "sentYearMonth",
                                                                                             "openTotal")

            # number of emails sent by every rep_account [Total_Emails_Sent] usinf F.sum("sent_year_month")
            emails2 = emails2.select("recordclass", "interactionTypeId", "emailOpened", "interactionRepId",
                                     "interactionAccountId", "interactionRepAccount", "emailProduct",
                                     "interactionmonth", "interactionyear", "interactionYearMonth", "sentYearMonth",
                                     "openTotal", F.sum("sentYearMonth").over(
                    Window.partitionBy("interactionRepAccount").orderBy('interactionYearMonth').rangeBetween(
                        Window.unboundedPreceding, 0)).alias("sentTotal"))

            # Finally Email Open score which is Total Emails Opened/ Total Emails Sent
            emails2 = emails2.withColumn("emailOpenScore", when(col("sentTotal") >= 3,
                                                                  F.round(emails2.openTotal / emails2.sentTotal,
                                                                          2)).otherwise(lit(None)))

            # (cri.Open_Total + updatesRcDse.Open_Total / (cri.Sent_Total + updatesRcDse.Sent_Total)

            # update the probabilities down through time
            # look back the last Email_Open_Score which was not null for that rep_account
            emails2 = emails2.withColumn("emailOpenScore", F.last('emailOpenScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            emails_final = emails2.select("interactionRepAccount", "interactionYearMonth",
                                          "emailOpenScore").drop_duplicates()

            master = master.join(emails_final, (master.interactionRepAccount == emails_final.interactionRepAccount) & (
                        master.interactionYearMonth == emails_final.interactionYearMonth), 'left_outer').select(
                master["*"], emails_final.emailOpenScore).dropDuplicates()
            master = master.withColumn("emailOpenScore", F.last('emailOpenScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # Metric 2: Product Tenure
            df_rc = df_rc.withColumn('interactionStartDateLocalDt',
                                     to_date(df_rc.interactionStartDateLocal, 'yyyy-MM-dd'))
            df_rc = df_rc.orderBy(["interactionRepAccount", 'interactionStartDateLocalDt'], ascending=[1, 1])

            # Find time stamps for first and last interactions
            # min,max,previous event
            tenure = df_rc.select("recordclass", "interactionId", "interactionRepId", "interactionAccountId",
                                  "interactionRepAccount", "interactionTypeId", "repActionTypeId",
                                  "interactionStartDateTime", "interactionStartDateLocalDt", "interactionIsCompleted",
                                  "interactionYearMonth", F.min("interactionStartDateLocalDt").over(
                    Window.partitionBy("interactionRepAccount")).alias("eventFirst"),
                                  F.max("interactionStartDateLocalDt").over(
                                      Window.partitionBy("interactionRepAccount", "interactionYearMonth")).alias(
                                      "eventLast"), F.lag("interactionStartDateLocalDt").over(
                    Window.partitionBy("interactionRepAccount").orderBy("interactionStartDateLocalDt")).alias(
                    "eventPrevious"))

            # calculate tenure score by subtracting lastevent from Firstevent
            tenure = tenure.withColumn("tenureScore", datediff(tenure.eventLast, tenure.eventFirst))

            tenure2 = tenure.select("interactionRepAccount", "tenureScore", "interactionYearMonth").distinct()

            # update tenure score down the time by looking at previous null value for that rep_account
            tenure2.withColumn("tenureScore", F.last('tenureScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            tenure2 = tenure2.select("interactionRepAccount", "interactionYearMonth", "tenureScore").drop_duplicates()

            master = master.join(tenure2, (master.interactionRepAccount == tenure2.interactionRepAccount) & (
                        master.interactionYearMonth == tenure2.interactionYearMonth), 'left_outer').select(master["*"],
                                                                                                             tenure2.tenureScore).dropDuplicates()
            master = master.withColumn("tenureScore", F.last('tenureScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # metric 3 completed visits only
            # filter visit records based on interactiontype='4'
            visits = df_rc.where(col("interactionTypeId") == '4').select("interactionId", "interactionTypeId",
                                                                         "interactionRepId", "interactionAccountId",
                                                                         "interactionRepAccount",
                                                                         "interactionStartDateLocalDt",
                                                                         "interactionIsCompleted", "interactionIsDeleted",
                                                                         "interactionYearMonth")
            # get only compeletd visits
            visits = visits.where(col("interactionIsCompleted") == 1).distinct()

            # choose first in group where more than one visit to the same HCP is logged for the day
            visits = visits.select(visits["*"], F.row_number().over(
                Window.partitionBy("interactionRepAccount", "interactionStartDateLocalDt").orderBy(
                    "interactionRepAccount", "interactionStartDateLocalDt")).alias(
                "visitsSamedaySameRepAcct")).filter(col("visitsSamedaySameRepAcct") == 1)

            # find total count of all visits for that rep_account and total count of all visits per month
            visits = visits.select(visits["*"],
                                   F.count('interactionId').over(Window.partitionBy("interactionRepAccount")).alias(
                                       "visitCount"), F.count('interactionId').over(
                    Window.partitionBy("interactionRepAccount", "interactionYearMonth")).alias("repAccountCount"))

            # test
            # visits.where((col("Interaction_rep_account")=="1057_53146") & (col("Interaction_startDateLocal_dt")=="2019-02-08")).show()

            visits2 = visits.select("interactionRepAccount", "interactionYearMonth", "interactionTypeId", "visitCount",
                                    "repAccountCount").distinct()
            visits2 = visits2.withColumn("interactionYearMonth", visits2["interactionYearMonth"].cast(IntegerType()))

            # for every rep_account get the first_month and last_month interaction
            visits2 = visits2.select(visits2["*"], F.min("interactionYearMonth").over(
                Window.partitionBy("interactionRepAccount")).alias("firstMonth"), F.max("interactionYearMonth").over(
                Window.partitionBy("interactionRepAccount")).alias("lastMonth"))

            # join visits with reps table
            visits3 = master.join(visits2, (visits2.interactionRepAccount == master.interactionRepAccount) & (
                        visits2.interactionYearMonth == master.interactionYearMonth), 'left_outer').select(
                master.interactionRepAccount, master.interactionYearMonth, "interactionTypeId", visits2.visitCount,
                visits2.repAccountCount, visits2.firstMonth, visits2.lastMonth)

            # define the window for each first_month, if it's null look bacl previous month to assign it
            window = Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth')
            # define the forward-filled column
            filled_column_first = last(visits3['firstMonth'], ignorenulls=True).over(window)
            # do the fill
            visits3 = visits3.withColumn('firstMonth', filled_column_first)

            # define the for last-filled column
            filled_column_last = last(visits3['lastMonth'], ignorenulls=True).over(window)
            visits3 = visits3.withColumn('lastMonth', filled_column_last)

            # test
            # visits3.where(col("Interaction_rep_account")=='4328_47027').show()

            # check the condition with [Total visits for that rep_account for that month] and interactionmonth>first_month and Interactionmonth<=last_month then 0 else [Total visits for that rep_account for that month]
            visits3 = visits3.withColumn("repAccountCount", when(
                (col("repAccountCount").isNull()) & (visits3.interactionYearMonth >= visits3.firstMonth) & (
                            visits3.interactionYearMonth <= visits3.lastMonth), 0).otherwise(visits3.repAccountCount))

            # visits3.where(col("Interaction_rep_account")=="1013_1015").show()
            # final calculation, avergae cummulative mean for that rep_account by Interaction_year_month
            visits4 = visits3.where(col("visitCount") >= 3).withColumn('visitScore', F.round(F.avg(
                when(col("repAccountCount").isNull(), 0).otherwise(
                    (visits3.repAccountCount) + visits3.repAccountCount * 0)).over(
                Window.partitionBy("interactionRepAccount").orderBy("interactionYearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)), 2)).select("interactionTypeId", "interactionRepAccount",
                                                               "interactionYearMonth", "visitScore").distinct()

            visits4 = visits4.select("interactionRepAccount", "interactionYearMonth", "visitScore").drop_duplicates()

            master = master.join(visits4, (master.interactionRepAccount == visits4.interactionRepAccount) & (
                        master.interactionYearMonth == visits4.interactionYearMonth), 'left_outer').select(master["*"],
                                                                                                             visits4.visitScore).dropDuplicates()
            master = master.withColumn("visitScore", F.last('visitScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # Metric 4: Product Rep-Account Visit Cadence
            df_rc = df_rc.withColumn('interactionStartDateLocal', to_date(df_rc.interactionStartDateLocal, 'yyyy-MM-dd'))
            df_rc = df_rc.withColumn("interactionYearMonth", df_rc["interactionYearMonth"].cast(IntegerType()))
            cadence = df_rc.where((col("interactionTypeId") == '4') & (col('interactionIsCompleted') == "1")).select(
                "interactionId", "interactionTypeId", "interactionRepId", "interactionAccountId",
                "interactionRepAccount", "interactionIsCompleted", "interactionStartDateLocal", "interactionIsDeleted",
                "interactionYearMonth")

            cadence = cadence.select(cadence["*"], F.min("interactionStartDateLocal").over(
                Window.partitionBy("interactionRepAccount")).alias("firstDate"),
                                     F.max("interactionStartDateLocal").over(
                                         Window.partitionBy("interactionRepAccount", "interactionYearMonth")).alias(
                                         "lastDate")).select(["*"]).withColumn("tenure",
                                                                                datediff("lastDate", "firstDate"))

            cadence = cadence.select(cadence["*"], F.lag("interactionStartDateLocal").over(
                Window.partitionBy("interactionRepAccount").orderBy("interactionStartDateLocal")).alias("nextDate"))
            cadence = cadence.select(cadence["*"],
                                     datediff("interactionStartDateLocal", "nextDate").alias("daysBtwVisits"))

            cadence = cadence.select(cadence["*"], F.round(F.avg("daysBtwVisits").over(
                Window.partitionBy("interactionRepAccount").orderBy("interactionYearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)), 2).alias("avgDaysBtwVisits"), F.round(
                F.stddev("daysBtwVisits").over(
                    Window.partitionBy("interactionRepAccount").orderBy("interactionYearMonth").rowsBetween(
                        Window.unboundedPreceding, 0)), 2).alias("stdDaysBtwVisits"))

            columns = ['avgDaysBtwVisits', 'stdDaysBtwVisits']
            for column in columns:
                cadence = cadence.withColumn(column, F.when(F.isnan(F.col(column)), None).otherwise(F.col(column)))

            # test
            # cadence.where(col("Interaction_rep_account")=="2308_198078").show()
            # z.show(cadence.where(col("Interaction_rep_account")=="1018_5726").select("avg_days_btw_visits","std_days_btw_visits"))

            cadence2 = cadence.withColumn("cadenceScore",
                                          F.round((cadence.avgDaysBtwVisits + cadence.stdDaysBtwVisits) / 2,
                                                  2)).select("interactionTypeId", "interactionRepAccount",
                                                             "interactionYearMonth", "interactionStartDateLocal",
                                                             "cadenceScore").distinct()

            w1 = Window.partitionBy("interactionRepAccount", "interactionYearMonth").orderBy(
                "interactionStartDateLocal")
            cadence2 = cadence2.withColumn("countdays", F.max("interactionStartDateLocal").over(w1))
            w3 = Window.partitionBy("interactionRepAccount", "interactionYearMonth").orderBy(F.desc("countdays"))
            cadence3 = cadence2.withColumn("rn", F.row_number().over(w3)).filter("rn=1").select("interactionTypeId",
                                                                                                "interactionRepAccount",
                                                                                                "interactionYearMonth",
                                                                                                "cadenceScore")

            cadence3 = cadence3.select("interactionRepAccount", "interactionYearMonth",
                                       "cadenceScore").drop_duplicates()

            master = master.join(cadence3, (master.interactionRepAccount == cadence3.interactionRepAccount) & (
                        master.interactionYearMonth == cadence3.interactionYearMonth), 'left_outer').select(master["*"],
                                                                                                              cadence3.cadenceScore).dropDuplicates()
            master = master.withColumn("cadenceScore", F.last('cadenceScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            sourcetable = "suggestions"
            df_suggestions = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
            # df_suggestions.cache()
            df_suggestions.createOrReplaceTempView("suggestions")
            logger.info("df_suggestions cached")

            spark_str = '''
            select suggestionReferenceId,detailRepActionTypeId,actionTaken,suggestedDate,repId,productId, accountId,concat(left(SUBSTRING_INDEX(suggestedDate,'-',2),4),right(SUBSTRING_INDEX(suggestedDate,'-',2),2)) suggestionYearMonth  from suggestions'''

            suggestions = self.spark.sql(spark_str)
            suggestions = suggestions.withColumn("repId", suggestions.repId.cast("String"))
            suggestions = suggestions.withColumn("accountId", suggestions.accountId.cast("String"))
            suggestions = suggestions.withColumn("suggestionRepAccount",
                                                 F.concat(F.col('repId'), F.lit('_'), F.col('accountId')))
            suggestions_visit = suggestions.where(col("detailRepActionTypeId") == '4').select("suggestionReferenceId",
                                                                                              "detailRepActionTypeId",
                                                                                              "actionTaken",
                                                                                              "suggestedDate", "repId",
                                                                                              "accountId",
                                                                                              "suggestionRepAccount",
                                                                                              "suggestionYearMonth")

            suggestions_visit = suggestions_visit.withColumn("lastFlag", when(col("suggestedDate") == (
                F.max("suggestedDate").over(
                    Window.partitionBy("suggestionRepAccount", "suggestionYearMonth", "suggestionReferenceId"))),
                                                                              1).otherwise(0))

            suggestions_visit = suggestions_visit.select(suggestions_visit["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("suggestionRepAccount", "suggestionYearMonth", "suggestionReferenceId")).alias(
                "uniqueSuggestionVisitCount")).filter("lastFlag==1")

            suggestions_visit = suggestions_visit.select(suggestions_visit["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("suggestionRepAccount", "suggestionYearMonth")).alias("totalSuggestionMo"),
                                                         when((col("actionTaken") == "Suggestions Completed"), 1).otherwise(
                                                             0).alias("suggestionComplete"))

            suggestions_visit = suggestions_visit.select(suggestions_visit["*"], F.sum("suggestionComplete").over(
                Window.partitionBy("suggestionRepAccount").orderBy("suggestionYearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)).alias("completeTotal"))

            last_visit2 = suggestions_visit.select("suggestionRepAccount", "detailRepActionTypeId",
                                                   "suggestionYearMonth", "suggestedDate", "totalSuggestionMo",
                                                   "completeTotal").distinct()

            w3 = Window.partitionBy("suggestionRepAccount", "suggestionYearMonth").orderBy(F.desc("suggestedDate"))
            last_visit2 = last_visit2.withColumn("rn", F.row_number().over(w3)).filter("rn=1")

            last_visit2 = last_visit2.select(last_visit2["*"], F.sum("totalSuggestionMo").over(
                Window.partitionBy("suggestionRepAccount").rowsBetween(Window.unboundedPreceding, 0)).alias(
                "suggestionTotal"))

            last_visit2 = last_visit2.withColumn("suggestionVisitScore", when(col("suggestionTotal") >= 3, F.round(
                last_visit2.completeTotal / last_visit2.suggestionTotal, 2)).otherwise(lit(None)))
            last_visit2 = last_visit2.select("detailRepActionTypeId", "suggestionRepAccount", "suggestionYearMonth",
                                             "suggestionVisitScore").distinct()

            last_visit2 = last_visit2.select("suggestionRepAccount", "suggestionYearMonth",
                                             "suggestionVisitScore").drop_duplicates()

            master = master.join(last_visit2, (master.interactionRepAccount == last_visit2.suggestionRepAccount) & (
                        master.interactionYearMonth == last_visit2.suggestionYearMonth), 'left_outer').select(master["*"],
                                                                                                                last_visit2.suggestionVisitScore).dropDuplicates()
            master = master.withColumn("suggestionVisitScore", F.last('suggestionVisitScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # metric 6
            suggestions_email = suggestions.where(col("detailRepActionTypeId") == '8').select("suggestionReferenceId",
                                                                                              "detailRepActionTypeId",
                                                                                              "actionTaken",
                                                                                              "suggestedDate", "repId",
                                                                                              "accountId",
                                                                                              "suggestionRepAccount",
                                                                                              "suggestionYearMonth")
            suggestions_email = suggestions_email.withColumn("lastFlag", when(col("suggestedDate") == (
                F.max("suggestedDate").over(
                    Window.partitionBy("suggestionRepAccount", "suggestionYearMonth", "suggestionReferenceId"))),
                                                                              1).otherwise(0))

            suggestions_email = suggestions_email.select(suggestions_email["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("suggestionRepAccount", "suggestionYearMonth", "suggestionReferenceId")).alias(
                "uniqueSuggestionEmailCount")).filter("lastFlag==1")

            suggestions_email = suggestions_email.select(suggestions_email["*"], F.count("suggestionReferenceId").over(
                Window.partitionBy("suggestionRepAccount", "suggestionYearMonth")).alias("totalSuggestionMo"),
                                                         when((col("actionTaken") == "Suggestions Completed"), 1).otherwise(
                                                             0).alias("suggestionComplete"))

            suggestions_email = suggestions_email.select(suggestions_email["*"], F.sum("suggestionComplete").over(
                Window.partitionBy("suggestionRepAccount").orderBy("suggestionYearMonth").rangeBetween(
                    Window.unboundedPreceding, 0)).alias("completeTotal"))

            last_email2 = suggestions_email.select("suggestionRepAccount", "detailRepActionTypeId",
                                                   "suggestionYearMonth", "suggestedDate", "totalSuggestionMo",
                                                   "completeTotal").distinct()

            w5 = Window.partitionBy("suggestionRepAccount", "suggestionYearMonth").orderBy(F.desc("suggestedDate"))
            last_email2 = last_email2.withColumn("rn", F.row_number().over(w5)).filter("rn=1")

            last_email2 = last_email2.select(last_email2["*"], F.sum("totalSuggestionMo").over(
                Window.partitionBy("suggestionRepAccount").rowsBetween(Window.unboundedPreceding, 0)).alias(
                "suggestionTotal"))

            last_email2 = last_email2.withColumn("suggestionEmailScore", when(col("suggestionTotal") >= 3, F.round(
                last_email2.completeTotal / last_email2.suggestionTotal, 2)).otherwise(lit(None)))

            last_email2 = last_email2.select("suggestionRepAccount", "suggestionYearMonth",
                                             "suggestionEmailScore").distinct()

            master = master.join(last_email2, (master.interactionRepAccount == last_email2.suggestionRepAccount) & (
                        master.interactionYearMonth == last_email2.suggestionYearMonth), 'left_outer').select(master["*"],
                                                                                                                last_email2.suggestionEmailScore).dropDuplicates()
            master = master.withColumn("suggestionEmailScore", F.last('suggestionEmailScore', True).over(
                Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                           0)))

            # ----------------------------------------------------------------------------------------------------------------------------

            # check if we have to add target

            if target == 'yes':
                sourcetable = "strategy_target"
                df_st = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
                df_st.cache()
                df_st.createOrReplaceTempView("st")
                logger.info("st cached")

                # sourcetable = "rep_account_assignment"
                # df_ra = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
                # df_ra.cache()
                # df_ra.createOrReplaceTempView("ra")
                # logger.info("ra cached")
                #
                # sourcetable = "rep_team_rep"
                # df_rt = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
                # df_rt.cache()
                # df_rt.createOrReplaceTempView("rt")
                # logger.info("df_rt cached")

                # get target and process a bit

                spark_str = """
                select targetAccount, repTeamIDAssociated, repIDAssociated,
                case when targetingLevelId=6 then 'VISIT' 
                when targetingLevelId=5 then 'SEND_ANY' end as interactionTargetingLevelId,
                case when targetingLevelId=6 then 'VISIT_DETAIL' 
                when targetingLevelId=5 then 'SEND' end as suggestionTargetingLevelId
                ,t.strategyTargetId,t.targetsPeriodId,t.target,t.startDate,t.endDate,concat(left(SUBSTRING_INDEX(t.startDate,'-',2),4),right(SUBSTRING_INDEX(t.startDate,'-',2),2)) startDateMonth,concat(left(SUBSTRING_INDEX(t.endDate,'-',2),4),right(SUBSTRING_INDEX(t.endDate,'-',2),2)) endDateMonth
                from st t 
                """

                df_st_2 = self.spark.sql(spark_str)
                df_st_2 = df_st_2.withColumn("repTeamIDAssociated", df_st_2.repTeamIDAssociated.cast("String"))
                df_st_2 = df_st_2.withColumn("repIDAssociated", df_st_2.repIDAssociated.cast("String"))
                df_st_2 = df_st_2.withColumn("targetAccount", df_st_2.targetAccount.cast("String"))
                # df_st_2=self.spark.sql(spark_str)
                logger.info(df_st_2.printSchema())
                df_st_2.createOrReplaceTempView("st_2")

                spark_str = """
                select * from (
                select rd.*,t1.interactionTargetingLevelId,t1.suggestionTargetingLevelId,t1.targetsPeriodId,t1.strategyTargetId,t1.target from rc rd 
                left join st_2 t1 
                on 
                allAccountId=t1.targetAccount and (rd.repTeamId = t1.repTeamIDAssociated or rd.allRepId = t1.repIDAssociated)
                and COALESCE(rd.interactionTypeName,rd.suggestionDetailRepActionName)=COALESCE(t1.interactionTargetingLevelId,t1.suggestionTargetingLevelId)
                and rd.allEffectivedate between t1.startDate and t1.endDate
                ) t 
                """

                df_rc_2 = self.spark.sql(spark_str)

                # df_rc_2.createOrReplaceTempView("rc_v2")
                # df_rc_2.repartition("recordclassid").write.mode('overwrite').partitionBy("recordclassid").parquet(s3_destination+customer+'/'+environment+"/recordclasses_st/")
                if batch:
                    logger.info("st recordclass size " + str(df_rc_2.count()))
                    df_rc_2.repartition(40).write.mode('overwrite').partitionBy("allYear", "recordclassId").format(
                        "delta").save(self.s3_destination + "data/silver/recordclasses_st/")

                else:
                    df_rc_2.persist(pyspark.StorageLevel.MEMORY_ONLY)
                    logger.info("st recordclass size " + str(df_rc_2.count()))
                    broadcast(df_rc_2)
                    self.utils.upsertRecordClass(df_rc_2, self.s3_destination + "data/silver/recordclasses_st/",
                                                 self.DeltaTable)
                    df_rc_2.createOrReplaceTempView("rc")
                    logger.info("done upserting recordclasses_st")
                    # df_rc_2 = self.utils.dataLoadDelta(self.s3_destination + "data/silver/recordclasses_st/")
                # metric 7
                pt = df_rc_2.select("interactionId", "interactionRepAccount", "targetsPeriodId", "interactionTypeId",
                                    "target").distinct()

                pt = pt.select(pt["*"], F.count("interactionId").over(
                    Window.partitionBy("interactionRepAccount", "targetsPeriodId", "interactionTypeId")).alias(
                    "targetCount"))
                pt = pt.select("interactionRepAccount", "targetsPeriodId", "interactionTypeId", "target",
                               "targetCount").distinct()
                pt = pt.select(pt["*"], F.sum("target").over(
                    Window.partitionBy("interactionRepAccount", "targetsPeriodId", "interactionTypeId")).alias(
                    "targetTotal"))

                pt = pt.withColumn("targetAchievement",
                                   when(col("target") != 0, F.round(pt.targetCount / pt.targetTotal, 2)).otherwise(0))

                pt3 = pt.groupby('interactionRepAccount', 'targetsPeriodId', 'targetCount').pivot("interactionTypeId").agg(
                    sum("targetAchievement"))

                pt3 = pt3.withColumnRenamed("11", "emails")
                pt3 = pt3.withColumnRenamed("4", "visits")

                pt3 = pt3.withColumn("visitScore", when(pt3.visits.isNotNull(), F.round(pt3.visits * pt3.targetCount, 2)))
                pt3 = pt3.withColumn("emailScore", when(pt3.visits.isNotNull(), F.round(pt3.emails * pt3.targetCount, 2)))
                pt3 = pt3.select("interactionRepAccount", "targetsPeriodId", "visitScore", "emailScore").distinct()
                pt3 = pt3.replace(float('nan'), None)

                pt3 = pt3.withColumn("emailScore", F.last('emailScore', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                pt3 = pt3.withColumn("emailScore", F.last('emailScore', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))
                pt3 = pt3.withColumn("visitScore", F.last('visitScore', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                pt3 = pt3.withColumn("visitScore", F.last('visitScore', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))

                w5 = Window.partitionBy("interactionRepAccount", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                pt3 = pt3.withColumn("rn", F.row_number().over(w5)).filter("rn=1")

                w6 = Window.partitionBy("interactionRepAccount", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                pt3 = pt3.withColumn("rn", F.row_number().over(w6)).filter("rn=1")
                ##forward fill email and visit score

                pt3 = pt3.select(pt3["*"], when((pt3.visitScore.isNotNull()) & (pt3.emailScore.isNotNull()),
                                                (pt3.visitScore + pt3.emailScore) / 2).when((pt3.visitScore.isNotNull()),
                                                                                              pt3.visitScore).otherwise(
                    pt3.emailScore).alias("targetAchievementScore"))
                pt3 = pt3.select("interactionRepAccount", "targetsPeriodId", "targetAchievementScore").distinct()

                pt3 = pt3.withColumn("targetsPeriodId", pt3["targetsPeriodId"].cast(IntegerType()))
                pt3 = pt3.join(df_st_2, (pt3.targetsPeriodId == df_st_2.targetsPeriodId), 'left_outer').select(pt3["*"],
                                                                                                               df_st_2.startDateMonth).dropDuplicates()

                master = master.join(pt3, (master.interactionRepAccount == pt3.interactionRepAccount) & (
                            master.interactionYearMonth == pt3.startDateMonth), 'left_outer').select(master["*"],
                                                                                                      pt3.targetAchievementScore).dropDuplicates()
                master = master.withColumn("targetAchievementScore", F.last('targetAchievementScore', True).over(
                    Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                               0)))

                # metric 8

                ct = df_rc_2.select("recordclass", "interactionId", "interactionRepAccount", "targetsPeriodId",
                                    "interactionTypeId", "target").distinct()
                ct = ct.where((col("interactionTypeId").isin({"11", "4"})) & ((col("interactionIsCompleted") == "1")))

                ct = ct.withColumn("channelCountPeriod", F.count("recordclass").over(
                    Window.partitionBy("interactionRepAccount", "targetsPeriodId", "interactionTypeId")))

                ct = ct.withColumn("channelTarget",
                                   when(col("target") != 0, F.round(ct.channelCountPeriod / ct.target, 2)).otherwise(
                                       ct.channelCountPeriod))
                ct = ct.select("interactionRepAccount", "targetsPeriodId", "interactionTypeId", "target",
                               "channelTarget").distinct()

                ct = ct.withColumn("channelUtilization", F.avg("channelTarget").over(
                    Window.partitionBy("interactionRepAccount", "interactionTypeId", "targetsPeriodId")))
                ct = ct.select("interactionRepAccount", "targetsPeriodId", "interactionTypeId", "target",
                               "channelUtilization").distinct()

                ct3 = ct.groupby('interactionRepAccount', 'targetsPeriodId').pivot(
                    "interactionTypeId").agg(sum("channelUtilization"))

                ct3 = ct3.withColumnRenamed("11", "emails")
                ct3 = ct3.withColumnRenamed("4", "visits")

                ct3 = ct3.withColumn("emails", F.last('emails', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                ct3 = ct3.withColumn("emails", F.last('emails', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))
                ct3 = ct3.withColumn("visits", F.last('visits', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 0)))
                ct3 = ct3.withColumn("visits", F.last('visits', True).over(
                    Window.partitionBy('interactionRepAccount', 'targetsPeriodId').rowsBetween(-sys.maxsize, 1)))

                w5 = Window.partitionBy("interactionRepAccount", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                ct3 = ct3.withColumn("rn", F.row_number().over(w5)).filter("rn=1")

                w6 = Window.partitionBy("interactionRepAccount", "targetsPeriodId").orderBy(F.desc("targetsPeriodId"))
                ct3 = ct3.withColumn("rn", F.row_number().over(w6)).filter("rn=1")

                ct3 = ct3.select(ct3["*"], when((~isnan(ct3.visits)) & (~isnan(ct3.emails)) & (ct3.emails >= ct3.visits),
                                                (F.round(ct3.visits / ct3.emails, 2))).when(
                    (~isnan(ct3.visits)) & (~isnan(ct3.emails)) & (ct3.visits >= ct3.emails),
                    (F.round(ct3.emails / ct3.visits, 2))).alias("channelQuarter"))

                ct3 = ct3.withColumn('channelScore', F.round(F.avg(ct3.channelQuarter).over(
                    Window.partitionBy("interactionRepAccount", "targetsPeriodId").orderBy(
                        "targetsPeriodId").rangeBetween(Window.unboundedPreceding, 0)), 2))

                ct3 = ct3.withColumn("channelScore", F.last('channelScore', True).over(
                    Window.partitionBy('interactionRepAccount').rowsBetween(-sys.maxsize, 0)))

                ct3 = ct3.select("interactionRepAccount", "targetsPeriodId", "channelScore").distinct()

                ct3 = ct3.withColumn("targetsPeriodId", ct3["targetsPeriodId"].cast(IntegerType()))

                ct3 = ct3.join(df_st_2, (ct3.targetsPeriodId == df_st_2.targetsPeriodId), 'left_outer').select(ct3["*"],
                                                                                                               df_st_2.startDateMonth).dropDuplicates()

                master = master.join(ct3, (master.interactionRepAccount == ct3.interactionRepAccount) & (
                            master.interactionYearMonth == ct3.startDateMonth), 'left_outer').select(master["*"],
                                                                                                      ct3.channelScore).dropDuplicates()
                master = master.withColumn("channelScore", F.last('channelScore', True).over(
                    Window.partitionBy('interactionRepAccount').orderBy('interactionYearMonth').rowsBetween(-sys.maxsize,
                                                                                                               0)))
                ##############
                # master = master.withColumn("target_achievement_score", lit(None).cast(StringType()))
                # master = master.withColumn("channel_score", lit(None).cast(StringType()))
                # master = master.withColumn("target_achievement_score",
                #                            when(master.target_achievement_score.isNull(), 0.0).otherwise(0.0))
                # master = master.withColumn("channel_score", when(master.channel_score.isNull(), 0.0).otherwise(0.0))
                ###############
                # master.write.mode('overwrite').partitionBy("Interaction_YearMonth").format("delta").save(
                #     self.s3_destination + "data/silver/cri_scores/")
                if batch:
                    master.write.mode('overwrite').partitionBy("interactionYearMonth").format("delta").save(
                        self.s3_destination + "data/silver/cri_scores/")
                else:
                    deltaTable = self.DeltaTable.forPath(self.spark, self.s3_destination + "data/silver/cri_scores/")
                    master = master.join(df_rc_2.select("interactionRepAccount").distinct(), "interactionRepAccount").select(master['*'])
                    master.persist(pyspark.StorageLevel.MEMORY_ONLY)
                    logger.info("cri_score size " + str(master.count()))
                    broadcast(master)
                    deltaTable.alias("cri_scores").merge(
                        master.alias("updates"),
                        " cri_scores.interactionYearMonth = updates.interactionYearMonth and cri_scores.interactionRepAccount = updates.interactionRepAccount ") \
                        .whenMatchedUpdateAll() \
                        .whenNotMatchedInsertAll() \
                        .execute()

            else:
                master = master.withColumn("targetAchievementScore", lit(None).cast(StringType()))
                master = master.withColumn("channelScore", lit(None).cast(StringType()))
                master = master.withColumn("targetAchievementScore",
                                           when(master.targetAchievementScore.isNull(), 0.0).otherwise(0.0))
                master = master.withColumn("channelScore", when(master.channelScore.isNull(), 0.0).otherwise(0.0))
                # master.write.mode('overwrite').partitionBy("Interaction_YearMonth").format("delta").save(
                #     self.s3_destination + "data/silver/cri_scores/")
                if batch:
                    master.write.mode('overwrite').partitionBy("interactionYearMonth").format("delta").save(
                        self.s3_destination + "data/silver/cri_scores/")
                else:
                    deltaTable = self.DeltaTable.forPath(self.spark, self.s3_destination + "data/silver/cri_scores/")
                    df_rc_2 = self.spark.sql("select * from rc")
                    master = master.join(df_rc_2.select("interactionRepAccount").distinct(),
                                         "interactionRepAccount").select(master['*'])
                    master.persist(pyspark.StorageLevel.MEMORY_ONLY)
                    logger.info("cri_score size " + str(master.count()))
                    broadcast(master)
                    deltaTable.alias("cri_scores").merge(
                        master.alias("updates"),
                        " cri_scores.interactionYearMonth = updates.interactionYearMonth and cri_scores.interactionRepAccount = updates.interactionRepAccount ") \
                        .whenMatchedUpdateAll() \
                        .whenNotMatchedInsertAll() \
                        .execute()
        except Exception as e:
            logger.error("Error in stRecordClassTransCri: {}".format(e))
            raise

# ----------------------------------------------------------------------------------------------------------------------------
