from awsglue.transforms import *
from pyspark.sql.functions import *
from pyspark.sql.functions import mean, min, max
from pyspark.sql import Row, functions as F
from pyspark.sql.window import Window
from pyspark.sql.functions import when, lit, col

from pyspark.sql.types import IntegerType
from pyspark.sql.functions import col, expr, when
from utils import Utils
import logging
import pyspark

logger = None

print('Import Successful')


class FinalDataTrans:
    def __init__(self, glue_context, customer, s3_destination, target, deltaTable, df_dict_pandas):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session
        self.target = target
        self.DeltaTable = deltaTable
        self.df_dict_pandas = df_dict_pandas

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")
        pass

    def run(self, batch):
        try:
            target = self.target
            sourcetable = "cri_scores"
            df_cri = self.utils.dataLoadDelta(self.s3_destination + "data/silver/" + sourcetable)
            #df_cri.cache()
            df_cri.createOrReplaceTempView("cri")
            logger.info("cri cached")

            # read new recordclasses
            if batch:
                if target == 'yes':
                    sourcetable = "recordclasses_st"
                    df_rc = self.utils.dataLoadDelta(self.s3_destination + "data/silver/" + sourcetable)
                    # df_rc=df_rc.withColumn('All_effectivedate',to_date(df_rc.All_effectivedate,'yyyy-MM-dd'))
                    #df_rc.cache()
                    #df_rc.createOrReplaceTempView("rc")
                else:
                    sourcetable = "recordclasses_dse"
                    df_rc = self.utils.dataLoadDelta(self.s3_destination + "data/silver/" + sourcetable)
                    # df_rc=df_rc.withColumn('All_effectivedate',to_date(df_rc.All_effectivedate,'yyyy-MM-dd'))
                    #df_rc.cache()
                    #df_rc.createOrReplaceTempView("rc")
            else:
                df_rc = self.spark.sql("select * from rc")
            logger.info("rc cached")
            df_rc = df_rc.withColumn("allRepId", df_rc["allRepId"].cast(IntegerType()))
            df_rc = df_rc.withColumn("allAccountId", df_rc["allAccountId"].cast(IntegerType()))

            sourcetable = "rep"
            df_rep = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
            #df_rep.cache()
            df_rep.createOrReplaceTempView("rep")
            logger.info("rep  cached")

            sourcetable = "rep_team_rep"
            df_repteamrep = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
           # df_repteamrep.cache()
            df_repteamrep.createOrReplaceTempView("rtr")
            logger.info("repteamrep  cached")

            sourcetable = "account"
            df_acct = self.utils.dataLoadDelta(self.s3_destination + "data/bronze/" + sourcetable)
            #df_acct.cache()
            df_acct.createOrReplaceTempView("acct")
            logger.info("account  cached")
            df_acct = df_acct.drop(df_acct.externalId)

            #print(df_rep.columns)
            df_rc = df_rc.join(df_acct, (df_rc.allAccountId == df_acct.accountId), 'left_outer').select(df_rc["*"],
                                                                                                         df_acct["*"])
            df_rc = df_rc.join(df_rep, (df_rc.allRepId == df_rep.repId), 'left_outer').select(df_rc["*"], df_rep.repName,
                                                                                               df_rep.repTimeZoneId,
                                                                                               df_rep.repIsActivated,
                                                                                               df_rep.repSeConfigId,
                                                                                               df_rep.repAvgLatCurrentAccounts,
                                                                                               df_rep.repAvgLongCurrentAccounts,
                                                                                               df_rep.avgMilesCurrentAccounts,
                                                                                               df_rep.repMaxMilesCurrentAccounts,
                                                                                               df_rep.repNumCurrAccounts,
                                                                                               df_rep.repNumCurrAccountsWithLatLong,
                                                                                               df_rep.repNumCurrAccountsWithValidLatLong)

            # scale CRI scores -----------------------------------------------------------------------------------------------------------------
            df_cri = df_cri.where((~isnan(df_cri.interactionRepAccount)) & (~isnan(df_cri.emailOpenScore)) & (
                ~isnan(df_cri.tenureScore)) & (~isnan(df_cri.visitScore)) & (~isnan(df_cri.cadenceScore)) & (
                                      ~isnan(df_cri.suggestionVisitScore)) & (~isnan(df_cri.suggestionEmailScore)) & (
                                      ~isnan(df_cri.targetAchievementScore)) & (~isnan(df_cri.channelScore)))

            w = Window.partitionBy("interactionYearMonth")
            scaled_result = (col("tenureScore") - min("tenureScore").over(w)) / (
                        max("tenureScore").over(w) - min("tenureScore").over(w))
            df_cri = df_cri.withColumn("tenureScoreStd", F.round(scaled_result, 2))

            scaled_result2 = (col("visitScore") - min("visitScore").over(w)) / (
                        max("visitScore").over(w) - min("visitScore").over(w))
            df_cri = df_cri.withColumn("visitScoreStd", F.round(scaled_result2, 2))

            scaled_result3 = (col("cadenceScore") - min("cadenceScore").over(w)) / (
                        max("cadenceScore").over(w) - min("cadenceScore").over(w))
            df_cri = df_cri.withColumn("cadenceScoreStd", 1 - F.round(scaled_result3, 2))

            scaled_result4 = (col("targetAchievementScore") - min("targetAchievementScore").over(w)) / (
                        max("targetAchievementScore").over(w) - min("targetAchievementScore").over(w))
            df_cri = df_cri.withColumn("targetAchievementScoreStd", F.round(scaled_result4, 2))

            df_cri = df_cri.select("interactionRepAccount", "interactionYearMonth", "emailOpenScore",
                                   "tenureScoreStd", "visitScoreStd", "cadenceScoreStd", "suggestionVisitScore",
                                   "suggestionEmailScore", "targetAchievementScoreStd", "channelScore")

            df_cri = df_cri.withColumn("nonNaCount", when(df_cri.emailOpenScore.isNotNull(), 1).otherwise(0) + when(
                df_cri.tenureScoreStd.isNotNull(), 1).otherwise(0) + when(df_cri.visitScoreStd.isNotNull(),
                                                                            1).otherwise(0) + when(
                df_cri.cadenceScoreStd.isNotNull(), 1).otherwise(0) + when(df_cri.suggestionVisitScore.isNotNull(),
                                                                             1).otherwise(0) + when(
                df_cri.suggestionEmailScore.isNotNull(), 1).otherwise(0) + when(
                df_cri.targetAchievementScoreStd.isNotNull(), 1).otherwise(0) + when(df_cri.channelScore.isNotNull(),
                                                                                        1).otherwise(0))

            df_cri = df_cri.withColumn("sumIndex", F.round(
                when(df_cri.emailOpenScore.isNull(), 0).otherwise(df_cri.emailOpenScore) + when(
                    df_cri.tenureScoreStd.isNull(), 0).otherwise(df_cri.tenureScoreStd) + when(
                    df_cri.visitScoreStd.isNull(), 0).otherwise(df_cri.visitScoreStd) + when(
                    df_cri.cadenceScoreStd.isNull(), 0).otherwise(df_cri.cadenceScoreStd) + when(
                    df_cri.suggestionVisitScore.isNull(), 0).otherwise(df_cri.suggestionVisitScore) + when(
                    df_cri.suggestionEmailScore.isNull(), 0).otherwise(df_cri.suggestionEmailScore) + when(
                    df_cri.targetAchievementScoreStd.isNull(), 0).otherwise(df_cri.targetAchievementScoreStd) + when(
                    df_cri.channelScore.isNull(), 0).otherwise(df_cri.channelScore), 2))
            df_cri = df_cri.withColumn("index",
                                       when(df_cri.nonNaCount > 2, F.round(df_cri.sumIndex / df_cri.nonNaCount, 2)).otherwise(
                                           lit(None)))

            df_cri = df_cri.select("interactionRepAccount", "interactionYearMonth", "emailOpenScore",
                                   "tenureScoreStd", "visitScoreStd", "cadenceScoreStd", "suggestionVisitScore",
                                   "suggestionEmailScore", "targetAchievementScoreStd", "channelScore", "sumIndex",
                                   "index")

            #df_cri.show()

            df_final = df_rc.join(df_cri, (df_rc.allRepAccount == df_cri.interactionRepAccount) & (
                        df_rc.allEffectiveyearmonth == df_cri.interactionYearMonth), 'left_outer').select(df_rc["*"],
                                                                                                            df_cri.emailOpenScore,
                                                                                                            df_cri.tenureScoreStd,
                                                                                                            df_cri.visitScoreStd,
                                                                                                            df_cri.cadenceScoreStd,
                                                                                                            df_cri.suggestionVisitScore,
                                                                                                            df_cri.suggestionEmailScore,
                                                                                                            df_cri.targetAchievementScoreStd,
                                                                                                            df_cri.channelScore,
                                                                                                            df_cri.sumIndex,
                                                                                                            df_cri.index).dropDuplicates()

            # x = df_final.select("Email_Email_Subject__c").distinct()
            # x = x.withColumn("Email_Subject_Id", monotonically_increasing_id())
            # df_final = df_final.join(x, (df_final.Email_Email_Subject__c == x.Email_Email_Subject__c), 'left_outer').select(
            #     df_final["*"], x["Email_Subject_Id"])
            df_final = df_final.withColumn("recordclassesId", df_final["recordclassId"])
            # rename final dataset column names according to master data dictionary
            logger.info("Renaming final dataset...")
            df_final = self.utils.renameFinalDataset(df_final, self.df_dict_pandas)
            logger.info("Finished renaming...")
            if batch:
                df_final.repartition(40).write.mode('overwrite').partitionBy("effectiveYear","recordclassId").format("delta").save(
                self.s3_destination + "data/silver/final_dataset/")
            else:
                df_final.persist(pyspark.StorageLevel.MEMORY_ONLY)
                logger.info("final recordclasses size " + str(df_final.count()))
                broadcast(df_final)
                self.utils.upsertRecordClass(df_final, self.s3_destination + "data/silver/final_dataset/", self.DeltaTable)
        except Exception as e:
            logger.error("Error in finalDataTrans: {}".format(e))
            raise
