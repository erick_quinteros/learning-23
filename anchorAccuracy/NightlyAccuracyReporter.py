import pandas as pd
from datetime import datetime

from anchorAccuracy.AccuracyReportDataLoader import AccuracyReportDataLoaderDriver
from anchorAccuracy.AccuracyReportCalculator import AccuracyReportCalculator
from anchorAccuracy.AccuracyReportSaver import AccuracyReportSaver
from common.pyUtils.logger import get_module_logger

logger = get_module_logger(__name__)


class NightlyAccuracyReporter:
    """
    object to handle accuracy report for nightly run
    """

    def __init__(self, conn_pool):
        self.is_nightly = True
        self.run_startDate = None
        self.run_endDate = None
        self.anchor_runDate = None
        self.conn_pool = conn_pool
        self.predict_df = None
        self.visit_df = None
        self.suggest_df = None
        self.predict_facility_df = None
        self.nightly_prediction_count = None
        self.run_uid = None
        self.build_uid = None
        self.version_uid = None
        self.accuracy_result = None

    def load_data(self):
        data_loader = AccuracyReportDataLoaderDriver(self.conn_pool, is_nightly=self.is_nightly, start_date=self.run_startDate, end_date=self.run_endDate).run()
        self.predict_df = data_loader.predict_df
        self.visit_df = data_loader.visit_df
        self.suggest_df = data_loader.suggest_df
        self.predict_facility_df = data_loader.predict_facility_df
        self.nightly_prediction_count = data_loader.nightly_prediction_count
        self.run_uid = data_loader.nightly_run_uid
        self.build_uid = data_loader.nightly_build_uid
        self.version_uid = data_loader.nightly_version_uid
        # get anchor runDate
        self.anchor_runDate = (datetime.strptime(data_loader.start_date, "%Y-%m-%d")-pd.tseries.offsets.BDay(1)).strftime("%Y-%m-%d")
        self.run_startDate = data_loader.start_date
        self.run_endDate = data_loader.end_date

    def analyze_accuracy(self):
        self.accuracy_result = AccuracyReportCalculator(self.is_nightly, self.predict_df, self.visit_df, self.suggest_df, self.predict_facility_df).run().result

    def save_accuracy(self):
        AccuracyReportSaver(self.conn_pool, is_nightly=self.is_nightly, start_date=self.run_startDate, end_date=self.run_endDate, accuracy_result=self.accuracy_result, nightly_prediction_count=self.nightly_prediction_count, run_uid=self.run_uid, build_uid=self.build_uid, version_uid=self.version_uid).run()

    def run(self):
        logger.info("Start Running NightlyAccuracyReporter")
        self.load_data()
        self.analyze_accuracy()
        self.save_accuracy()
        logger.info("Finish Running NightlyAccuracyReporter for anchor running on {}".format(self.anchor_runDate))