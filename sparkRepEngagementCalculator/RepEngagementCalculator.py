import os
import sys
import logging
import datetime

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)

from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from pyspark.sql.functions import year, month, udf, countDistinct, sum, col, trim, min, unix_timestamp, from_unixtime
from pyspark import SparkContext
from datetime import date, datetime, timedelta
from dateutil.relativedelta import *
from common.pyUtils.logger import get_module_logger
from common.DataAccessLayer.SparkDataAccessLayer import initialize, CommonDbAccessor, SparkDSEAccessor, SparkLearningAccessor, SparkCSAccessor, SparkStageAccessor
from common.DataAccessLayer.DataAccessLayer import connect_learning_database, connect_dse_database
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.logger.Logger import create_logger


logger = get_module_logger("REP_ENGAGEMENT_CALCULATION")


# Global variable
spark = None
numPartitions = 64


class RepEngagementCalculator:
    """
    This class is responsible for computing rep engagement probability
    """

    def __init__(self, learning_home_dir, database_config):
        logger.info("Initializing")
        self.learning_home_dir = learning_home_dir
        self.database_config = database_config

        self.dse_db_name = database_config.dse_db_name
        self.db_learning = database_config.learning_db_name

        self.dse_accessor = SparkDSEAccessor()
        self.learning_accessor = SparkLearningAccessor()
        self.cs_accessor = SparkCSAccessor()
        self.stage_accessor = SparkStageAccessor()

    def format_dates(self, today, date_past):
        date_past_month = date_past.strftime("%m")
        date_past_year = date_past.strftime("%Y")
        today_format = today.strftime("%Y-%m-%d")
        date_past_format = "{past_year}-{past_month}-01".format(past_year=date_past_year, past_month=date_past_month)
        start_date_string = " where startDateLocal >= '{START}'".format(START=date_past_format)
        end_date_string = " and startDateLocal <= '{END}'".format(END=today_format)
        published_suggestions_condition = "WHERE DR.startDateLocal BETWEEN '{START} 00:00:00' AND '{END} 23:59:59' ".format(START=date_past_format, END=today_format)
        synched_Suggestions_condition = "AND A.Sync_Completed_Datetime_vod__c > '{START} 00:00:00'".format(START=date_past_format)
        logger.debug("Start date={START_DATE} and end date={END_DATE} to run rep engagement".format(START_DATE=date_past_format, END_DATE=today_format))
        return start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition

    def get_initial_tables(self, published_suggestions_condition):
        #Gets DSE Run, DSE Run Rep Date, Spark DSE Run, and Spark DSE Run Rep Date tables
        dse_run_df = self.dse_accessor.get_run_df(published_suggestions_condition)
        logger.info("Success fetching DSE RUN Table")
        dseRunRepDate_df = self.dse_accessor.get_run_rep_date_df()
        logger.info("Success fetching DSERunRepDate Table")
        spark_dse_run_df = self.dse_accessor.get_spark_run_df(published_suggestions_condition)
        logger.info("Success fetching Spark DSE RUN Table")
        spark_dse_run_rep_Date_df = self.dse_accessor.get_spark_run_rep_date_df()
        logger.info("Success fetching SparkDSERunRepDate Table")
        return dse_run_df, dseRunRepDate_df, spark_dse_run_df, spark_dse_run_rep_Date_df

    def join_DSErun_DSErunrepdate(self, dserun_df, dseRunRepDate_df):
        logger.info("Started joining DSERun and DSERunRepDate")
        publishedDate = udf(lambda startDateTime: startDateTime.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime) else None)
        nextPublishedDate = udf(lambda startDateTime_old, startDateTime_new: startDateTime_new.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime_old < startDateTime_new) else None)
        final_df_one = dserun_df.alias("a").join(dseRunRepDate_df.alias("b"), on=['runId']).groupBy("a.runId", "a.runUID", "a.startDateLocal", "b.repId", "a.startDateTime")\
            .agg(publishedDate("a.startDateTime").alias("publishedDate"))
        logger.info("Success joining DSERun and DSERunRepDate")
        return final_df_one, nextPublishedDate

    def calculate_next_published_date(self, final_df_one, is_spark, nextPublishedDate):
        #Determines next published date based on the state and publish dates
        logger.info("Calculating next Published date")
        final_df_two = final_df_one
        cond = [final_df_one.repId == final_df_two.repId]
        if is_spark:
            nextPublished = final_df_one.alias("a").join(final_df_two.alias("b"), cond, how="left").groupBy("a.repId", "a.runUID", "a.runId", "a.publishedDate", "a.startDateLocal").agg(min(nextPublishedDate("a.startDateTime", "b.startDateTime")).alias("nextPublishedDate"))
        else:
            nextPublished = final_df_one.alias("a").join(final_df_two.alias("b"), cond, how="left").groupBy("a.repId", "a.runId", "a.runUID", "a.publishedDate", "a.startDateLocal").agg(min(nextPublishedDate("a.startDateTime", "b.startDateTime")).alias("nextPublishedDate"))
        logger.info("Success next Published date")
        return nextPublished

    def join_spark_DSErun_DSErunrepdate(self, dserun_df, dseRunRepDate_df):
        logger.info("Started joining SparkDSERun and SparkDSERunRepDate")
        publishedDate = udf(lambda startDateTime: startDateTime.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime) else None)
        nextPublishedDate = udf(lambda startDateTime_old, startDateTime_new: startDateTime_new.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime_old < startDateTime_new) else None)
        final_df_one = dserun_df.alias("a").join(dseRunRepDate_df.alias("b"), on=['runUID']).groupBy("a.runUID", "a.runId", "b.repId", "a.startDateLocal", "a.startDateTime")\
            .agg(publishedDate("a.startDateTime").alias("publishedDate"))
        logger.info("Success joining SparkDSERun and SparkDSERunRepDate")
        return final_df_one, nextPublishedDate

    def merge_published_dates(self, nextPublished, nextPublished_spark):
        logger.info("Merging both spark and non-spark next published dates")
        nextPublished_total = nextPublished.union(nextPublished_spark)
        logger.info("Success merging both spark and non-spark next published dates")
        return nextPublished_total

    def get_rep_table(self):
        logger.info("Getting Rep Table")
        rep_df = self.dse_accessor.get_rep_df()
        logger.info("Success fetching Rep Table")
        return rep_df

    def merge_next_published_with_rep(self, next_published_tot, rep_df):
        logger.info("Merging next published dates with REP table")
        next_published_tot = next_published_tot.alias("a").join(rep_df.alias("b"), on="repId", how="left").select("a.repId", "a.runId", "a.runUID", "a.publishedDate", "a.nextPublishedDate", "b.externalId", "a.startDateLocal")
        logger.info("Success merging next published dates with REP table")
        return next_published_tot

    def get_sync_info(self, synched_Suggestions_condition):
        logger.info("Getting sync info Table")
        sync_tracking_df = self.cs_accessor.get_sync_info_table(synched_Suggestions_condition)
        sync_tracking_df.cache()
        logger.info("Success fetching sync info Table")
        return sync_tracking_df

    def merge_sync_info_with_externalID(self, next_published_tot, sync_tracking_df):
        logger.info("Merging sync info and externalID")
        externalId_df = next_published_tot.select('externalId').distinct()
        cond = [sync_tracking_df.OwnerId == externalId_df.externalId]
        sync_info_df = sync_tracking_df.alias("a").join(externalId_df.alias("b"), cond, how="left").select("a.OwnerId", "a.CreatedDate")
        sync_info_df = sync_info_df.withColumnRenamed("OwnerId", "externalId")
        sync_info_df = sync_info_df.withColumnRenamed("CreatedDate", "syncStart")
        logger.info("Success merging sync info and externalID")
        return sync_info_df

    def merge_published_suggestions_with_syncs(self, next_published_tot, sync_info_df):
        logger.info("Merging Published suggestions and syncs")
        sync_info_df = sync_info_df.select('externalId', from_unixtime(unix_timestamp('syncStart', '%Y-%m-%d %H:%M:%S')).alias('syncStart'))
        sync_info_df.createOrReplaceTempView("sync_data")
        next_published_tot.createOrReplaceTempView("published_info")
        sqlContext = SQLContext(spark)
        synched_runs_df = sqlContext.sql("SELECT PS.runId as runId, PS.repId as repId, PS.startDateLocal as startDateLocal, \
        SUM(CASE WHEN RS.SyncStart BETWEEN PS.publishedDate AND PS.nextPublishedDate THEN 1 ELSE 0 END) Times_Synched \
        FROM published_info PS LEFT JOIN sync_data RS ON PS.externalId = RS.externalId GROUP BY PS.repId, PS.runId, PS.startDateLocal")
        logger.info("Success Merging Published suggestions and syncs :" + str(synched_runs_df.count()))
        return synched_runs_df


    def initialize_df(self):
        #Connects to db, selects DSE data, and merges df to prepare for calculations

        logger.info("Started RepEngagement Calculation")
        today = date.today()
        date_past = today + relativedelta(years=-2)
        date_past = date_past.replace(day=1)
        start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition = self.format_dates(today, date_past)
        repEngagement_df = self.learning_accessor.get_rep_engagement_table()
        if repEngagement_df.count() > 0:
            logger.info("Engagement Calculation on last 2 months of data")
            date_past = today + relativedelta(months=-2)
            date_past = date_past.replace(day=1)
            start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition = self.format_dates(today, date_past)
        dse_run_df, dseRunRepDate_df, spark_dse_run_df, spark_dseRunRepDate_df = self.get_initial_tables(published_suggestions_condition)
        final_df, nextPublishedDate = self.join_DSErun_DSErunrepdate(dse_run_df, dseRunRepDate_df)
        next_published = self.calculate_next_published_date(final_df, 0, nextPublishedDate)
        spark_final_df, nextPublishedDate = self.join_spark_DSErun_DSErunrepdate(spark_dse_run_df, spark_dseRunRepDate_df)
        spark_next_published = self.calculate_next_published_date(spark_final_df, 1, nextPublishedDate)
        next_published_tot = self.merge_published_dates(next_published, spark_next_published)
        rep_df = self.get_rep_table()
        next_published_tot = self.merge_next_published_with_rep(next_published_tot, rep_df)
        sync_tracking_df = self.get_sync_info(synched_Suggestions_condition)
        sync_info_df = self.merge_sync_info_with_externalID(next_published_tot, sync_tracking_df)
        synched_runs_df = self.merge_published_suggestions_with_syncs(next_published_tot, sync_info_df)
        return today, date_past, start_date_string, end_date_string, repEngagement_df, synched_runs_df

    def get_AKT_RepLicense_arc(self):
        logger.info("Getting AKT_RepLicense_arc")
        rep_license_arc_df = self.stage_accessor.get_AKT_rep_license_arc()
        return rep_license_arc_df

    def get_rpt_dim_calendar(self):
        logger.info("Getting rpt_dim_calendar")
        dim_calendar_df = self.stage_accessor.get_rpt_dim_calendar()
        return dim_calendar_df

    def get_rpt_Suggestion_Delivered_stg(self, today, date_past, start_date_string, end_date_string, synched_runs_df):
        logger.info("Getting RPT_Suggestion_Delivered_stg")
        suggestion_Delivered_df_raw = self.stage_accessor.get_rpt_Suggestion_Delivered_stg(today, date_past)
        suggestion_Delivered_df_raw.cache()
        logger.info("Success fetching RPT_Suggestion_Delivered_stg :" + str(suggestion_Delivered_df_raw.count()))

        logger.info("Start eliminating non-synched runIds per rep")
        sync_final = synched_runs_df.where(synched_runs_df.Times_Synched > 0)
        suggestion_Delivered_df = suggestion_Delivered_df_raw.alias("a").join(sync_final.alias("b"), on=['repId', 'runId', 'startDateLocal'])
        suggestion_Delivered_df_raw.unpersist()
        suggestion_Delivered_df.cache()
        suggestion_Delivered_df_final = suggestion_Delivered_df.filter(suggestion_Delivered_df.Times_Synched.isNotNull())
        logger.info("Success eliminating non-synched runIds per rep :" + str(suggestion_Delivered_df_final.count()))
        return suggestion_Delivered_df_final

    def merge_suggestions_delivered_with_AKT_RepLicense_arc(self, suggestion_Delivered_df, repLicenseTable_df):
        logger.info("Merging Suggestion delivered stage table & AKT_RepLicense_arc table")
        cond = [suggestion_Delivered_df.repUID == repLicenseTable_df.externalId,
                suggestion_Delivered_df.startDateLocal >= repLicenseTable_df.startDate,
                suggestion_Delivered_df.startDateLocal <= repLicenseTable_df.endDate]
        SuggDel_RepLicence_df = suggestion_Delivered_df.alias("a").join(repLicenseTable_df.alias("b"), cond).select(
            col("a.startDateLocal"), col("a.repUID"), col("a.repName"), col("b.cluster"), col("a.seConfigId"),
            col("a.seConfigName"), col("a.suggestionDriver"), col("a.suggestionReferenceId"), col("b.territoryId"),
            col("b.territoryName"), col("a.suggestedDate"), col("a.actionTaken"))
        logger.info("Merge Success Suggestion delivered stage table & AKT_RepLicense_arc table")
        return SuggDel_RepLicence_df

    def merge_suggestions_delivered_RepLicense_with_rpt_dim(self, SuggDel_RepLicence_df, dim_calendar_df):
        logger.info("Merging Suggestion_delivered_RepLicence & rpt_dim_calendar table")
        cond = [SuggDel_RepLicence_df.startDateLocal == dim_calendar_df.datefield]
        suggestions_df = SuggDel_RepLicence_df.join(dim_calendar_df, cond, how='left')
        logger.info("Merge Success Suggestion_delivered_RepLicence & rpt_dim_calendar table")
        return suggestions_df

    def filter_holidays(self, suggestions_df):
        #Removes actions not taken on holidays and weekends
        logger.info("Filtering out holidays, weekends table and action not taken")
        suggestions_df = suggestions_df[((suggestions_df['is_holiday'] != 1) | (suggestions_df['is_weekend'] != 1)) & (suggestions_df['actionTaken'] != 'No Action Taken')]
        logger.info("Success Filtering out holidays, weekends table and action not taken")
        return suggestions_df

    def finalize_df(self, today, date_past, start_date_string, end_date_string, synched_runs_df):
        #Determine suggestions delivered, accepted, and not acted on to calculate engagement

        repLicenseTable_df = self.get_AKT_RepLicense_arc()
        dim_calendar_df = self.get_rpt_dim_calendar()
        suggestion_Delivered_df = self.get_rpt_Suggestion_Delivered_stg(today, date_past, start_date_string, end_date_string, synched_runs_df)
        SuggDel_RepLicence_df = self.merge_suggestions_delivered_with_AKT_RepLicense_arc(suggestion_Delivered_df, repLicenseTable_df)
        suggestions_df = self.merge_suggestions_delivered_RepLicense_with_rpt_dim(SuggDel_RepLicence_df, dim_calendar_df)
        suggestions_df = self.filter_holidays(suggestions_df)

        sqlContext = SQLContext(spark)

        suggestions_df = suggestions_df.repartition("seConfigId", "suggestionDriver")

        suggestions_df.createOrReplaceTempView("suggestions")

        engagements = sqlContext.sql("select year(`RPT`.`startDateLocal`) as Year,\
                month(`RPT`.`startDateLocal`) as Month,\
                `RPT`.`repUID` as repUID,\
                `RPT`.`repName` as repName,\
                ucase(`RPT`.`cluster`) as repTeamUID,\
                ucase(`RPT`.`cluster`) as repTeamName,\
                `RPT`.`territoryId` as territoryId, \
                if((ucase(`RPT`.`cluster`) = 'GEWH'), `RPT`.`territoryName`, concat(`RPT`.`territoryName`, '-', `RPT`.`repName`)) as territoryName,\
                `RPT`.`seConfigId` as seConfigId,\
                `RPT`.`seConfigName` as seConfigName,\
                `RPT`.`suggestionDriver` as suggestionType,\
                COUNT(DISTINCT IF(actionTaken IN ('Suggestions Dismissed', 'Suggestions Completed'), `RPT`.`suggestionReferenceId`, NULL)) as engagedUniqueSuggestionsCount \
                from suggestions RPT GROUP BY `RPT`.`repUID`, year(`RPT`.`startDateLocal`), month(`RPT`.`startDateLocal`), `RPT`.`seConfigId`,\
                `RPT`.`suggestionDriver`, `RPT`.`repName`, `RPT`.`seConfigName`, `RPT`.`cluster`, `RPT`.`territoryId`, `RPT`.`territoryName`")

        engagements.createOrReplaceTempView("engagements")

        logger.info("Engagement - Engagement Calculation Done")

        suggestions_delivered = sqlContext.sql("SELECT Year, Month, repUID, repName, repTeamUID, repTeamName, territoryId, territoryName, \
                seConfigId, seConfigName, suggestionType, sum(SuggestionsDelivered) as TotalSuggestionsDeliveredTimes \
                FROM (select year(`RPT`.`startDateLocal`) as Year,\
                month(`RPT`.`startDateLocal`) as Month,\
                `RPT`.`repUID` as repUID,\
                `RPT`.`repName` as repName,\
                ucase(`RPT`.`cluster`) as repTeamUID,\
                ucase(`RPT`.`cluster`) as repTeamName,\
                `RPT`.`territoryId` as territoryId,\
                if((ucase(`RPT`.`cluster`) = 'GEWH'), `RPT`.`territoryName`, concat(`RPT`.`territoryName`, '-', `RPT`.`repName`)) as territoryName,\
                `RPT`.`seConfigId` as seConfigId,\
                `RPT`.`seConfigName` as seConfigName,\
                `RPT`.`suggestionDriver` as suggestionType,\
                `RPT`.`suggestionReferenceId`,\
                COUNT(DISTINCT `RPT`.`suggestedDate`) as SuggestionsDelivered \
                from suggestions RPT group by `RPT`.`suggestionReferenceId`, year(`RPT`.`startDateLocal`), month(`RPT`.`startDateLocal`), `RPT`.`seConfigId`,\
                `RPT`.`cluster`, `RPT`.`territoryId`, `RPT`.`territoryName`, \
                `RPT`.`suggestionDriver`, `RPT`.`repUID`, `RPT`.`repName`, `RPT`.`seConfigName`) inn group by Year, Month, repName, repUID, seConfigId, suggestionType, \
                repTeamUID, repTeamName, territoryId, territoryName, seConfigName")

        suggestions_delivered.createOrReplaceTempView("suggestions_delivered")

        logger.info("Total Suggestions Delivered Calculation Done")

        output = sqlContext.sql("select e.`Year` as year,e.`Month` as month,e.`repUID` as repUID,e.`repName` as repName, e.`repTeamUID` as repTeamUID,\
                e.`repTeamName`, e.`territoryId`, e.`territoryName`,\
                e.`seConfigId` as seConfigId,e.`seConfigName` as seConfigName,e.`suggestionType` as suggestionType,e.`engagedUniqueSuggestionsCount` as engagedUniqueSuggestionsCount,\
                s.`TotalSuggestionsDeliveredTimes` as TotalSuggestionsDeliveredTimes \
                from engagements e  join suggestions_delivered s on (e.repUID = s.repUID AND e.repName = s.repName AND e.Year = s.Year AND e.Month = s.Month AND e.seConfigId = s.seConfigId AND \
                e.suggestionType = s.suggestionType AND e.seConfigName = s.seConfigName AND e.territoryId = s.territoryId AND e.repTeamUID = s.repTeamUID)")

        logger.info("Calculation Done Successful")
        return output

    def truncating_data(self, repEngagement_df, today):
        logger.info("Started truncating last 2-3 months of data")
        if repEngagement_df.count() > 0:
            connection = connect_learning_database()
            cursor = connection.cursor(buffered=True)
            for i in range(3):
                date_past = today + relativedelta(months=-i)
                month = date_past.strftime("%m")
                year = date_past.strftime("%Y")
                logger.debug("Deleting {MONTH}, {YEAR}".format(MONTH=month, YEAR=year))
                sql_query = "DELETE FROM {DB_NAME}.RepEngagementCalculation WHERE month = '{MONTH}' AND year = '{YEAR}'".format(DB_NAME=self.db_learning, MONTH=month, YEAR=year)
                cursor.execute(sql_query)
                print(sql_query)
            connection.commit()
            cursor.close()
            connection.close()
        logger.info("Truncating Done")

    def populating_RepEngagementTable(self, final_df):
        #Populates db with truncated data
        logger.info("Now populating rep engagement calculations to RepEngagementTable")
        self.learning_accessor.write_rep_engagement_table(final_df)
        logger.info("DB Populating complete")

    def populate_db(self, today, repEngagement_df, final_df):
        #Truncates and populates db

        logger.info("Rep Engagement Calculation complete")
        self.truncating_data(repEngagement_df, today)
        self.populating_RepEngagementTable(final_df)

    def update_ChannelActionMap(self):

        logger.info("Now updating ChannelActionMap")

        connection = connect_dse_database()

        cursor = connection.cursor(buffered=True)

        sql_query_template = "UPDATE {tableName_channelActionMap}" \
                             " SET appearsInInteractionData = 1" \
                             " WHERE repActionTypeId IN (SELECT DISTINCT repActionTypeId FROM {tableName_interaction})"


        tableName_channelActionMap = "{db_learning}.ChannelActionMap".format(db_learning=self.db_learning)
        tableName_interaction = "{db_dse}.Interaction".format(db_dse=self.dse_db_name)

        sql_query = sql_query_template.format(tableName_channelActionMap=tableName_channelActionMap,
                                              tableName_interaction=tableName_interaction)

        logger.info("SQL to update ChannelActionMap: {sql_query}".format(sql_query=sql_query))

        cursor.execute(sql_query)
        connection.commit()
        cursor.close()
        connection.close()
        logger.info("ChannelActionMap has been updated")

    def calculate(self):
        """
            This function gives engagement level for all reps, territories,
            suggestionType, configs, as well as update the Channel Action Map
            :return:
        """
        today, date_past, start_date_string, end_date_string, repEngagement_df, synched_runs_df = self.initialize_df()
        final_df = self.finalize_df(today, date_past, start_date_string, end_date_string, synched_runs_df)
        self.populate_db(today, repEngagement_df, final_df)
        self.update_ChannelActionMap()

def main():
    global spark
    global numPartitions

    # Validate the input argument to the script
    if len(sys.argv) != 12:
        logger.error("Invalid arguments to the optimizer script")
        exit(0)

    # Retrieve the arguments
    db_host = sys.argv[1]
    db_user = sys.argv[2]
    db_password = sys.argv[3]
    dse_db_name = sys.argv[4]
    cs_db_name = sys.argv[5]
    stage_db_name = sys.argv[6]
    learning_db_name = sys.argv[7]
    db_port = sys.argv[8]  # Update to 33066 for testing on Local machine
    learning_home_dir = sys.argv[9]
    customer = sys.argv[10]
    environment = sys.argv[11]

    # Get the singleton instance of the database config and set the properties

    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name, stage_db_name)

    # Create spark context
    spark = SparkSession.builder.appName("RepEngagementCalculators").getOrCreate()

    initialize("RepEngagementCalculatorLogger", spark)
    RepEngagementCalculator(learning_home_dir, database_config).calculate()

if __name__ == "__main__":
    main()
