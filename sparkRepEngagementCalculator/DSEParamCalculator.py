import pandas as pd
import json
import math
from datetime import datetime
from common.pyUtils.logger import get_module_logger
from common.pyUtils.learning_api_connector import LearningApiConnector

logger = get_module_logger(__name__)


def normal_round(n, precision):
    multiplier = 10 ** precision
    logger.info(n * multiplier - math.floor(n * multiplier))
    if n * multiplier - math.floor(n * multiplier) < 0.5:
        return math.floor(n * multiplier) / (multiplier * 1.0)
    return math.ceil(n * multiplier) / (multiplier * 1.0)

def get_engagement_query_params(engagement_filters_row):
    query_params = ""  # conditionally add query params if they are determined in db row
    if engagement_filters_row.get('suggestionType') != "both": query_params = query_params + "&suggestionType={suggestionType}".format(suggestionType=engagement_filters_row.get('suggestionType'))
    if engagement_filters_row.get( 'repTeamUIDs') is not None: query_params = query_params + "&repTeamUID={repTeamUIDs}".format(repTeamUIDs=engagement_filters_row.get('repTeamUIDs'))
    if engagement_filters_row.get('territoryIds') is not None: query_params = query_params + "&territoryUID={territoryIds}".format(territoryIds=engagement_filters_row.get('territoryIds'))
    if engagement_filters_row.get('startYear') is not None and engagement_filters_row.get('startMonth') is not None:
        today = datetime.today()
        query_params = query_params + "&endYear={endYear}&endMonth={endMonth}".format(endYear=today.year,endMonth=today.month)
        query_params = query_params + "&startYear={startYear}".format(startYear=int(engagement_filters_row.get('startYear')))
        query_params = query_params + "&startMonth={startMonth}".format(startMonth=int(engagement_filters_row.get('startMonth')))
    if query_params == "": return query_params
    return query_params.replace("&", "?", 1)  # replace first & with ? if there are query params

def compute_threshold(df, rep_threshold_type, bottom_percentile):
    logger.info("computing threshold, threshold type is: {rep_threshold_type}".format(rep_threshold_type=rep_threshold_type))
    threshold = None;
    if rep_threshold_type == "mean": threshold =  df["performance"].mean()
    if rep_threshold_type == "median": threshold =  df["performance"].median()
    if rep_threshold_type == "mode": threshold =  df["performance"].mode().iloc[-1] # returns list of all modes, get max mode
    if rep_threshold_type == "bottom": threshold =  df["performance"].quantile(bottom_percentile / 100, interpolation='lower')
    logger.info("Computed threshold is {threshold}".format(threshold=normal_round(threshold, 2)))
    return (normal_round(threshold, 2)) # round this

def compute_whitelist(df, threshold):
    if threshold >= df["performance"].max(): return ";".join(df["repUID"].values.tolist()) # edge case
    return";".join(df[df["performance"] < threshold]["repUID"].values.tolist())


class DSEParamCalculator:
    """
    This class is responsible for computing recomputing DSE engagement parameters
    """

    def __init__(self, customer, environment, learning_home_dir):
        self.learning_api_connector = LearningApiConnector(customer, environment, learning_home_dir)
        self.reps_df = None
        self.engagement_filters_df = None
        self.temp_se_config_id = None
        self.temp_engagement_filters_group = None
        self.temp_engagement_data_dict = {}

    def _fetch_engagement_filters(self):
        url_suffix = "RepEngagementCalculation/engagementFiltersForDSESettings"
        json_string = self.learning_api_connector.make_get_call(url_suffix)
        if json_string is None or json_string == "[]":
            logger.info("No engagement filters loaded, exiting")
            exit(0)
        engagement_filters_df = pd.DataFrame(json.loads(json_string))
        self.engagement_filters_df = engagement_filters_df.replace({pd.np.nan: None})
        logger.info("Fetched {rows} sets of engagement filters".format(rows=self.engagement_filters_df.shape[0]))

    def _fetch_reps(self):
        logger.info("loading reps from /DSE/reps api")
        url_suffix = "DSE/reps"
        json_string = self.learning_api_connector.make_get_call(url_suffix)
        if json_string is None or json_string == "[]":
            logger.info("No reps loaded, exiting")
            exit(0)
        self.reps_df = pd.DataFrame(json.loads(json_string))[["repUID"]]
        logger.info("{count} reps loaded".format(count=self.reps_df.shape[0]))

    def _fetch_engagement_data(self):
        logger.info("Using learning api to fetch rep engagement calculation data")
        url_suffix_base =  "RepEngagementCalculation/{seConfigId}/calculation".format(seConfigId=self.temp_se_config_id)
        self.temp_engagement_data_dict.clear()
        for index, row in self.temp_engagement_filters_group.head().iterrows():
            logger.info("Fetching data for seConfigId {se_config_id} and suggestionType {suggestionType}".format(se_config_id=self.temp_se_config_id, suggestionType=row["suggestionType"]))
            json_string = self.learning_api_connector.make_get_call(url_suffix_base + get_engagement_query_params(row))
            if json_string is None or json_string == "[]":
                logger.info("No calculated engagement data for seConfigId {se_config_id} and suggestionType {suggestionType}, continueing".format(se_config_id=self.temp_se_config_id, suggestionType=row["suggestionType"]))
                continue
            json_data = json.loads(json_string)
            engagement_data_df = pd.DataFrame(json_data)
            engagement_data_df['performance'] = engagement_data_df['performance'].apply(lambda x: normal_round(x, 2))
            engagement_data_df = engagement_data_df.merge(self.reps_df, how="inner", on="repUID")
            logger.info("fetched engagement calculations for {number_reps} reps".format(number_reps=engagement_data_df.shape[0]))
            self.temp_engagement_data_dict[row["suggestionType"]] = (engagement_data_df, row["repThresholdType"], row["bottomPercentile"])

    def _calculate_dse_params_and_save(self):
        target_threshold = trigger_threshold  = whitelist = None
        if "both" in self.temp_engagement_data_dict:
            trigger_threshold = target_threshold = compute_threshold(*self.temp_engagement_data_dict.get("both"))
            whitelist = compute_whitelist(self.temp_engagement_data_dict.get("both")[0], trigger_threshold)
        if "target" in self.temp_engagement_data_dict:
            target_threshold = compute_threshold(*self.temp_engagement_data_dict.get("target"))
            whitelist = compute_whitelist(self.temp_engagement_data_dict.get("target")[0], target_threshold)
        if "trigger" in self.temp_engagement_data_dict:
            trigger_threshold = compute_threshold(*self.temp_engagement_data_dict.get("trigger"))
        dse_params_dict = {}
        if target_threshold is not None: dse_params_dict["engagementPredictionProbabilityThreshold"] = str(target_threshold)
        if trigger_threshold is not None: dse_params_dict["engagementPredictionForTriggerProbabilityThresholds"] = ','.join([str(trigger_threshold)] * 3)
        if whitelist is not None: dse_params_dict["engagementPredictionRepUIDList"] = whitelist
        if not dse_params_dict:
            logger.info("No dse params to save, moving to next config")
            return
        logger.info("Saving dse parameters. Json body is: {json_body}".format(json_body=json.dumps(dse_params_dict)))
        url_suffix = "DSE/DSEConfig/" + str(self.temp_se_config_id) + "/globalParameters"
        response = self.learning_api_connector.make_post_call(url_suffix, dse_params_dict)
        if response is None: logger.error("API call failed with, check learning service and aktana service health, and ensure config exists and is not locked")

    def run(self):
        self._fetch_engagement_filters()
        self._fetch_reps()
        for groupName, group in self.engagement_filters_df.groupby('seConfigId'): # group by config, compute all dse params for config in single iteration
            logger.info("Beginning calculation of DSE params for config: {config_id}".format(config_id=groupName));
            self.temp_se_config_id = groupName
            self.temp_engagement_filters_group = group
            self._fetch_engagement_data()
            self._calculate_dse_params_and_save()
