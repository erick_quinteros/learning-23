import logging
import os
import sys

import json
import mysql.connector
from mysql.connector import Error

class aktana_ml_utils:

    """
    PURPOSE:
        This is an utility class for ML modules at Aktana that use Databricks, Snowflake, ADL
    """

    def __init__(self, p_log_file_name = None):

        """
        PURPOSE:
            This does any required initialization steps, which in this class is
            basically just turning on logging.
        """

        self.cmdline_params = {}
        self.metadata_params = {}
        self.snowflake_params = {}
        self.adl_params = {}
        self.databricks_params = {}

        file_name = p_log_file_name
        if file_name is None:
            file_name = '/tmp/aktana_ml_utils.log'

        # -- (> ---------- SECTION=begin_logging -----------------------------
        logging.basicConfig(
            filename=file_name,
            level=logging.INFO)
        # -- <) ---------- END_SECTION ---------------------------------------


    # -- (> ---------------------------- SECTION=initialize ------------------------
    def initialize(self, argv):

        """
        PURPOSE:
            Read appropriate metadata for the client
                * Parse command-line to read cust/env/region
                * Read Snowflake config for cust/env
                * Read Databricks config for cust/env
                * Read ADL config for cust/env
        """

        # if it is already initialized, no need to read again
        if len(self.cmdline_params) > 0:
            return

        # Get the customer, env and region from the command line.
        self.get_cmdline_params(argv)

        # Parameter added to enable testing of the driver program's signature without actually doing the work
        if "test" in self.cmdline_params:
            print("Test mode.  Exiting...")
            exit(1)

        # Read metadata db info from config json
        self.get_metadata_params()

        return (self.cmdline_params, self.metadata_params)

    # -- <) ---------------------------- END_SECTION=initialize --------------------

    def get_cmdline_params(self, args):

        """
        PURPOSE:
            Read the command-line arguments and store them in a dictionary.
            Command-line arguments should come in pairs, e.g.:
                "--customer abc"
        INPUTS:
            The command line arguments (sys.argv).
        RETURNS:
            Returns the dictionary.
        DESIRABLE ENHANCEMENTS:
            Improve error detection and handling.
        """

        self.cmdline_params = {}

        i = 1
        while i < len(args) - 1:
            property_name = args[i]
            # Strip off the leading "--" from the tag, e.g. from "--user".
            property_name = property_name[2:]
            property_value = args[i + 1]
            self.cmdline_params[property_name] = property_value
            i += 2

        return self.cmdline_params


    def get_metadata_params(self):

        """
        PURPOSE:
            Read metadata config file based on region to read metadata db params
        INPUTS:
            The dictionary of command line params
        RETURNS:
            Returns the metadata params from the json config file as a dictionary.
        DESIRABLE ENHANCEMENTS:
            Improve error detection and handling.
        """

        if "customer" not in self.cmdline_params or "env" not in self.cmdline_params or "app" not in self.cmdline_params:
            print("--customer, --env and --app are required in commandline")
            exit(1)

        customer = self.cmdline_params["customer"]
        environment = self.cmdline_params["env"]

        try:
            region = self.cmdline_params["region"]
        except:
            region = ""
            self.cmdline_params["region"] = region
            
        # get params to connect to metadata db in mysql
        metadataFilePath = self.get_metadata_config_filename(region)
        print("Reading metadata db conn-params for region={} from {}".format(region, metadataFilePath))
        with open(metadataFilePath, 'r') as f:
            self.metadata_params = json.load(f)
        
        # Set the default mysql port, if not explicitly specified
        if "port" not in self.metadata_params:
            self.metadata_params["port"] = 3306

        return self.metadata_params

    def get_metadata_config_filename(self, region):
        filePath = os.path.dirname(os.path.abspath(__file__))
        defaultMetadataFile = 'customer-metadata.json'
        if region != "":
            metadataFile = filePath + "/" + region + '.' + defaultMetadataFile
            if os.path.isfile(metadataFile):
                return metadataFile
        return filePath + "/" + defaultMetadataFile

    def get_snowflake_metadata(self):

        """
        PURPOSE:
            Read Aktana metadata for Customer Snowflake info and add it to snowflake_params dictionary.
        INPUTS:
            The dictionary of metadata parameters and command-line parameters
        RETURNS:
            Returns the snowflake params as a dictionary.
        DESIRABLE ENHANCEMENTS:
            Improve error detection and handling.
        """
            
        self.snowflake_params = {}

        try:
            connection = mysql.connector.connect(host=self.metadata_params["host"], database=self.metadata_params["database"], user=self.metadata_params["username"],
                                                    password=self.metadata_params["password"], port=self.metadata_params["port"])
        except Error as e:
            print("Could not connect to metadata:", e)
            exit(1)

        try:
            if connection.is_connected():
                cursor = connection.cursor()
                query = "select b.account, b.warehouse, b.db, b.dbschema, b.user, b.password, b.region, b.endpoint from `Customer` a join `CustomerSnowflakeConfigProperties` b on a.customerId = b.customerId where a.`customerName`='{}' and b.`envName`='{}' and b.`appName`='{}'".format(
                    self.cmdline_params["customer"], self.cmdline_params["env"], self.cmdline_params["app"])
                cursor.execute(query)
                record = cursor.fetchall()
                if (len(record) > 0):
                    # Set connection params (if not overridden in the command-line)
                    self.snowflake_params["snowflake-account"] = record[0][0]
                    self.snowflake_params["snowflake-warehouse"] = record[0][1]
                    self.snowflake_params["snowflake-database"] = record[0][2]
                    self.snowflake_params["snowflake-schema"] = record[0][3]
                    self.snowflake_params["snowflake-user"] = record[0][4]
                    self.snowflake_params["snowflake-password"] = record[0][5]
                    self.snowflake_params["snowflake-region"] = record[0][6]

                    # Overwrite snowflake param with command-line value if overridden in the command line
                    if "snowflake-account" in self.cmdline_params:
                        self.snowflake_params["snowflake-account"] = self.cmdline_params["snowflake-account"]
                    if "snowflake-warehouse" in self.cmdline_params:
                        self.snowflake_params["snowflake-warehouse"] = self.cmdline_params["snowflake-warehouse"]
                    if "snowflake-database" in self.cmdline_params:
                        self.snowflake_params["snowflake-database"] = self.cmdline_params["snowflake-database"]
                    if "snowflake-schema" in self.cmdline_params:
                        self.snowflake_params["snowflake-schema"] = self.cmdline_params["snowflake-schema"]
                    if "snowflake-user" in self.cmdline_params:
                        self.snowflake_params["snowflake-user"] = self.cmdline_params["snowflake-user"]
                    if "snowflake-password" in self.cmdline_params:
                        self.snowflake_params["snowflake-password"] = self.cmdline_params["snowflake-password"]
                    if "snowflake-region" in self.cmdline_params:
                        self.snowflake_params["snowflake-region"] = self.cmdline_params["snowflake-region"]
                        
                    if self.snowflake_params["snowflake-region"] != "":
                        self.snowflake_params["snowflake-account"] = self.snowflake_params["snowflake-account"] + "." + self.snowflake_params["snowflake-region"]

                    #print('Success reading from CustomerSnowflakeConfigProperties table, account={}, pwd=****, database={}, schema={}, warehouse={}, user={}'.format(
                    #    self.snowflake_params["account"], self.snowflake_params["database"], self.snowflake_params["schema"], self.snowflake_params["warehouse"], self.snowflake_params["user"]
                    #))
                else:
                    print('Customer Information not found in CustomerSnowflakeConfigProperties table for customer={}, env={}, app={}'.format(self.cmdline_params["customer"], self.cmdline_params["env"], self.cmdline_params["app"]))
        except Error as e:
            print("Could not read Snowflake metadata:", e)
        finally:
            # closing database connection.
            try:
                if (connection.is_connected()):
                    cursor.close()
                    connection.close()
            except NameError:
                print("")

        return self.snowflake_params

    def get_databricks_metadata(self):

        """
        PURPOSE:
            Read Aktana metadata for Customer Databricks info and add it to databricks_params dictionary.
        INPUTS:
            The dictionary of metadata parameters and command-line parameters
        RETURNS:
            Returns the databricks params as a dictionary.
        DESIRABLE ENHANCEMENTS:
            Improve error detection and handling.
        """

        self.databricks_params = {}

        try:
            connection = mysql.connector.connect(host=self.metadata_params["host"], database=self.metadata_params["database"], user=self.metadata_params["username"],
                                                    password=self.metadata_params["password"], port=self.metadata_params["port"])
        except Error as e:
            print("Could not connect to metadata:", e)
            exit(1)

        try:
            if connection.is_connected():
                cursor = connection.cursor()
                query = "select b.endPoint, b.apiUri, b.zoneId, b.token, b.sparkVersion, b.workerNodeType, b.minWorkers, b.maxWorkers, b.timeoutSecs, b.dockerImageEnv, b.dockerImageBranch, b.dockerImageCommitId, b.dockerUsername, b.dockerPassword from `Customer` a join `CustomerDatabricksConfig` b on a.customerId = b.customerId where a.`customerName`='{}' and b.`envName`='{}' and b.`appName`='{}'".format(
                    self.cmdline_params["customer"], self.cmdline_params["env"], self.cmdline_params["app"])
                cursor.execute(query)
                record = cursor.fetchall()
                if (len(record) > 0):
                    # Set connection params (if not overridden in the command-line)
                    self.databricks_params["db-endPoint"] = record[0][0]
                    self.databricks_params["db-apiUri"] = record[0][1]
                    self.databricks_params["db-zoneId"] = record[0][2]
                    self.databricks_params["db-token"] = record[0][3]
                    self.databricks_params["db-sparkVersion"] = record[0][4]
                    self.databricks_params["db-workerNodeType"] = record[0][5]
                    self.databricks_params["db-minWorkers"] = record[0][6]
                    self.databricks_params["db-maxWorkers"] = record[0][7]
                    self.databricks_params["db-timeoutSecs"] = record[0][8]
                    self.databricks_params["db-dockerImageEnv"] = record[0][9]
                    self.databricks_params["db-dockerImageBranch"] = record[0][10]
                    self.databricks_params["db-dockerImageCommitId"] = record[0][11]
                    self.databricks_params["db-dockerUsername"] = record[0][12]
                    self.databricks_params["db-dockerPassword"] = record[0][13]

                    # Overwrite docker params with command-line value if overridden in the command line
                    if "db-dockerImageBranch" in self.cmdline_params:
                        self.databricks_params["db-dockerImageBranch"] = self.cmdline_params["db-dockerImageBranch"]
                    if "db-dockerImageCommitId" in self.cmdline_params:
                        self.databricks_params["db-dockerImageCommitId"] = self.cmdline_params["db-dockerImageCommitId"]

                    #print('Success reading from CustomerDatabricksConfig table, endPoint={}, apiUri={}, zoneId={}, token={}, sparkVersion={}, workerNodeType={}, minWorkers={}, maxWorkers={}, timeoutSecs={}'.format(
                    #    self.databricks_params["endPoint"], self.databricks_params["apiUri"], self.databricks_params["zoneId"], self.databricks_params["token"], self.databricks_params["sparkVersion"], self.databricks_params["workerNodeType"], self.databricks_params["minWorkers"], self.databricks_params["maxWorkers"], self.databricks_params["timeoutSecs"]
                    #))
                else:
                    print('Customer Information not found in CustomerDatabricksConfig table for customer={}, env={}, app={}'.format(self.cmdline_params["customer"], self.cmdline_params["env"], self.cmdline_params["app"]))
        except Error as e:
            print("Could not read Databricks metadata:", e)
        finally:
            # closing database connection.
            try:
                if (connection.is_connected()):
                    cursor.close()
                    connection.close()
            except NameError:
                print("")
        
        return self.databricks_params

    def get_adl_metadata(self):

        """
        PURPOSE:
            Read Aktana metadata for Customer ADL Config info and add it to adl_params dictionary.
        INPUTS:
            The dictionary of metadata parameters and command-line parameters
        RETURNS:
            Returns the adl_params as a dictionary.
        DESIRABLE ENHANCEMENTS:
            Improve error detection and handling.
        """

        self.adl_params = {}

        try:
            connection = mysql.connector.connect(host=self.metadata_params["host"], database=self.metadata_params["database"], user=self.metadata_params["username"],
                                                    password=self.metadata_params["password"], port=self.metadata_params["port"])
        except Error as e:
            print("Could not connect to metadata:", e)
            exit(1)

        try:
            if connection.is_connected():
                cursor = connection.cursor()
                query = "select b.rptS3Location, b.adlS3Location, b.iamRole, b.codeRepository, b.mappingLocation, b.loadingLocation, b.awsRegion, b.awsAccessKey, b.awsSecretKey, b.availabilityZone from `Customer` a join `CustomerADLConfig` b on a.customerId = b.customerId where a.`customerName`='{}' and b.`environment`='{}'".format(
                    self.cmdline_params["customer"], self.cmdline_params["env"])
                cursor.execute(query)
                record = cursor.fetchall()
                if (len(record) > 0):
                    # Set connection params (if not overridden in the command-line)
                    self.adl_params["adl-rptS3Location"] = record[0][0]
                    self.adl_params["adl-adlS3Location"] = record[0][1]
                    self.adl_params["adl-iamRole"] = record[0][2]
                    self.adl_params["adl-codeRepository"] = record[0][3]
                    self.adl_params["adl-mappingLocation"] = record[0][4]
                    self.adl_params["adl-loadingLocation"] = record[0][5]
                    self.adl_params["adl-awsRegion"] = record[0][6]
                    self.adl_params["adl-awsAccessKey"] = record[0][7]
                    self.adl_params["adl-awsSecretKey"] = record[0][8]
                    self.adl_params["adl-availabilityZone"] = record[0][9]

                    #print('Success reading from CustomerDatabricksConfig table, rptS3Location={}, adlS3Location={}, iamRole={}, codeRepository={}, mappingLocation={}, loadingLocation={}, awsRegion={}, awsAccessKey={}, awsSecretKey={}, availabilityZone={}'.format(
                    #    self.adl_params["rptS3Location"], 
                    #    self.adl_params["adlS3Location"], 
                    #   self.adl_params["iamRole"], 
                    #   self.adl_params["codeRepository"], 
                    #    self.adl_params["mappingLocation"], 
                    #    self.adl_params["loadingLocation"], 
                    #    self.adl_params["awsRegion"], 
                    #    self.adl_params["awsAccessKey"], 
                    #    self.adl_params["awsSecretKey"], 
                    #    self.adl_params["availabilityZone"]
                    #))
                else:
                    print('Customer Information not found in CustomerADLConfig table for customer={}, env={}'.format(self.cmdline_params["customer"], self.cmdline_params["env"]))
        except Error as e:
            print("Could not read ADL metadata:", e)
        finally:
            # closing database connection.
            try:
                if (connection.is_connected()):
                    cursor.close()
                    connection.close()
            except NameError:
                print("")
        
        return self.adl_params

# ----------------------------------------------------------------------------

if __name__ == '__main__':
    mlutils = aktana_ml_utils()
    cmdline_params, metadata_params = mlutils.initialize(sys.argv)

    print ("Cmdline=",cmdline_params)
    print ("Metadata params=",metadata_params)
    print ("Snowflake params=",mlutils.get_snowflake_metadata())
    print ("Databricks params=", mlutils.get_databricks_metadata())
    print ("ADL params=", mlutils.get_adl_metadata())
