import logging
import os
import sys

import json
import snowflake.connector
import mysql.connector
from mysql.connector import Error
from aktana_ml_utils import aktana_ml_utils

class snowflake_python_base:

    """
    PURPOSE:
        This is the Base/Parent class for programs that use the Snowflake
        Connector for Python.
        This class is intended primarily for:
            * Sample programs, e.g. in the documentation.
            * Tests.
    """

    def __init__(self, p_log_file_name = None):

        """
        PURPOSE:
            This does any required initialization steps, which in this class is
            basically just turning on logging.
        """

        file_name = p_log_file_name
        if file_name is None:
            file_name = '/tmp/snowflake_python_base.log'

        # -- (> ---------- SECTION=begin_logging -----------------------------
        logging.basicConfig(
            filename=file_name,
            level=logging.INFO)
        # -- <) ---------- END_SECTION ---------------------------------------


    # -- (> ---------------------------- SECTION=main ------------------------
    def main(self, argv):

        """
        PURPOSE:
            Most tests follow the same basic pattern in this main() method:
               * Create a connection.
               * Set up, e.g. use (or create and use) the warehouse, database,
                 and schema.
               * Run the queries (or do the other tasks, e.g. load data).
               * Clean up. In this test/demo, we drop the warehouse, database,
                 and schema. In a customer scenario, you'd typically clean up
                 temporary tables, etc., but wouldn't drop your database.
               * Close the connection.
        """

        mlutils = aktana_ml_utils()
        cmdline_params, metadata_params = mlutils.initialize(argv)
        snowflake_params = mlutils.get_snowflake_metadata()
        
        #Databricks and ADL are not used by this program.  So the following 2 lines are not required
        #databricks_params = mlutils.get_databricks_metadata()
        #adl_params = mlutils.get_adl_metadata()

        # Read the connection parameters (e.g. user ID) from the command line
        # and environment variables, then connect to Snowflake.
        connection = self.create_connection(snowflake_params)

        # Set up anything we need (e.g. a separate schema for the test/demo).
        self.set_up(connection, cmdline_params, snowflake_params)

        # Do the "real work", for example, create a table, insert rows, SELECT
        # from the table, etc.
        self.process_db(connection, cmdline_params, snowflake_params)

        # Clean up. In this case, we drop the temporary warehouse, database, and
        # schema.
        self.clean_up(connection, cmdline_params, snowflake_params)

        #print("\nClosing connection...")
        # -- (> ------------------- SECTION=close_connection -----------------
        connection.close()
        # -- <) ---------------------------- END_SECTION ---------------------

    # -- <) ---------------------------- END_SECTION=main --------------------

    def create_connection(self, connection_parameters):

        """
        PURPOSE:
            This connects gets account and login information from the
            environment variables and command-line parameters, connects to the
            server, and returns the connection object.
        INPUTS:
            argv: This is usually sys.argv, which contains the command-line
                  parameters. It could be an equivalent substitute if you get
                  the parameter information from another source.
        RETURNS:
            A connection.
        """

        # Get account and login information from environment variables and
        # command-line parameters.
        # Note that ACCOUNT might require the region and cloud platform where
        # your account is located, in the form of
        #     '<your_account_name>.<region>.<cloud>'
        # for example
        #     'xy12345.us-east-1.azure')
        # -- (> ----------------------- SECTION=set_login_info ---------------

        # Get the password from an appropriate environment variable, if
        # available.

        # If the password is set by both command line and env var, the
        # command-line value takes precedence over (is written over) the
        # env var value.
        try:
            PASSWORD = connection_parameters["snowflake-password"]
        except:
            PASSWORD = ""

        if PASSWORD is None or PASSWORD == '':
            connection_parameters["snowflake-password"] = os.getenv('SNOWSQL_PWD')

        # If the password wasn't set either in the environment var or on
        # the command line...
        try:
            PASSWORD = connection_parameters["snowflake-password"]
        except:
            PASSWORD = ""            
        if PASSWORD is None or PASSWORD == '':
            print("ERROR: snowflake password not found.  Need it from metadata or --snowflake-password command-line parameter or SNOWSQL_PWD environment variable required")
            sys.exit(-2)
        # -- <) ---------------------------- END_SECTION ---------------------

        print("Connecting to Snowflake...")
        # -- (> ------------------- SECTION=connect_to_snowflake ---------
        conn = snowflake.connector.connect(
            user=connection_parameters["snowflake-user"],
            password=connection_parameters["snowflake-password"],
            account=connection_parameters["snowflake-account"],
            warehouse=connection_parameters["snowflake-warehouse"],
            database=connection_parameters["snowflake-database"],
            schema=connection_parameters["snowflake-schema"]
            )
        # -- <) ---------------------------- END_SECTION -----------------

        return conn


    def set_up(self, connection, cmdline_params, snowflake_params):

        """
        PURPOSE:
            Set up to run a test. You can override this method with one
            appropriate to your test/demo.
        """

        # Create a temporary warehouse, database, and schema.
        #self.create_warehouse_database_and_schema(connection)


    def process_db(self, connection, cmdline_params, snowflake_params):

        """
        PURPOSE:
            Your sub-class should override this to include the code required for
            your documentation sample or your test case.
            This default method does a very simple self-test that shows that the
            connection was successful.
        """

        # Create a cursor for this connection.
        cursor1 = conn.cursor()
        # This is an example of an SQL statement we might want to run.
        command = "SELECT PI()"
        # Run the statement.
        cursor1.execute(command)
        # Get the results (should be only one):
        for row in cursor1:
            print(row[0])
        # Close this cursor.
        cursor1.close()


    def clean_up(self, connection, cmdline_params, snowflake_params):

        """
        PURPOSE:
            Clean up after a test. You can override this method with one
            appropriate to your test/demo.
        """

        # Create a temporary warehouse, database, and schema.
        #self.drop_warehouse_database_and_schema(connection)


# ----------------------------------------------------------------------------

if __name__ == '__main__':
    spb = snowflake_python_base()
    spb.main(sys.argv)
