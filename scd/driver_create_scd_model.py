#!/usr/bin/env python
# encoding: utf-8
import base64
import requests
import json
import re
import sys

from aktana_ml_utils import aktana_ml_utils

mlutils = aktana_ml_utils()
cmdline_params, metadata_params = mlutils.initialize(sys.argv)
databricks_params = mlutils.get_databricks_metadata()
snowflake_params = mlutils.get_snowflake_metadata()

if __name__ == "__main__":
  databricks_domain_url = databricks_params["db-endPoint"]
  databricks_api_url_get_job_list = databricks_params["db-apiUri"] + 'list'
  databricks_api_url_create = databricks_params["db-apiUri"] + 'create'
  databricks_api_url_reset_job = databricks_params["db-apiUri"] + 'reset'
  databricks_delete_job_id = -1
  
  databricks_token = databricks_params["db-token"]
  docker_image_environment = databricks_params["db-dockerImageEnv"]
  branch_name = databricks_params["db-dockerImageBranch"]
  commit_id = databricks_params["db-dockerImageCommitId"]
  docker_username = databricks_params["db-dockerUsername"]
  docker_password = databricks_params["db-dockerPassword"]
  
  snowflake_account = snowflake_params["snowflake-account"]
  snowflake_database = snowflake_params["snowflake-database"]
  snowflake_schema = snowflake_params["snowflake-schema"]
  snowflake_warehouse = snowflake_params["snowflake-warehouse"]
  snowflake_username = snowflake_params["snowflake-user"] 
  snowflake_password = snowflake_params["snowflake-password"]
  
  databricks_job_name = cmdline_params["customer"] + "_" + cmdline_params["env"]  + "_" + cmdline_params["app"] + "_job"
  
  # Check if Job already present
  try:
    response = requests.get('%s%s' % (databricks_domain_url, databricks_api_url_get_job_list), headers={'Authorization': 'Bearer %s' % databricks_token})
    if response.status_code == 200:
      for item in json.loads(response.text)["jobs"]:
        if item['settings']['name'] == databricks_job_name:
          databricks_delete_job_id = item['job_id']
          break
    else:
      print(response.text)
      exit(1)
  except requests.exceptions.HTTPError as e:
    print(e.response.text)
    exit(1)
  
  docker_url = "aktanarepo-si-docker-{}-local.jfrog.io/learning:{}-{}".format(docker_image_environment, branch_name, commit_id)
  clusterConfiguration = {
                   "spark_version": databricks_params["db-sparkVersion"],
                   "node_type_id": databricks_params["db-workerNodeType"],
                 "docker_image": {
                   "url": docker_url,
                   "basic_auth": {
                     "username": docker_username,
                     "password": docker_password
                   }
                 },
                 "aws_attributes": {
                   "first_on_demand": 1,
                   "availability": "SPOT_WITH_FALLBACK",
                   "zone_id": databricks_params["db-zoneId"],
                   "spot_bid_price_percent": 100,
                   "ebs_volume_count": 0
                 },
                 "cluster_log_conf": { 
                   "dbfs" : { 
                     "destination" : "dbfs:/" + databricks_job_name + "-logs"
                   }
                 },
                 "autoscale": {
                   "min_workers": databricks_params["db-minWorkers"],
                   "max_workers": databricks_params["db-maxWorkers"]
                 }
                }
  # Job if aleady existed, then update the job
  if databricks_delete_job_id != -1:
    try:
      response = requests.post('%s%s' % (databricks_domain_url,databricks_api_url_reset_job), headers={'Authorization': 'Bearer %s' % databricks_token}, 
                               json={
                                   "job_id": databricks_delete_job_id,
                                   "new_settings": {
                                       "name": databricks_job_name,
                                       "new_cluster": clusterConfiguration,
                                       "timeout_seconds": databricks_params["db-timeoutSecs"],
                                     "spark_submit_task": {
                                       "parameters": ["/learning/scd/code/NVS_AUS_Snowflake_Spark.py", snowflake_username, snowflake_password, snowflake_account, snowflake_warehouse, snowflake_database, snowflake_schema]
                                      }
                                   }
                               })
      if response.status_code == 200:
        print(databricks_job_name + " - Job already existed, Updating the existing Job")
      else:
        print(response.text)
        exit(1)
    except requests.exceptions.HTTPError as e:
      print(e.response.text)
      exit(1)
  else:
    try:
      response = requests.post('%s%s' % (databricks_domain_url,databricks_api_url_create), headers={'Authorization': 'Bearer %s' % databricks_token},
                               json={
                                 "name": databricks_job_name,
                                 "new_cluster": clusterConfiguration,
                                  "timeout_seconds": databricks_params["db-timeoutSecs"],
                                  "spark_submit_task": {
                                  "parameters": ["/learning/scd/code/NVS_AUS_Snowflake_Spark.py", snowflake_username, snowflake_password, snowflake_account, snowflake_warehouse, snowflake_database, snowflake_schema]
                                }
                               })
      if response.status_code == 200:
        print(databricks_job_name + " - Job Created Successfully")
      else:
        print(response.text)
        exit(1)
    except requests.exceptions.HTTPError as e:
      print(e.response.text)
      exit(1)
