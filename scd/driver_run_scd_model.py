#!/usr/bin/env python

import base64
import requests
import json
import sys
import time
from aktana_ml_utils import aktana_ml_utils

mlutils = aktana_ml_utils()
cmdline_params, metadata_params = mlutils.initialize(sys.argv)
databricks_params = mlutils.get_databricks_metadata()

databricks_endpoint = databricks_params["db-endPoint"]
databricks_job_name = cmdline_params["customer"] + "_" + cmdline_params["env"]  + "_" + cmdline_params["app"] + "_job"
databricks_job_id = -1
API_URL_list = databricks_params["db-apiUri"] + 'list'
API_URL_run = databricks_params["db-apiUri"] + 'run-now'
#TOKEN = 'dapibf7aea388236f756d8e9e6c44dfad16d'
TOKEN=databricks_params["db-token"]

try:
    response = requests.get('%s%s' % (databricks_endpoint, API_URL_list), headers={'Authorization': 'Bearer %s' % TOKEN})
    if response.status_code == 200:
      for item in json.loads(response.text)["jobs"]:
        if item['settings']['name'] == databricks_job_name:
          databricks_job_id = item['job_id']
          break
    else:
      print(response.text)
      exit(1)
except requests.exceptions.HTTPError as e:
    print(e.response.text)
    exit(1)

if databricks_job_id != -1:
    try: 
      print("Job Id: " + str(databricks_job_id))
      response = requests.post('%s%s' % (databricks_endpoint, API_URL_run), headers={'Authorization': 'Bearer %s' % TOKEN}, json={ "job_id": databricks_job_id })
      if response.status_code == 200:
          runId = response.json()["run_id"]
          print("Job Started, with RUN-ID: " + str(runId))
          API_URL_run_info = databricks_params["db-apiUri"] + 'runs/get?run_id={}'.format(runId)
          while True:
              time.sleep(30)
              print ("*************")
              try:
                  response = requests.get('%s%s' % (databricks_endpoint, API_URL_run_info), headers={'Authorization': 'Bearer %s' % TOKEN})
                  if response.status_code == 200:
                      respJson = response.json()
                      r = respJson["state"]
                      print (time.strftime("%H:%M:%S", time.localtime()), ",State:", r["life_cycle_state"], ",Message:", r["state_message"])
                      if 'result_state' in r:   
                        print ("Result:", r["result_state"])
                      if r["life_cycle_state"] == "SKIPPED" and "Skipping this run" in r["state_message"]:
                          print("JOB SKIPPED")
                          print("Life Cycle State", r["life_cycle_state"])
                          print("Reason: ",r["state_message"])
                          exit(1)
                          break 
                      if 'result_state' in r and r["result_state"] != "SUCCESS":
                          print("JOB FAILED")
                          print("Job Result", r["result_state"])
                          print("Life Cycle State", r["life_cycle_state"])
                          exit(1)
                          break 
                      if 'result_state' in r and r["result_state"] == "SUCCESS":
                          print("JOB SUCCESS")
                          break
                  else:
                      print("API jobs/runs/get FAILED")
                      print (response.content)
                      break
                      exit(1)
              except requests.exceptions.HTTPError as e:
                  print(e.response.text)
                  exit(1)
      else:
        print("API /jobs/run-now FAILED")
        exit(1)
    except requests.exceptions.HTTPError as e:
        print(e.response.text)
        exit(1)
else:
  print("Job doesn't exists")
  exit(1)
  