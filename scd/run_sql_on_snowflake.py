
import sys
import snowflake.connector
from snowflake_python_base import snowflake_python_base
from codecs import open

class run_sql_on_snowflake (snowflake_python_base):

    """
    PURPOSE:
        This program executes the scd-pre-processor script.
    """

    def process_db(self, conn, cmdline_params, snowflake_params):

        """
        INPUTS:
            conn is a Connection object returned from snowflake.connector.connect().
        """
        scriptFile = cmdline_params["scriptfile"]
        if scriptFile is None or scriptFile == '':
            print("ERROR: Use --scriptfile to specify sql script to execute")
            sys.exit(-3)

        print("Executing SQL script from {}...".format(scriptFile))
        with open(scriptFile, 'r', encoding='utf-8') as f:
            for cur in conn.execute_stream(f):
                for ret in cur:
                    print(ret)

if __name__ == '__main__':

    test_case = run_sql_on_snowflake()
    test_case.main(sys.argv)
