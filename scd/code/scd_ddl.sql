create
or replace TABLE F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP (
    F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_EXTERNAL_ACCOUNT_KEY NUMBER(38, 0),
    DIM_BRAND_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    DIM_PRODUCT_EXTENSION_KEY NUMBER(38, 0),
    DIM_CURRENCY_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    DIM_MARKETBASKET_KEY NUMBER(38, 0),
    ACCOUNT_NAME VARCHAR(255),
    ACCOUNT_ID NUMBER(38, 0),
    ACCOUNT_UID VARCHAR(40),
    SALES_REFERENCE_ACCOUNT_ID VARCHAR(255),
    BRAND_NAME VARCHAR(100),
    PRODUCT_NAME VARCHAR(100),
    PRODUCT_NAME_ENGLISH VARCHAR(100),
    PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_UID VARCHAR(40),
    PRODUCT_STRENGTH VARCHAR(100),
    PRODUCT_PACKAGE VARCHAR(100),
    CURRENCY_CODE VARCHAR(5),
    METRIC_NAME VARCHAR(100),
    FREQUENCY_NAME VARCHAR(100),
    MARKETBASKET_NAME VARCHAR(255),
    MARKET_METRIC_AS_OF_DATE DATE,
    MARKET_METRIC_AS_OF_YEAR NUMBER(38, 0),
    WEEK_NUMBER NUMBER(38, 0),
    MARKET_SHARE_VALUE NUMBER(20, 6),
    MARKET_VOLUME NUMBER(20, 6),
    DW_CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    primary key (F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP_KEY)
);

create
or replace TABLE F_ACCOUNT_WEEKLY_ROLLUP (
    F_ACCOUNT_WEEKLY_ROLLUP_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_EXTERNAL_ACCOUNT_KEY NUMBER(38, 0),
    DIM_BRAND_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    DIM_PRODUCT_EXTENSION_KEY NUMBER(38, 0),
    DIM_CURRENCY_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    ACCOUNT_NAME VARCHAR(255),
    ACCOUNT_ID NUMBER(38, 0),
    ACCOUNT_UID VARCHAR(40),
    SALES_REFERENCE_ACCOUNT_ID VARCHAR(255),
    BRAND_NAME VARCHAR(100),
    PRODUCT_NAME VARCHAR(100),
    PRODUCT_NAME_ENGLISH VARCHAR(100),
    PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_UID VARCHAR(40),
    PRODUCT_STRENGTH VARCHAR(100),
    PRODUCT_PACKAGE VARCHAR(100),
    CURRENCY_CODE VARCHAR(5),
    METRIC_NAME VARCHAR(100),
    FREQUENCY_NAME VARCHAR(100),
    SALE_DATE DATE,
    SALE_YEAR NUMBER(38, 0),
    WEEK_NUMBER NUMBER(38, 0),
    QUANTITY NUMBER(20, 6),
    NET_SALE_AMOUNT NUMBER(20, 6),
    AKT_REFERENCE_ID VARCHAR(100),
    DW_CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    primary key (F_ACCOUNT_WEEKLY_ROLLUP_KEY)
);

create
or replace TABLE F_BRICK_WEEKLY_ROLLUP (
    F_BRICK_WEEKLY_ROLLUP_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_BRICK_KEY NUMBER(38, 0),
    DIM_BRAND_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    DIM_PRODUCT_EXTENSION_KEY NUMBER(38, 0),
    DIM_CURRENCY_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    BRICK_HCO_CODE VARCHAR(100),
    BRAND_NAME VARCHAR(100),
    PRODUCT_NAME VARCHAR(100),
    PRODUCT_NAME_ENGLISH VARCHAR(100),
    PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_UID VARCHAR(40),
    PRODUCT_STRENGTH VARCHAR(100),
    PRODUCT_PACKAGE VARCHAR(100),
    CURRENCY_CODE VARCHAR(5),
    METRIC_NAME VARCHAR(100),
    FREQUENCY_NAME VARCHAR(100),
    SALE_DATE DATE,
    SALE_YEAR NUMBER(38, 0),
    WEEK_NUMBER NUMBER(38, 0),
    QUANTITY NUMBER(20, 6),
    NET_SALE_AMOUNT NUMBER(20, 6),
    AKT_REFERENCE_ID VARCHAR(100),
    DW_CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    primary key (F_BRICK_WEEKLY_ROLLUP_KEY)
);

CREATE
OR REPLACE VIEW VW_ACCOUNT_WEEKLY_MARKETBASKET_VOLUME -- Market Basket Volume is the rolling sum for the Rx (US) or Units (non-US)
-- for all of the products in the Market Basket by Account or Brick.
-- Rows with negative quantity are treated as 0 quantity values.
-- max(SALE_DATE) aligns with max(SALE_DATE) from F_ACCOUNT_WEEKLY.
AS
SELECT
    A.ACCOUNT_ID,
    A.ACCOUNT_UID,
    A.SALES_REFERENCE_ACCOUNT_ID,
    A.ACCOUNT_NAME,
    A.MARKETBASKET_NAME,
    A.PRODUCT_IDENTIFIER,
    A.PRODUCT_UID,
    A.PRODUCT_NAME,
    A.PRODUCT_NAME_ENGLISH,
    A.METRIC_NAME,
    A.SALE_DATE,
    A.PERIOD_NUMBER,
    -- A.QUANTITY AS PRODUCT_VOLUME_SUM,
    CASE
        WHEN A.PERIOD_NUMBER = 1 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 2 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 3 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 4 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 5 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 6 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 7 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 8 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 9 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 10 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 11 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 12 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 13 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                AND CURRENT ROW
        )
        ELSE 0.00000
    END AS ROLLING_PRODUCT_VOLUME_SUM,
    -- MARKETBASKET_SUM,
    ROLLING_MARKETBASKET_SUM
FROM
    (
        SELECT
            ACCOUNT_ID,
            ACCOUNT_UID,
            SALES_REFERENCE_ACCOUNT_ID,
            ACCOUNT_NAME,
            MARKETBASKET_NAME,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            SALE_DATE,
            SALE_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            QUANTITY,
            NET_SALE_AMOUNT,
            GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
        FROM
            (
                SELECT
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    mb.MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER,
                    -- SUM(QUANTITY) AS QUANTITY
                    CASE
                        WHEN SUM(QUANTITY) < 0 THEN 0
                        ELSE SUM(QUANTITY)
                    END AS QUANTITY,
                    CASE
                        WHEN SUM(NET_SALE_AMOUNT) < 0 THEN 0
                        ELSE SUM(NET_SALE_AMOUNT)
                    END AS NET_SALE_AMOUNT
                FROM
                    SCD.F_ACCOUNT_WEEKLY_ROLLUP fwr
                    INNER JOIN (
                        SELECT
                            COALESCE(pm.DIM_PRODUCT_ALL_KEY, -1) AS PRODUCT_ALL_KEY,
                            DIM_MARKETBASKET_KEY,
                            dpe.PRODUCT_NAME
                        FROM
                            DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm
                            LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY
                    ) pm ON fwr.DIM_PRODUCT_ALL_KEY = pm.PRODUCT_ALL_KEY
                    AND fwr.PRODUCT_NAME = pm.PRODUCT_NAME
                    INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                WHERE
                    SALE_DATE <= (
                        SELECT
                            MAX(SALE_DATE)
                        FROM
                            DW_CENTRAL.F_ACCOUNT_WEEKLY
                    ) -- AND SALES_REFERENCE_ACCOUNT_ID = '2489e17'
                    -- AND MARKETBASKET_NAME = '***ntre*****'
                    -- AND SALE_DATE = '2020-01-10'
                    -- AND METRIC_NAME = 'TRx'
                GROUP BY
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    mb.MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER --,PERIOD_NUMBER
                    -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,mb.MARKETBASKET_NAME,PRODUCT_NAME,METRIC_NAME,SALE_DATE,SALE_YEAR,WEEK_NUMBER --,PERIOD_NUMBER
            ) AS d
            CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,METRIC_NAME,YEAR,SALE_DATE,PERIOD_NUMBER
            -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,PERIOD_NUMBER,SALE_DATE
    ) AS A
    INNER JOIN (
        SELECT
            ACCOUNT_ID,
            ACCOUNT_UID,
            SALES_REFERENCE_ACCOUNT_ID,
            ACCOUNT_NAME,
            MARKETBASKET_NAME,
            METRIC_NAME,
            SALE_DATE,
            SALE_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            SALES_METRIC_VALUE AS MARKETBASKET_SUM,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 2 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 5 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_MARKETBASKET_SUM
        FROM
            (
                SELECT
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    QUANTITY,
                    NET_SALE_AMOUNT,
                    GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
                FROM
                    (
                        SELECT
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            mb.MARKETBASKET_NAME,
                            METRIC_NAME,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            CASE
                                WHEN SUM(QUANTITY) < 0 THEN 0
                                ELSE SUM(QUANTITY)
                            END AS QUANTITY,
                            CASE
                                WHEN SUM(NET_SALE_AMOUNT) < 0 THEN 0
                                ELSE SUM(NET_SALE_AMOUNT)
                            END AS NET_SALE_AMOUNT
                        FROM
                            SCD.F_ACCOUNT_WEEKLY_ROLLUP fwr -- INNER JOIN DIM_PRODUCT_MARKET_MAPPING pm ON fwr.PRODUCT_KEY = pm.PRODUCT_KEY
                            INNER JOIN (
                                SELECT
                                    COALESCE(pm.DIM_PRODUCT_ALL_KEY, -1) AS PRODUCT_ALL_KEY,
                                    DIM_MARKETBASKET_KEY,
                                    dpe.PRODUCT_NAME
                                FROM
                                    DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm
                                    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY
                            ) pm ON fwr.DIM_PRODUCT_ALL_KEY = pm.PRODUCT_ALL_KEY
                            AND fwr.PRODUCT_NAME = pm.PRODUCT_NAME
                            INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                        WHERE
                            fwr.SALE_DATE <= (
                                SELECT
                                    MAX(SALE_DATE)
                                FROM
                                    DW_CENTRAL.F_ACCOUNT_WEEKLY
                            ) -- AND SALES_REFERENCE_ACCOUNT_ID = '2489e17'
                            -- AND MARKETBASKET_NAME = '***ntre*****'
                            -- AND SALE_DATE = '2020-01-10'
                            -- AND METRIC_NAME = 'TRx'
                        GROUP BY
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            mb.MARKETBASKET_NAME,
                            METRIC_NAME,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER -- ORDER BY  ACCOUNT_ID,ACCOUNT_UID,SALES_REFERENCE_ACCOUNT_ID,ACCOUNT_NAME,mb.MARKETBASKET_NAME,METRIC_NAME,SALE_DATE,PERIOD_NUMBER
                    ) AS mkt_basket_inner
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp
            ) AS mkt_basket -- ORDER BY  SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,METRIC_NAME,PERIOD_NUMBER,SALE_DATE
    ) AS B -- ON      COALESCE(A.ACCOUNT_ID,-1)       = COALESCE(B.ACCOUNT_ID,-1)
    -- AND     COALESCE(A.ACCOUNT_UID,'99999') = COALESCE(B.ACCOUNT_UID,'99999')
    ON A.SALES_REFERENCE_ACCOUNT_ID = B.SALES_REFERENCE_ACCOUNT_ID
    AND A.MARKETBASKET_NAME = B.MARKETBASKET_NAME
    AND A.METRIC_NAME = B.METRIC_NAME
    AND A.SALE_DATE = B.SALE_DATE
    AND A.PERIOD_NUMBER = B.PERIOD_NUMBER -- ORDER BY A.ACCOUNT_ID,A.ACCOUNT_UID,A.ACCOUNT_NAME,A.SALES_REFERENCE_ACCOUNT_ID,A.MARKETBASKET_NAME,A.PRODUCT_ID,A.PRODUCT_NAME,A.METRIC_NAME,A.SALE_YEAR,A.PERIOD_NUMBER,A.SALE_DATE
;

CREATE
OR REPLACE VIEW VW_ACCOUNT_WEEKLY_MARKET_SHARE -- Market Share is the rolling sum for the Rx (US) or Units (non-US) for the product
-- divided by the rolling sum for the Market Basket by Account or Brick
-- Rows with negative quantity are treated as 0 quantity values.
-- max(SALE_DATE) needs to align with max(SALE_DATE) from F_BRICK_WEEKLY.
AS
SELECT
    final.ACCOUNT_ID,
    final.ACCOUNT_UID,
    final.SALES_REFERENCE_ACCOUNT_ID,
    final.ACCOUNT_NAME,
    final.MARKETBASKET_NAME,
    final.PRODUCT_IDENTIFIER,
    final.PRODUCT_UID,
    final.PRODUCT_NAME,
    final.PRODUCT_NAME_ENGLISH,
    final.METRIC_NAME,
    final.SALE_DATE,
    -- YEAR(final.SALE_DATE) AS YEAR,
    -- WEEK(final.SALE_DATE) AS WEEK,
    -- rms.SUM_UNITS,
    final.PERIOD_NUMBER,
    final.SALES_METRIC_VALUE AS PRODUCT_SUM,
    final.ROLLING_PRODUCT_SUM,
    -- vw.QUANTITY AS MARKETBASKET_VOLUME,
    vw.ROLLING_MARKETBASKET_SUM,
    -- ROLLING_MARKET_SHARE,
    final.ROLLING_PRODUCT_SUM / NULLIF(vw.ROLLING_MARKETBASKET_SUM, 0) * 100 AS ROLLING_MARKET_SHARE
FROM
    (
        SELECT
            rms.ACCOUNT_ID,
            rms.ACCOUNT_UID,
            rms.SALES_REFERENCE_ACCOUNT_ID,
            rms.ACCOUNT_NAME,
            rms.MARKETBASKET_NAME,
            rms.PRODUCT_IDENTIFIER,
            rms.PRODUCT_UID,
            rms.PRODUCT_NAME,
            rms.PRODUCT_NAME_ENGLISH,
            rms.METRIC_NAME,
            rms.SALE_DATE,
            rms.SALE_YEAR,
            rms.WEEK_NUMBER,
            rms.PERIOD_NUMBER,
            rms.QUANTITY,
            rms.NET_SALE_AMOUNT,
            rms.SALES_METRIC_VALUE,
            -- YEAR,
            -- WEEK_NUMBER,
            -- NET_SALE_AMOUNT,
            -- d.SUM_UNITS,
            -- vw.ROLLING_SUM_QUANTITY AS MARKETBASKET_VOLUME,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 2 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 5 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_PRODUCT_SUM
        FROM
            (
                SELECT
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    QUANTITY,
                    NET_SALE_AMOUNT,
                    GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
                FROM
                    (
                        SELECT
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            mb.MARKETBASKET_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            fwr.PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            METRIC_NAME,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            -- SUM(QUANTITY) AS QUANTITY
                            CASE
                                WHEN SUM(QUANTITY) < 0 THEN 0
                                ELSE SUM(QUANTITY)
                            END AS QUANTITY,
                            CASE
                                WHEN SUM(NET_SALE_AMOUNT) < 0 THEN 0
                                ELSE SUM(NET_SALE_AMOUNT)
                            END AS NET_SALE_AMOUNT
                        FROM
                            SCD.F_ACCOUNT_WEEKLY_ROLLUP fwr
                            INNER JOIN (
                                SELECT
                                    COALESCE(pm.DIM_PRODUCT_ALL_KEY, -1) AS PRODUCT_ALL_KEY,
                                    DIM_MARKETBASKET_KEY,
                                    dpe.PRODUCT_NAME
                                FROM
                                    DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm
                                    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY -- WHERE MARKETBASKET_KEY = 3
                            ) pm ON fwr.DIM_PRODUCT_ALL_KEY = pm.PRODUCT_ALL_KEY
                            AND fwr.PRODUCT_NAME = pm.PRODUCT_NAME
                            INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                        WHERE
                            fwr.SALE_DATE <= (
                                SELECT
                                    MAX(SALE_DATE)
                                FROM
                                    DW_CENTRAL.F_ACCOUNT_WEEKLY
                            ) -- AND     SALES_REFERENCE_ACCOUNT_ID = 'e580e33'
                            -- AND     PRODUCT_NAME LIKE '***L OT ACE%'
                            -- AND     YEAR = 2020
                        GROUP BY
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            mb.MARKETBASKET_NAME,
                            METRIC_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            fwr.PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER -- ORDER BY BRICK_HCO_CODE,mb.MARKETBASKET_NAME,METRIC_NAME,PRODUCT_ID,PRODUCT_UID,PRODUCT_NAME,SALE_DATE,YEAR,PERIOD_NUMBER
                    ) AS inner_result
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- order by BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,PRODUCT_ID,PRODUCT_NAME,PERIOD_NUMBER,SALE_DATE
            ) as rms
    ) AS final
    INNER JOIN SCD.vw_ACCOUNT_WEEKLY_MARKETBASKET_VOLUME vw -- ON          COALESCE(vw.ACCOUNT_ID,-1)       = COALESCE(final.ACCOUNT_ID,-1)
    -- AND         COALESCE(vw.ACCOUNT_UID,'99999') = COALESCE(final.ACCOUNT_UID,'99999')
    ON vw.SALES_REFERENCE_ACCOUNT_ID = final.SALES_REFERENCE_ACCOUNT_ID
    AND vw.MARKETBASKET_NAME = final.MARKETBASKET_NAME
    AND COALESCE(vw.PRODUCT_IDENTIFIER, -1000) = COALESCE(final.PRODUCT_IDENTIFIER, -1000)
    AND COALESCE(vw.PRODUCT_UID, 'NA') = COALESCE(final.PRODUCT_UID, 'NA')
    AND vw.PRODUCT_NAME = final.PRODUCT_NAME
    AND vw.METRIC_NAME = final.METRIC_NAME
    AND vw.SALE_DATE = final.SALE_DATE
    AND vw.PERIOD_NUMBER = final.PERIOD_NUMBER -- WHERE
    -- vw.PERIOD_NUMBER = 3 AND
    -- final.PERIOD_NUMBER = 3
    -- AND         final.SALE_DATE = '2018-04-14'
ORDER BY
    final.SALES_REFERENCE_ACCOUNT_ID,
    final.MARKETBASKET_NAME,
    final.METRIC_NAME,
    final.PRODUCT_NAME,
    PERIOD_NUMBER,
    SALE_DATE;

CREATE
OR REPLACE VIEW VW_ACCOUNT_WEEKLY_ORDER_GAP_DETECTION AS
SELECT
    ACCOUNT_ID,
    ACCOUNT_UID,
    ACCOUNT_NAME,
    SALES_REFERENCE_ACCOUNT_ID,
    PRODUCT_IDENTIFIER,
    PRODUCT_UID,
    PRODUCT_NAME,
    PRODUCT_NAME_ENGLISH,
    METRIC_NAME,
    SALE_DATE,
    SALES_METRIC_VALUE,
    CASE
        WHEN (
            PREVIOUS_NON_NULL_DATE IS NULL
            AND SALES_METRIC_VALUE > 0
        ) THEN 0
        ELSE DATEDIFF(WEEK, PREVIOUS_NON_NULL_DATE, SALE_DATE)
    END AS ORDER_GAP_WEEK
FROM
    (
        SELECT
            ACCOUNT_ID,
            ACCOUNT_UID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            SALE_DATE,
            COALESCE(
                NULL_SALE_DATE_QUANTITY,
                NULL_SALE_DATE_NET_SALE_AMOUNT
            ) AS NULL_SALE_DATE,
            COALESCE(
                LAG(NULL_SALE_DATE_QUANTITY) IGNORE NULLS OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_NAME,
                    SALES_REFERENCE_ACCOUNT_ID,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    METRIC_NAME
                    ORDER BY
                        SALE_DATE
                ),
                LAG(NULL_SALE_DATE_NET_SALE_AMOUNT) IGNORE NULLS OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_NAME,
                    SALES_REFERENCE_ACCOUNT_ID,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    METRIC_NAME
                    ORDER BY
                        SALE_DATE
                )
            ) AS PREVIOUS_NON_NULL_DATE,
            GREATEST(NET_SALE_AMOUNT, QUANTITY) AS SALES_METRIC_VALUE -- CONDITIONAL_TRUE_EVENT(QUANTITY)
            --      OVER (PARTITION BY BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME ORDER BY SALE_DATE) AS QTY_CONDITIONAL_TRUE_EVENT
        FROM
            (
                SELECT
                    fwr.ACCOUNT_ID,
                    fwr.ACCOUNT_UID,
                    fwr.ACCOUNT_NAME,
                    fwr.SALES_REFERENCE_ACCOUNT_ID,
                    fwr.PRODUCT_IDENTIFIER,
                    fwr.PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    fwr.PRODUCT_NAME_ENGLISH,
                    fwr.METRIC_NAME,
                    fwr.SALE_DATE,
                    CASE
                        WHEN SUM(fwr.QUANTITY) <= 0 THEN NULL
                        ELSE fwr.SALE_DATE
                    END AS NULL_SALE_DATE_QUANTITY,
                    SUM(fwr.QUANTITY) AS QUANTITY,
                    CASE
                        WHEN SUM(fwr.NET_SALE_AMOUNT) <= 0 THEN NULL
                        ELSE fwr.SALE_DATE
                    END AS NULL_SALE_DATE_NET_SALE_AMOUNT,
                    SUM(fwr.NET_SALE_AMOUNT) AS NET_SALE_AMOUNT
                    /*
                     CASE WHEN fwr.QUANTITY <= 0 THEN NULL
                     ELSE fwr.SALE_DATE
                     END AS NULL_SALE_DATE_QUANTITY,
                     fwr.QUANTITY AS QUANTITY,
                     CASE WHEN fwr.NET_SALE_AMOUNT <= 0 THEN NULL
                     ELSE fwr.SALE_DATE
                     END
                     AS NULL_SALE_DATE_NET_SALE_AMOUNT
                     ,fwr.NET_SALE_AMOUNT AS NET_SALE_AMOUNT
                     */
                FROM
                    SCD.F_ACCOUNT_WEEKLY_ROLLUP fwr
                WHERE
                    fwr.SALE_DATE <= (
                        SELECT
                            MAX(SALE_DATE)
                        FROM
                            DW_CENTRAL.F_ACCOUNT_WEEKLY
                    ) -- AND       fwr.ACCOUNT_KEY = 15
                    -- AND       fwr.PRODUCT_KEY = 2
                GROUP BY
                    fwr.ACCOUNT_ID,
                    fwr.ACCOUNT_UID,
                    fwr.ACCOUNT_NAME,
                    fwr.SALES_REFERENCE_ACCOUNT_ID,
                    fwr.METRIC_NAME,
                    fwr.PRODUCT_IDENTIFIER,
                    fwr.PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    fwr.PRODUCT_NAME_ENGLISH,
                    fwr.SALE_DATE -- ORDER BY  fwr.ACCOUNT_ID,fwr.ACCOUNT_UID,fwr.ACCOUNT_NAME,fwr.SALES_REFERENCE_ACCOUNT_ID,fwr.METRIC_NAME,fwr.PRODUCT_ID,fwr.PRODUCT_UID,fwr.PRODUCT_NAME,fwr.PRODUCT_NAME_ENGLISH,fwr.SALE_DATE
            ) AS d
    ) AS d_final
ORDER BY
    ACCOUNT_ID,
    ACCOUNT_UID,
    ACCOUNT_NAME,
    SALES_REFERENCE_ACCOUNT_ID,
    METRIC_NAME,
    PRODUCT_IDENTIFIER,
    PRODUCT_UID,
    PRODUCT_NAME,
    SALE_DATE;

CREATE
OR REPLACE VIEW VW_ACCOUNT_WEEKLY_SALES_VOLUME AS
SELECT
    ACCOUNT_ID,
    ACCOUNT_UID,
    ACCOUNT_NAME,
    SALES_REFERENCE_ACCOUNT_ID,
    PRODUCT_IDENTIFIER,
    PRODUCT_UID,
    PRODUCT_NAME,
    PRODUCT_NAME_ENGLISH,
    METRIC_NAME,
    SALE_DATE,
    SALE_YEAR,
    WEEK_NUMBER,
    PERIOD_NUMBER,
    NET_SALE_AMOUNT,
    QUANTITY,
    SALES_METRIC_VALUE,
    CASE
        WHEN PERIOD_NUMBER = 1 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 2 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 3 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 4 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 5 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 6 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 7 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 8 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 9 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 10 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 11 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 12 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 13 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY ACCOUNT_ID,
            ACCOUNT_NAME,
            SALES_REFERENCE_ACCOUNT_ID,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                SALES_REFERENCE_ACCOUNT_ID,
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                AND CURRENT ROW
        )
        ELSE 0.00000
    END AS ROLLING_WEEK_SUM -- AKT_REFERENCE_ID
FROM
    (
        SELECT
            ACCOUNT_ID,
            ACCOUNT_UID,
            SALES_REFERENCE_ACCOUNT_ID,
            ACCOUNT_NAME,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            SALE_DATE,
            SALE_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            QUANTITY,
            NET_SALE_AMOUNT,
            -- AKT_REFERENCE_ID,
            GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
        FROM
            (
                SELECT
                    fwr.ACCOUNT_ID,
                    fwr.ACCOUNT_UID,
                    fwr.SALES_REFERENCE_ACCOUNT_ID,
                    fwr.ACCOUNT_NAME,
                    fwr.PRODUCT_IDENTIFIER,
                    fwr.PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    fwr.PRODUCT_NAME_ENGLISH,
                    fwr.METRIC_NAME,
                    fwr.SALE_DATE,
                    fwr.SALE_YEAR,
                    fwr.WEEK_NUMBER,
                    SUM(
                        CASE
                            WHEN fwr.QUANTITY <= 0.00000 THEN 0.00000
                            ELSE fwr.QUANTITY
                        END
                    ) AS QUANTITY,
                    SUM(
                        CASE
                            WHEN fwr.NET_SALE_AMOUNT <= 0.00000 THEN 0.00000
                            ELSE fwr.NET_SALE_AMOUNT
                        END
                    ) AS NET_SALE_AMOUNT
                    /*
                     CASE WHEN fwr.QUANTITY <= 0.00000 THEN 0.00000
                     ELSE fwr.QUANTITY
                     END AS QUANTITY,
                     CASE WHEN fwr.NET_SALE_AMOUNT <= 0.00000 THEN 0.00000
                     ELSE fwr.NET_SALE_AMOUNT
                     END AS NET_SALE_AMOUNT
                     */
                    -- AKT_REFERENCE_ID
                FROM
                    SCD.F_ACCOUNT_WEEKLY_ROLLUP fwr
                WHERE
                    fwr.SALE_DATE <= (
                        SELECT
                            MAX(SALE_DATE)
                        FROM
                            DW_CENTRAL.F_ACCOUNT_WEEKLY
                    ) -- AND       fwr.ACCOUNT_KEY = 3282
                    -- AND	    fwr.PRODUCT_KEY = 94 -- 2
                GROUP BY
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    ACCOUNT_NAME,
                    SALES_REFERENCE_ACCOUNT_ID,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER --,AKT_REFERENCE_ID
                    -- ORDER BY fwr.METRIC_NAME,fwr.SALE_DATE
            ) max_date
            CROSS JOIN DW_CENTRAL.DIM_PERIOD dp
    ) AS outer
ORDER BY
    ACCOUNT_ID,
    ACCOUNT_NAME,
    SALES_REFERENCE_ACCOUNT_ID,
    PRODUCT_IDENTIFIER,
    PRODUCT_UID,
    PRODUCT_NAME,
    METRIC_NAME,
    SALE_YEAR,
    PERIOD_NUMBER,
    WEEK_NUMBER;

CREATE
OR REPLACE VIEW VW_BRICK_WEEKLY_MARKETBASKET_VOLUME -- Market Basket Volume is the rolling sum for the Rx (US) or Units (non-US)
-- for all of the products in the Market Basket by Account or Brick.
-- Rows with negative quantity are treated as 0 quantity values.
-- max(SALE_DATE) aligns with max(SALE_DATE) from F_BRICK_WEEKLY.
AS
SELECT
    A.BRICK_HCO_CODE,
    A.MARKETBASKET_NAME,
    A.PRODUCT_IDENTIFIER,
    A.PRODUCT_UID,
    A.PRODUCT_NAME,
    A.PRODUCT_NAME_ENGLISH,
    A.METRIC_NAME,
    A.SALE_DATE,
    A.PERIOD_NUMBER,
    -- A.QUANTITY AS PRODUCT_VOLUME_SUM,
    CASE
        WHEN A.PERIOD_NUMBER = 1 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 2 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 3 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 4 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 5 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 6 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 7 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 8 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 9 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 10 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 11 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 12 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 13 THEN SUM(A.SALES_METRIC_VALUE) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.SALE_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.SALE_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                AND CURRENT ROW
        )
        ELSE 0.00000
    END AS ROLLING_PRODUCT_VOLUME_SUM,
    -- MARKETBASKET_SUM,
    ROLLING_MARKETBASKET_SUM
FROM
    (
        SELECT
            BRICK_HCO_CODE,
            MARKETBASKET_NAME,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            SALE_DATE,
            SALE_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            QUANTITY,
            NET_SALE_AMOUNT,
            GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
        FROM
            (
                SELECT
                    BRICK_HCO_CODE,
                    mb.MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER,
                    -- SUM(QUANTITY) AS QUANTITY
                    CASE
                        WHEN SUM(QUANTITY) < 0 THEN 0
                        ELSE SUM(QUANTITY)
                    END AS QUANTITY,
                    CASE
                        WHEN SUM(NET_SALE_AMOUNT) < 0 THEN 0
                        ELSE SUM(NET_SALE_AMOUNT)
                    END AS NET_SALE_AMOUNT
                FROM
                    SCD.F_BRICK_WEEKLY_ROLLUP fwr
                    INNER JOIN (
                        SELECT
                            COALESCE(pm.DIM_PRODUCT_ALL_KEY, -1) AS PRODUCT_ALL_KEY,
                            DIM_MARKETBASKET_KEY,
                            dpe.PRODUCT_NAME
                        FROM
                            DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm -- LEFT JOIN DW_CENTRAL.DIM_PRODUCT dp ON pm.DIM_PRODUCT_KEY = dp.DIM_PRODUCT_KEY
                            LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY -- WHERE MARKETBASKET_KEY = 3
                    ) pm ON fwr.DIM_PRODUCT_ALL_KEY = pm.PRODUCT_ALL_KEY
                    AND fwr.PRODUCT_NAME = pm.PRODUCT_NAME
                    INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                WHERE
                    SALE_DATE <= (
                        SELECT
                            MAX(SALE_DATE)
                        FROM
                            DW_CENTRAL.F_BRICK_WEEKLY
                    ) -- AND BRICK_HCO_CODE in ('05006')
                    -- AND PRODUCT_NAME LIKE ('%NTRE%')
                    -- and MARKETBASKET_NAME LIKE ('%NTRE%')
                GROUP BY
                    BRICK_HCO_CODE,
                    mb.MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    fwr.PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER --,PERIOD_NUMBER
                    -- ORDER BY BRICK_HCO_CODE,mb.MARKETBASKET_NAME,PRODUCT_NAME,METRIC_NAME,SALE_DATE,SALE_YEAR,WEEK_NUMBER --,PERIOD_NUMBER
            ) AS d
            CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- ORDER BY BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,YEAR,SALE_DATE,PERIOD_NUMBER
            -- ORDER BY BRICK_HCO_CODE,MARKETBASKET_NAME,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,PERIOD_NUMBER,SALE_DATE
    ) AS A
    INNER JOIN (
        SELECT
            BRICK_HCO_CODE,
            MARKETBASKET_NAME,
            METRIC_NAME,
            SALE_DATE,
            SALE_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            SALES_METRIC_VALUE AS MARKETBASKET_SUM,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 2 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 5 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(SALES_METRIC_VALUE) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_MARKETBASKET_SUM
        FROM
            (
                SELECT
                    BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    QUANTITY,
                    NET_SALE_AMOUNT,
                    GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
                FROM
                    (
                        SELECT
                            BRICK_HCO_CODE,
                            mb.MARKETBASKET_NAME,
                            METRIC_NAME,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            -- SUM(QUANTITY) AS QUANTITY
                            CASE
                                WHEN SUM(QUANTITY) < 0 THEN 0
                                ELSE SUM(QUANTITY)
                            END AS QUANTITY,
                            CASE
                                WHEN SUM(NET_SALE_AMOUNT) < 0 THEN 0
                                ELSE SUM(NET_SALE_AMOUNT)
                            END AS NET_SALE_AMOUNT
                        FROM
                            SCD.F_BRICK_WEEKLY_ROLLUP fwr
                            INNER JOIN (
                                SELECT
                                    COALESCE(pm.DIM_PRODUCT_ALL_KEY, -1) AS PRODUCT_ALL_KEY,
                                    DIM_MARKETBASKET_KEY,
                                    dpe.PRODUCT_NAME
                                FROM
                                    DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm -- LEFT JOIN DW_CENTRAL.DIM_PRODUCT dp ON pm.DIM_PRODUCT_KEY = dp.DIM_PRODUCT_KEY
                                    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY -- WHERE MARKETBASKET_KEY = 2
                            ) pm ON fwr.DIM_PRODUCT_ALL_KEY = pm.PRODUCT_ALL_KEY
                            AND fwr.PRODUCT_NAME = pm.PRODUCT_NAME
                            INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                        WHERE
                            SALE_DATE <= (
                                SELECT
                                    MAX(SALE_DATE)
                                FROM
                                    DW_CENTRAL.F_BRICK_WEEKLY
                            ) -- AND BRICK_HCO_CODE in ('05006')
                            -- AND PRODUCT_NAME LIKE ('%NTRE%')
                            -- and MARKETBASKET_NAME LIKE ('%NTRE%')
                        GROUP BY
                            BRICK_HCO_CODE,
                            mb.MARKETBASKET_NAME,
                            METRIC_NAME,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER
                        ORDER BY
                            BRICK_HCO_CODE,
                            mb.MARKETBASKET_NAME,
                            METRIC_NAME,
                            SALE_DATE
                    ) AS mkt_basket_inner
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- ORDER BY BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,SALE_DATE,PERIOD_NUMBER,WEEK_NUMBER
            ) AS mkt_basket -- ORDER BY  BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,PERIOD_NUMBER,SALE_DATE
    ) AS B ON A.BRICK_HCO_CODE = B.BRICK_HCO_CODE
    AND A.MARKETBASKET_NAME = B.MARKETBASKET_NAME
    AND A.METRIC_NAME = B.METRIC_NAME
    AND A.SALE_DATE = B.SALE_DATE
    AND A.PERIOD_NUMBER = B.PERIOD_NUMBER
ORDER BY
    A.BRICK_HCO_CODE,
    A.MARKETBASKET_NAME,
    A.PRODUCT_IDENTIFIER,
    A.PRODUCT_NAME,
    A.METRIC_NAME,
    A.SALE_YEAR,
    A.PERIOD_NUMBER,
    A.SALE_DATE;

CREATE
OR REPLACE VIEW VW_BRICK_WEEKLY_MARKET_SHARE -- Market Share is the rolling sum for the Rx (US) or Units (non-US) for the product
-- divided by the rolling sum for the Market Basket by Account or Brick
-- Rows with negative quantity are treated as 0 quantity values.
-- max(SALE_DATE) needs to align with max(SALE_DATE) from F_BRICK_WEEKLY.
AS
SELECT
    final.BRICK_HCO_CODE,
    final.MARKETBASKET_NAME,
    final.PRODUCT_IDENTIFIER,
    final.PRODUCT_UID,
    final.PRODUCT_NAME,
    final.PRODUCT_NAME_ENGLISH,
    final.METRIC_NAME,
    final.SALE_DATE,
    final.PERIOD_NUMBER,
    -- final.QUANTITY AS PRODUCT_SUM,
    final.SALES_METRIC_VALUE AS PRODUCT_SUM,
    final.ROLLING_PRODUCT_SUM,
    vw.ROLLING_MARKETBASKET_SUM,
    COALESCE(
        final.ROLLING_PRODUCT_SUM / NULLIF(vw.ROLLING_MARKETBASKET_SUM, 0) * 100,
        0
    ) AS ROLLING_MARKET_SHARE -- (final.PRODUCT_ROLLING_SUM_UNITS/NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100 AS ROLLING_MARKET_SHARE
FROM
    (
        SELECT
            rms.BRICK_HCO_CODE,
            rms.MARKETBASKET_NAME,
            rms.PRODUCT_IDENTIFIER,
            rms.PRODUCT_UID,
            rms.PRODUCT_NAME,
            rms.PRODUCT_NAME_ENGLISH,
            rms.METRIC_NAME,
            rms.SALE_DATE,
            rms.SALE_YEAR,
            rms.WEEK_NUMBER,
            rms.PERIOD_NUMBER,
            rms.QUANTITY,
            rms.NET_SALE_AMOUNT,
            rms.SALES_METRIC_VALUE,
            -- YEAR,
            -- WEEK_NUMBER,
            -- NET_SALE_AMOUNT,
            -- d.SUM_UNITS,
            -- vw.ROLLING_SUM_QUANTITY AS MARKETBASKET_VOLUME,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 2 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 5 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(rms.SALES_METRIC_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    SALE_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.SALE_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_PRODUCT_SUM
        FROM
            (
                SELECT
                    BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    QUANTITY,
                    NET_SALE_AMOUNT,
                    GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
                FROM
                    (
                        SELECT
                            BRICK_HCO_CODE,
                            mb.MARKETBASKET_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            fwr.PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            METRIC_NAME,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            -- SUM(QUANTITY) AS QUANTITY
                            CASE
                                WHEN SUM(QUANTITY) < 0 THEN 0
                                ELSE SUM(QUANTITY)
                            END AS QUANTITY,
                            CASE
                                WHEN SUM(NET_SALE_AMOUNT) < 0 THEN 0
                                ELSE SUM(NET_SALE_AMOUNT)
                            END AS NET_SALE_AMOUNT
                        FROM
                            SCD.F_BRICK_WEEKLY_ROLLUP fwr
                            INNER JOIN (
                                SELECT
                                    COALESCE(pm.DIM_PRODUCT_ALL_KEY, -1) AS PRODUCT_ALL_KEY,
                                    DIM_MARKETBASKET_KEY,
                                    dpe.PRODUCT_NAME
                                FROM
                                    DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm -- LEFT JOIN DW_CENTRAL.DIM_PRODUCT dp ON pm.DIM_PRODUCT_KEY = dp.DIM_PRODUCT_KEY
                                    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY -- WHERE MARKETBASKET_KEY = 3
                            ) pm ON fwr.DIM_PRODUCT_ALL_KEY = pm.PRODUCT_ALL_KEY
                            AND fwr.PRODUCT_NAME = pm.PRODUCT_NAME
                            INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY -- CROSS JOIN DIM_PERIOD dp
                        WHERE
                            SALE_DATE <= (
                                SELECT
                                    MAX(SALE_DATE)
                                FROM
                                    DW_CENTRAL.F_BRICK_WEEKLY
                            ) -- AND BRICK_HCO_CODE in ('05006')
                            -- AND PRODUCT_NAME LIKE ('%NTRE%')
                            -- and MARKETBASKET_NAME LIKE ('%NTRE%')
                        GROUP BY
                            BRICK_HCO_CODE,
                            mb.MARKETBASKET_NAME,
                            METRIC_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            fwr.PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            SALE_DATE,
                            SALE_YEAR,
                            WEEK_NUMBER -- ORDER BY BRICK_HCO_CODE,mb.MARKETBASKET_NAME,METRIC_NAME,PRODUCT_IDENTIFIER,PRODUCT_UID,PRODUCT_NAME,SALE_DATE,SALE_YEAR
                            -- ) AS d
                    ) AS inner_result
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- ORDER BY BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,PRODUCT_IDENTIFIER,PRODUCT_NAME,SALE_DATE,PERIOD_NUMBER
            ) AS rms -- ORDER by BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,PRODUCT_IDENTIFIER,PRODUCT_NAME,PERIOD_NUMBER,SALE_DATE
    ) AS final
    INNER JOIN SCD.VW_BRICK_WEEKLY_MARKETBASKET_VOLUME vw ON vw.BRICK_HCO_CODE = final.BRICK_HCO_CODE
    AND vw.MARKETBASKET_NAME = final.MARKETBASKET_NAME -- AND         vw.PRODUCT_ID          = final.PRODUCT_ID
    -- AND         vw.PRODUCT_UID         = final.PRODUCT_UID
    AND COALESCE(vw.PRODUCT_IDENTIFIER, -1000) = COALESCE(final.PRODUCT_IDENTIFIER, -1000)
    AND COALESCE(vw.PRODUCT_UID, 'NA') = COALESCE(final.PRODUCT_UID, 'NA')
    AND vw.PRODUCT_NAME = final.PRODUCT_NAME
    AND vw.METRIC_NAME = final.METRIC_NAME
    AND vw.SALE_DATE = final.SALE_DATE
    AND vw.PERIOD_NUMBER = final.PERIOD_NUMBER -- WHERE
    -- vw.PERIOD_NUMBER = 3 AND
    -- final.PERIOD_NUMBER = 3
    -- AND         final.SALE_DATE = '2018-04-14'
ORDER BY
    final.BRICK_HCO_CODE,
    final.MARKETBASKET_NAME,
    final.METRIC_NAME,
    final.PRODUCT_NAME,
    PERIOD_NUMBER,
    SALE_DATE;

CREATE
OR REPLACE VIEW VW_BRICK_WEEKLY_ORDER_GAP_DETECTION -- calculating the weeks-since-last-order by Sale_Date, Account/Brick, and product.
-- The period number should always be 1 since there is no rolling involved
-- (only aggregating in case we need to aggregate from the daily level to the weekly level or in case we have multiple entries for one Account/Brick per product per week).
-- The first non-zero QUANTITY for a BRICK_HCO_CODE and Product should have ORDER_GAP_WEEK = 0.
-- Any weeks occurring before the first entry non-zero QUANTITY for a BRICK_HCO_CODE and Product should be NULL.
-- After the first non-zero entry, we should start counting by 1.
-- So, for example, if a BRICK_HCO and Product have a non-zero entry for week 2019-01-19
-- and then another non-zero entry for week 2019-01-26, the ORDER_GAP_WEEK for 2019-01-26 will be 1 week instead of 0 weeks.
AS
SELECT
    BRICK_HCO_CODE,
    PRODUCT_IDENTIFIER,
    PRODUCT_UID,
    PRODUCT_NAME,
    PRODUCT_NAME_ENGLISH,
    METRIC_NAME,
    SALE_DATE,
    SALES_METRIC_VALUE,
    CASE
        WHEN (
            PREVIOUS_NON_NULL_DATE IS NULL
            AND SALES_METRIC_VALUE > 0
        ) THEN 0
        ELSE DATEDIFF(WEEK, PREVIOUS_NON_NULL_DATE, SALE_DATE)
    END AS ORDER_GAP_WEEK
    /*
     DATEDIFF(WEEK,MIN(SALE_DATE)
     OVER (PARTITION BY BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,QTY_CONDITIONAL_TRUE_EVENT ORDER BY SALE_DATE)
     ,SALE_DATE
     ) AS ORDER_GAP_WEEK
     */
    -- MIN(SALE_DATE)
    --    OVER (PARTITION BY BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,QTY_CONDITIONAL_TRUE_EVENT ORDER BY SALE_DATE) AS MIN_SALE_DATE,
    -- MIN(case when SUM(QUANTITY) = 0.0 then SALE_DATE end) over (partition by BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,QUANTITY order by SALE_DATE)
    -- AS
    -- conditional_true_event,
    -- ROW_NUMBER() OVER (PARTITION BY BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,conditional_true_event ORDER BY SALE_DATE) AS row_num
FROM
    (
        SELECT
            BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            SALE_DATE,
            COALESCE(
                NULL_SALE_DATE_QUANTITY,
                NULL_SALE_DATE_NET_SALE_AMOUNT
            ) AS NULL_SALE_DATE,
            COALESCE(
                LAG(NULL_SALE_DATE_QUANTITY) IGNORE NULLS OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    METRIC_NAME
                    ORDER BY
                        SALE_DATE
                ),
                LAG(NULL_SALE_DATE_NET_SALE_AMOUNT) IGNORE NULLS OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    METRIC_NAME
                    ORDER BY
                        SALE_DATE
                )
            ) AS PREVIOUS_NON_NULL_DATE,
            GREATEST(NET_SALE_AMOUNT, QUANTITY) AS SALES_METRIC_VALUE -- CONDITIONAL_TRUE_EVENT(QUANTITY)
            --      OVER (PARTITION BY BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME ORDER BY SALE_DATE) AS QTY_CONDITIONAL_TRUE_EVENT
        FROM
            (
                SELECT
                    fwr.BRICK_HCO_CODE,
                    fwr.PRODUCT_IDENTIFIER,
                    fwr.PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    fwr.PRODUCT_NAME_ENGLISH,
                    fwr.METRIC_NAME,
                    fwr.SALE_DATE,
                    CASE
                        WHEN SUM(fwr.QUANTITY) <= 0 THEN NULL
                        ELSE fwr.SALE_DATE
                    END AS NULL_SALE_DATE_QUANTITY,
                    SUM(fwr.QUANTITY) AS QUANTITY,
                    CASE
                        WHEN SUM(fwr.NET_SALE_AMOUNT) <= 0 THEN NULL
                        ELSE fwr.SALE_DATE
                    END AS NULL_SALE_DATE_NET_SALE_AMOUNT,
                    SUM(fwr.NET_SALE_AMOUNT) AS NET_SALE_AMOUNT
                FROM
                    SCD.F_BRICK_WEEKLY_ROLLUP fwr
                    /*INNER JOIN    (SELECT BRICK_KEY,
                     BRICK_HCO_CODE,
                     GREATEST(PRODUCT_KEY,EXTERNAL_PRODUCT_KEY) AS PRODUCT_KEY,
                     METRIC_KEY,
                     MAX(SALE_DATE) AS MAX_SALE_DATE
                     -- CASE WHEN YEAR(MAX(SALE_DATE)) < YEAR(CURRENT_DATE())
                     --     THEN CURRENT_DATE()
                     --      ELSE MAX(SALE_DATE)
                     --      END
                     --    AS MAX_SALE_DATE
                     FROM   F_BRICK_WEEKLY
                     GROUP BY BRICK_KEY,BRICK_HCO_CODE,PRODUCT_KEY,EXTERNAL_PRODUCT_KEY,METRIC_KEY
                     ) fw
                     ON        fwr.BRICK_KEY       = fw.BRICK_KEY
                     AND       fwr.BRICK_HCO_CODE  = fw.BRICK_HCO_CODE
                     AND       fwr.PRODUCT_KEY     = fw.PRODUCT_KEY
                     AND       fwr.METRIC_KEY      = fw.METRIC_KEY
                     WHERE     fwr.SALE_DATE       <= fw.MAX_SALE_DATE
                     */
                WHERE
                    fwr.SALE_DATE <= (
                        SELECT
                            MAX(SALE_DATE)
                        FROM
                            DW_CENTRAL.F_BRICK_WEEKLY
                    ) -- AND       fwr.ACCOUNT_KEY = 3282
                    -- AND       fwr.PRODUCT_KEY = 2
                GROUP BY
                    fwr.BRICK_HCO_CODE,
                    fwr.METRIC_NAME,
                    fwr.PRODUCT_IDENTIFIER,
                    fwr.PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    fwr.PRODUCT_NAME_ENGLISH,
                    fwr.SALE_DATE -- ORDER BY  fwr.BRICK_HCO_CODE,fwr.METRIC_NAME,fwr.PRODUCT_ID,fwr.PRODUCT_UID,fwr.PRODUCT_NAME,fwr.PRODUCT_NAME_ENGLISH,fwr.SALE_DATE
            ) AS d
    ) AS d_final
ORDER BY
    BRICK_HCO_CODE,
    METRIC_NAME,
    PRODUCT_IDENTIFIER,
    PRODUCT_UID,
    PRODUCT_NAME,
    SALE_DATE;

CREATE
OR REPLACE VIEW VW_BRICK_WEEKLY_SALES_VOLUME -- Sales Volume is the rolling sum for the Rx (US) or Units (non-US) for the product by Account or Brick
AS
SELECT
    BRICK_HCO_CODE,
    PRODUCT_IDENTIFIER,
    PRODUCT_UID,
    PRODUCT_NAME,
    PRODUCT_NAME_ENGLISH,
    METRIC_NAME,
    SALE_DATE,
    SALE_YEAR,
    WEEK_NUMBER,
    PERIOD_NUMBER,
    NET_SALE_AMOUNT,
    QUANTITY,
    SALES_METRIC_VALUE,
    -- SUM(QUANTITY) OVER (PARTITION BY BRICK_HCO_CODE,PRODUCT_NAME,METRIC_NAME,YEAR,PERIOD_NUMBER
    --                             ORDER BY PRODUCT_NAME,YEAR,WEEK_NUMBER,PERIOD_NUMBER
    --                             )
    --                             AS ROLLING_SUM_QUANTITY,
    CASE
        WHEN PERIOD_NUMBER = 1 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 2 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 3 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 4 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 5 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 6 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 7 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 8 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 9 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 10 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 11 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 12 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                AND CURRENT ROW
        )
        WHEN PERIOD_NUMBER = 13 THEN SUM(SALES_METRIC_VALUE) OVER (
            PARTITION BY BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_NAME,
            METRIC_NAME,
            SALE_YEAR,
            PERIOD_NUMBER
            ORDER BY
                PRODUCT_IDENTIFIER,
                PRODUCT_NAME,
                METRIC_NAME,
                SALE_DATE,
                PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                AND CURRENT ROW
        )
        ELSE 0.00000
    END AS ROLLING_WEEK_SUM
FROM
    (
        SELECT
            BRICK_HCO_CODE,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            SALE_DATE,
            SALE_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            QUANTITY,
            NET_SALE_AMOUNT,
            GREATEST(QUANTITY, NET_SALE_AMOUNT) AS SALES_METRIC_VALUE
        FROM
            (
                SELECT
                    fwr.BRICK_HCO_CODE,
                    fwr.PRODUCT_IDENTIFIER,
                    fwr.PRODUCT_UID,
                    fwr.PRODUCT_NAME,
                    fwr.PRODUCT_NAME_ENGLISH,
                    fwr.METRIC_NAME,
                    fwr.SALE_DATE,
                    fwr.SALE_YEAR,
                    fwr.WEEK_NUMBER,
                    SUM(
                        CASE
                            WHEN fwr.QUANTITY <= 0.00000 THEN 0.00000
                            ELSE fwr.QUANTITY
                        END
                    ) AS QUANTITY,
                    SUM(
                        CASE
                            WHEN fwr.NET_SALE_AMOUNT <= 0.00000 THEN 0.00000
                            ELSE fwr.NET_SALE_AMOUNT
                        END
                    ) AS NET_SALE_AMOUNT
                FROM
                    SCD.F_BRICK_WEEKLY_ROLLUP fwr
                WHERE
                    fwr.SALE_DATE <= (
                        SELECT
                            MAX(SALE_DATE)
                        FROM
                            DW_CENTRAL.F_BRICK_WEEKLY
                    )
                GROUP BY
                    BRICK_HCO_CODE,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    SALE_DATE,
                    SALE_YEAR,
                    WEEK_NUMBER
            ) AS max_date
            CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- WHERE   BRICK_HCO_CODE = '05174'
            -- AND     PRODUCT_NAME = '**NTRE****'
            -- AND     SALE_YEAR = 2018
            -- GROUP BY BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_UID,PRODUCT_NAME,METRIC_NAME,SALE_DATE,YEAR,PERIOD_NUMBER,WEEK_NUMBER
            -- ORDER BY BRICK_HCO_CODE,PRODUCT_ID,PRODUCT_UID,PRODUCT_NAME,METRIC_NAME,SALE_DATE,SALE_YEAR,PERIOD_NUMBER,WEEK_NUMBER
    ) AS outer_result
ORDER BY
    BRICK_HCO_CODE,
    PRODUCT_IDENTIFIER,
    PRODUCT_NAME,
    METRIC_NAME,
    SALE_YEAR,
    PERIOD_NUMBER,
    WEEK_NUMBER;

CREATE
OR REPLACE VIEW VW_F_ACCOUNT_WEEKLY_ROLLUP AS
SELECT
    COALESCE(faw.DIM_EXTERNAL_ACCOUNT_KEY, -1) AS DIM_EXTERNAL_ACCOUNT_KEY,
    faw.DIM_BRAND_KEY,
    faw.DIM_PRODUCT_ALL_KEY AS DIM_PRODUCT_ALL_KEY,
    NULL AS DIM_PRODUCT_EXTENSION_KEY,
    faw.DIM_CURRENCY_KEY,
    faw.DIM_METRIC_KEY,
    faw.DIM_FREQUENCY_KEY,
    daext.ACCOUNT_NAME,
    dse.ACCOUNT_ID,
    faw.ACCOUNT_UID,
    daext.SALES_REFERENCE_ACCOUNT_ID,
    dbr.BRAND_NAME,
    -- IFF(dse.ACCOUNT_KEY > 0, dse.ACCOUNT_NAME, daext.ACCOUNT_NAME) AS ACCOUNT_NAME,
    -- dse.ACCOUNT_ID AS ACCOUNT_ID,
    -- faw.ACCOUNT_UID,
    -- COALESCE(dse.SALES_REFERENCE_ACCOUNT_ID,daext.SALES_REFERENCE_ACCOUNT_ID) AS SALES_REFERENCE_ACCOUNT_ID,
    dpd.PRODUCT_NAME,
    dpd.PRODUCT_NAME_ENGLISH,
    dpd.PRODUCT_IDENTIFIER,
    faw.PRODUCT_UID,
    NULL AS PRODUCT_STRENGTH,
    NULL AS PRODUCT_PACKAGE,
    faw.CURRENCY_CODE,
    dm.METRIC_NAME,
    df.FREQUENCY_NAME,
    REFERENCE_SALE_DATE AS SALE_DATE,
    faw.REFERENCE_SALE_YEAR AS SALE_YEAR,
    faw.WEEK_NUMBER,
    COALESCE(qty.QUANTITY, 0.000000) AS QUANTITY,
    COALESCE(qty.NET_SALE_AMOUNT, 0.000000) AS NET_SALE_AMOUNT -- faw.DSE_PRODUCT_KEY,
    -- faw.EXTERNAL_PRODUCT_KEY,
    -- dpd.PRODUCT_NAME,dpex.PRODUCT_NAME,
    -- IFF(dpd.PRODUCT_KEY > 0, dpd.PRODUCT_NAME, dpex.PRODUCT_NAME) AS PRODUCT_NAME,
    -- IFF(dpd.PRODUCT_KEY > 0, dpd.PRODUCT_NAME_ENGLISH, dpex.PRODUCT_NAME_ENGLISH) AS PRODUCT_NAME_ENGLISH,
    -- dpd.PRODUCT_ID AS PRODUCT_ID,
    -- faw.PRODUCT_UID,
    -- faw.PRODUCT_EXTENSION_KEY,
    -- pext.PRODUCT_STRENGTH,
    -- pext.PRODUCT_PACKAGE,        
    -- COALESCE(SALE_DATE,REFERENCE_SALE_DATE) AS SALE_DATE,       
    -- faw.SALE_YEAR,
    -- qty.AKT_REFERENCE_ID
FROM
    (
        SELECT
            DISTINCT faw.DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            -- PRODUCT_EXTENSION_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            ACCOUNT_UID,
            PRODUCT_UID,
            CURRENCY_CODE,
            dd.WEEK_NUMBER,
            dd.REFERENCE_SALE_DATE,
            dd.REFERENCE_SALE_YEAR
        FROM
            (
                SELECT
                    DISTINCT COALESCE(DIM_EXTERNAL_ACCOUNT_KEY, -1) AS DIM_EXTERNAL_ACCOUNT_KEY,
                    DIM_BRAND_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    -- PRODUCT_EXTENSION_KEY,
                    DIM_CURRENCY_KEY,
                    DIM_METRIC_KEY,
                    DIM_FREQUENCY_KEY,
                    ACCOUNT_UID,
                    PRODUCT_UID,
                    CURRENCY_CODE,
                    -- AKT_REFERENCE_ID,
                    YEAR(SALE_DATE) AS SALE_YEAR
                FROM
                    DW_CENTRAL.F_ACCOUNT_WEEKLY -- WHERE   DIM_EXTERNAL_ACCOUNT_KEY = 1154 
                    --             AND SALE_DATE = '2020-02-17'
                    -- 		    AND DIM_PRODUCT_ALL_KEY = 516
            ) AS faw
            CROSS JOIN (
                SELECT
                    DATE AS REFERENCE_SALE_DATE,
                    YEAR AS REFERENCE_SALE_YEAR,
                    WEEK_NUMBER
                FROM
                    DW_CENTRAL.DIM_DATE dd
                    INNER JOIN (
                        SELECT
                            YEAR(MIN(SALE_DATE)) AS MIN_SALE_DATE,
                            DAYNAME(MIN(SALE_DATE)) AS DAYNAME
                        FROM
                            DW_CENTRAL.F_ACCOUNT_WEEKLY
                    ) AS faw
                WHERE
                    -- DAY_NAME = 'Saturday'
                    LEFT(dd.DAY_NAME, 3) = faw.DAYNAME
                    AND (
                        dd.YEAR >= faw.MIN_SALE_DATE
                        AND dd.YEAR <= YEAR(CURRENT_DATE())
                    )
            ) AS dd
        WHERE
            faw.SALE_YEAR <= dd.REFERENCE_SALE_YEAR -- ORDER BY REFERENCE_SALE_DATE
    ) AS faw
    LEFT JOIN (
        SELECT
            COALESCE(DIM_EXTERNAL_ACCOUNT_KEY, -1) AS DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            -- PRODUCT_EXTENSION_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            ACCOUNT_UID,
            PRODUCT_UID,
            CURRENCY_CODE,
            SALE_DATE,
            YEAR(SALE_DATE) AS SALE_YEAR,
            WEEK_NUMBER,
            SUM(
                CASE
                    WHEN QUANTITY <= 0 THEN 0
                    ELSE QUANTITY
                END
            ) AS QUANTITY,
            SUM(
                CASE
                    WHEN NET_SALE_AMOUNT <= 0 THEN 0
                    ELSE NET_SALE_AMOUNT
                END
            ) AS NET_SALE_AMOUNT -- AKT_REFERENCE_ID
            -- select *
        FROM
            DW_CENTRAL.F_ACCOUNT_WEEKLY -- WHERE   DIM_EXTERNAL_ACCOUNT_KEY = 1154 
            --    AND SALE_DATE = '2020-02-17'
            --    AND DIM_PRODUCT_ALL_KEY = 516
            --    AND     METRIC_KEY = 1
        GROUP BY
            DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            ACCOUNT_UID,
            PRODUCT_UID,
            CURRENCY_CODE,
            SALE_DATE,
            SALE_YEAR,
            WEEK_NUMBER
    ) AS qty ON faw.DIM_EXTERNAL_ACCOUNT_KEY = qty.DIM_EXTERNAL_ACCOUNT_KEY -- AND     faw.ACCOUNT_UID              = qty.ACCOUNT_UID
    AND faw.DIM_BRAND_KEY = qty.DIM_BRAND_KEY
    AND faw.DIM_PRODUCT_ALL_KEY = qty.DIM_PRODUCT_ALL_KEY -- AND		faw.PRODUCT_EXTENSION_KEY	 = COALESCE(qty.PRODUCT_EXTENSION_KEY,-1)
    -- AND     faw.PRODUCT_UID              = qty.PRODUCT_UID
    -- AND     faw.CURRENCY_KEY             = qty.CURRENCY_KEY
    AND faw.DIM_METRIC_KEY = COALESCE(qty.DIM_METRIC_KEY, -1)
    AND faw.DIM_FREQUENCY_KEY = qty.DIM_FREQUENCY_KEY -- AND     faw.SALE_YEAR               = qty.SALE_YEAR
    AND faw.REFERENCE_SALE_YEAR = qty.SALE_YEAR
    AND faw.WEEK_NUMBER = qty.WEEK_NUMBER -- AND		 faw.AKT_REFERENCE_ID			= qty.AKT_REFERENCE_ID
    LEFT JOIN DW_CENTRAL.DIM_EXTERNAL_ACCOUNT daext ON faw.DIM_EXTERNAL_ACCOUNT_KEY = daext.DIM_EXTERNAL_ACCOUNT_KEY
    LEFT JOIN DW_CENTRAL.DIM_ACCOUNT dse ON dse.ACCOUNT_UID = daext.ACCOUNT_UID
    AND dse.RECORD_END_DATE > '3000-01-01' -- AND da.SOURCE_SYSTEM_NAME IN ('DSE','dse')
    LEFT JOIN DW_CENTRAL.DIM_BRAND dbr ON faw.DIM_BRAND_KEY = dbr.DIM_BRAND_KEY
    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpd ON faw.DIM_PRODUCT_ALL_KEY = dpd.DIM_PRODUCT_ALL_KEY -- LEFT JOIN	 	DW_CENTRAL.DIM_PRODUCT_EXTENSION pext ON COALESCE(faw.PRODUCT_EXTENSION_KEY,-1) = COALESCE(pext.PRODUCT_EXTENSION_KEY,-1)
    LEFT JOIN DW_CENTRAL.DIM_METRIC dm ON COALESCE(faw.DIM_METRIC_KEY, -1) = COALESCE(dm.DIM_METRIC_KEY, -1)
    INNER JOIN DW_CENTRAL.DIM_FREQUENCY df ON faw.DIM_FREQUENCY_KEY = df.DIM_FREQUENCY_KEY
ORDER BY
    faw.DIM_EXTERNAL_ACCOUNT_KEY,
    faw.DIM_PRODUCT_ALL_KEY,
    -- faw.PRODUCT_UID,
    -- faw.CURRENCY_KEY,
    faw.DIM_METRIC_KEY,
    -- faw.REFERENCE_SALE_YEAR,
    --  faw.WEEK_NUMBER,
    faw.REFERENCE_SALE_DATE;

CREATE
OR REPLACE VIEW VW_F_BRICK_WEEKLY_ROLLUP AS
SELECT
    fbw.DIM_BRICK_KEY,
    fbw.DIM_BRAND_KEY,
    fbw.DIM_PRODUCT_ALL_KEY,
    NULL AS DIM_PRODUCT_EXTENSION_KEY,
    fbw.DIM_CURRENCY_KEY,
    fbw.DIM_METRIC_KEY,
    fbw.DIM_FREQUENCY_KEY,
    fbw.BRICK_HCO_CODE,
    dbr.BRAND_NAME,
    dpd.PRODUCT_NAME,
    dpd.PRODUCT_NAME_ENGLISH,
    dpd.PRODUCT_IDENTIFIER,
    fbw.PRODUCT_UID,
    NULL AS PRODUCT_STRENGTH,
    NULL AS PRODUCT_PACKAGE,
    fbw.CURRENCY_CODE,
    dm.METRIC_NAME,
    df.FREQUENCY_NAME,
    REFERENCE_SALE_DATE AS SALE_DATE,
    fbw.REFERENCE_SALE_YEAR AS SALE_YEAR,
    fbw.WEEK_NUMBER,
    -- fbw.REFERENCE_SALE_DATE,
    COALESCE(qty.QUANTITY, 0.000000) AS QUANTITY,
    COALESCE(qty.NET_SALE_AMOUNT, 0.000000) AS NET_SALE_AMOUNT -- qty.AKT_REFERENCE_ID
FROM
    (
        SELECT
            DISTINCT fbw.DIM_BRICK_KEY,
            fbw.BRICK_HCO_CODE,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            PRODUCT_UID,
            DIM_CURRENCY_KEY,
            CURRENCY_CODE,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            dd.WEEK_NUMBER,
            dd.REFERENCE_SALE_DATE,
            dd.REFERENCE_SALE_YEAR
        FROM
            (
                SELECT
                    DISTINCT DIM_BRICK_KEY,
                    BRICK_HCO_CODE,
                    DIM_BRAND_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    PRODUCT_UID,
                    DIM_CURRENCY_KEY,
                    CURRENCY_CODE,
                    DIM_METRIC_KEY,
                    DIM_FREQUENCY_KEY,
                    YEAR(SALE_DATE) AS SALE_YEAR
                FROM
                    DW_CENTRAL.F_BRICK_WEEKLY -- WHERE     BRICK_HCO_CODE = 'LOM76'
                    -- AND     EXTERNAL_PRODUCT_KEY = 24
            ) AS fbw
            CROSS JOIN (
                SELECT
                    DATE AS REFERENCE_SALE_DATE,
                    YEAR AS REFERENCE_SALE_YEAR,
                    WEEK_NUMBER
                FROM
                    DW_CENTRAL.DIM_DATE dd
                    INNER JOIN (
                        SELECT
                            YEAR(MIN(SALE_DATE)) AS MIN_SALE_DATE,
                            DAYNAME(MIN(SALE_DATE)) AS DAYNAME
                        FROM
                            DW_CENTRAL.F_BRICK_WEEKLY -- WHERE   SOURCE_SYSTEM_NAME = 'IQVIA Retail Weekly'
                    ) AS faw
                WHERE
                    -- DAY_NAME = 'Saturday'
                    LEFT(dd.DAY_NAME, 3) = faw.DAYNAME
                    AND (
                        dd.YEAR >= faw.MIN_SALE_DATE
                        AND dd.YEAR <= YEAR(CURRENT_DATE())
                    ) -- GROUP BY YEAR,WEEK_NUMBER
            ) AS dd
        WHERE
            fbw.SALE_YEAR <= dd.REFERENCE_SALE_YEAR
    ) AS fbw
    LEFT JOIN (
        SELECT
            DIM_BRICK_KEY,
            BRICK_HCO_CODE,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            PRODUCT_UID,
            DIM_CURRENCY_KEY,
            CURRENCY_CODE,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            SALE_DATE,
            YEAR(SALE_DATE) AS SALE_YEAR,
            WEEK(SALE_DATE) AS WEEK_NUMBER,
            SUM(
                CASE
                    WHEN QUANTITY <= 0 THEN 0
                    ELSE QUANTITY
                END
            ) AS QUANTITY,
            SUM(
                CASE
                    WHEN NET_SALE_AMOUNT <= 0 THEN 0
                    ELSE NET_SALE_AMOUNT
                END
            ) AS NET_SALE_AMOUNT -- AKT_REFERENCE_ID
        FROM
            DW_CENTRAL.F_BRICK_WEEKLY -- 20,506
            -- WHERE   SOURCE_SYSTEM_NAME = 'IQVIA Retail Weekly'
            -- WHERE     BRICK_HCO_CODE = 'LOM76'
            -- AND     EXTERNAL_PRODUCT_KEY = 24
            -- AND     YEAR(SALE_DATE) = 2018
        GROUP BY
            DIM_BRICK_KEY,
            BRICK_HCO_CODE,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            PRODUCT_UID,
            DIM_CURRENCY_KEY,
            CURRENCY_CODE,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            SALE_DATE,
            YEAR(SALE_DATE),
            WEEK(SALE_DATE) -- ORDER BY  SALE_DATE
    ) AS qty ON fbw.DIM_BRICK_KEY = qty.DIM_BRICK_KEY
    AND fbw.BRICK_HCO_CODE = qty.BRICK_HCO_CODE
    AND fbw.DIM_BRAND_KEY = qty.DIM_BRAND_KEY
    AND fbw.DIM_PRODUCT_ALL_KEY = qty.DIM_PRODUCT_ALL_KEY -- AND     fbw.PRODUCT_UID             = qty.PRODUCT_UID
    -- AND     fbw.CURRENCY_KEY            = qty.CURRENCY_KEY
    AND fbw.DIM_METRIC_KEY = COALESCE(qty.DIM_METRIC_KEY, -1)
    AND fbw.DIM_FREQUENCY_KEY = qty.DIM_FREQUENCY_KEY -- AND     fbw.SALE_YEAR               = qty.SALE_YEAR
    AND fbw.REFERENCE_SALE_YEAR = qty.SALE_YEAR
    AND fbw.WEEK_NUMBER = qty.WEEK_NUMBER -- LEFT JOIN   DIM_BRICK db ON fbw.BRICK_KEY = db.BRICK_KEY
    LEFT JOIN DW_CENTRAL.DIM_BRAND dbr ON fbw.DIM_BRAND_KEY = dbr.DIM_BRAND_KEY
    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpd ON fbw.DIM_PRODUCT_ALL_KEY = dpd.DIM_PRODUCT_ALL_KEY
    INNER JOIN DW_CENTRAL.DIM_METRIC dm ON fbw.DIM_METRIC_KEY = dm.DIM_METRIC_KEY
    INNER JOIN DW_CENTRAL.DIM_FREQUENCY df ON fbw.DIM_FREQUENCY_KEY = df.DIM_FREQUENCY_KEY
ORDER BY
    fbw.DIM_BRICK_KEY,
    fbw.DIM_BRAND_KEY,
    fbw.DIM_PRODUCT_ALL_KEY,
    -- fbw.PRODUCT_UID,
    -- fbw.DIM_CURRENCY_KEY,
    fbw.DIM_METRIC_KEY,
    -- fbw.REFERENCE_SALE_YEAR,
    -- fbw.WEEK_NUMBER,
    fbw.REFERENCE_SALE_DATE;


CREATE VIEW VW_ACCOUNT_WEEKLY_MARKETBASKET_VOLUME_CUSTOM -- Market Basket Volume is the rolling sum for the Rx (US) or Units (non-US)
-- for all of the products in the Market Basket by Account or Brick.
-- Rows with negative quantity are treated as 0 quantity values.
-- max(MARKET_METRIC_AS_OF_DATE) aligns with max(MARKET_METRIC_AS_OF_DATE) from F_ACCOUNT_WEEKLY.
AS
SELECT
    A.ACCOUNT_ID,
    A.ACCOUNT_UID,
    A.SALES_REFERENCE_ACCOUNT_ID,
    A.ACCOUNT_NAME,
    A.MARKETBASKET_NAME,
    A.PRODUCT_IDENTIFIER,
    A.PRODUCT_UID,
    A.PRODUCT_NAME,
    A.PRODUCT_NAME_ENGLISH,
    A.METRIC_NAME,
    A.MARKET_METRIC_AS_OF_DATE,
    A.PERIOD_NUMBER,
    -- A.QUANTITY AS PRODUCT_VOLUME_SUM,
    CASE
        WHEN A.PERIOD_NUMBER = 1 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 2 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 3 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 4 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 5 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 6 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 7 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 8 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 9 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 10 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 11 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 12 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 13 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.ACCOUNT_ID,
            A.ACCOUNT_UID,
            A.SALES_REFERENCE_ACCOUNT_ID,
            A.ACCOUNT_NAME,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.SALES_REFERENCE_ACCOUNT_ID,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                AND CURRENT ROW
        )
        ELSE 0.00000
    END AS ROLLING_PRODUCT_VOLUME_SUM,
    ROLLING_MARKETBASKET_SUM
FROM
    (
        SELECT
            DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_PRODUCT_ALL_KEY,
            DIM_METRIC_KEY,
            DIM_MARKETBASKET_KEY,
            ACCOUNT_ID,
            ACCOUNT_UID,
            SALES_REFERENCE_ACCOUNT_ID,
            ACCOUNT_NAME,
            MARKETBASKET_NAME,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            MARKET_METRIC_AS_OF_DATE,
            MARKET_METRIC_AS_OF_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            MARKET_VOLUME
        FROM
            (
                SELECT
                    DIM_EXTERNAL_ACCOUNT_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    DIM_METRIC_KEY,
                    DIM_MARKETBASKET_KEY,
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER,
                    -- SUM(QUANTITY) AS QUANTITY
                    CASE
                        WHEN SUM(MARKET_VOLUME) < 0 THEN 0
                        ELSE SUM(MARKET_VOLUME)
                    END AS MARKET_VOLUME
                FROM
                    SCD.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP fwr
                    /*
                     INNER JOIN (SELECT  COALESCE(pm.DIM_PRODUCT_ALL_KEY,-1) AS PRODUCT_ALL_KEY,
                     DIM_MARKETBASKET_KEY,
                     dpe.PRODUCT_NAME
                     FROM  DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm 
                     LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY
                     ) pm
                     ON      fwr.DIM_PRODUCT_ALL_KEY   = pm.PRODUCT_ALL_KEY
                     AND     fwr.PRODUCT_NAME          = pm.PRODUCT_NAME
                     INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                     */
                WHERE
                    MARKET_METRIC_AS_OF_DATE <= (
                        SELECT
                            MAX(MARKET_METRIC_AS_OF_DATE)
                        FROM
                            DW_CENTRAL.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM
                    ) -- AND SALES_REFERENCE_ACCOUNT_ID = '2489e17'
                    -- AND MARKETBASKET_NAME = '***ntre*****'
                    -- AND MARKET_METRIC_AS_OF_DATE = '2020-01-10'
                    -- AND METRIC_NAME = 'TRx'
                GROUP BY
                    DIM_EXTERNAL_ACCOUNT_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    DIM_METRIC_KEY,
                    DIM_MARKETBASKET_KEY,
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER --,PERIOD_NUMBER
                    -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,mb.MARKETBASKET_NAME,PRODUCT_NAME,METRIC_NAME,MARKET_METRIC_AS_OF_DATE,MARKET_METRIC_AS_OF_YEAR,WEEK_NUMBER --,PERIOD_NUMBER
            ) AS d
            CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,METRIC_NAME,YEAR,MARKET_METRIC_AS_OF_DATE,PERIOD_NUMBER
            -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,PERIOD_NUMBER,MARKET_METRIC_AS_OF_DATE
    ) AS A
    INNER JOIN (
        SELECT
            DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_PRODUCT_ALL_KEY,
            DIM_METRIC_KEY,
            DIM_MARKETBASKET_KEY,
            ACCOUNT_ID,
            ACCOUNT_UID,
            SALES_REFERENCE_ACCOUNT_ID,
            ACCOUNT_NAME,
            MARKETBASKET_NAME,
            METRIC_NAME,
            MARKET_METRIC_AS_OF_DATE,
            MARKET_METRIC_AS_OF_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            MARKET_VOLUME AS MARKETVOLUME_SUM,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 2 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 5 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        SALES_REFERENCE_ACCOUNT_ID,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_MARKETBASKET_SUM
        FROM
            (
                SELECT
                    DIM_EXTERNAL_ACCOUNT_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    DIM_METRIC_KEY,
                    DIM_MARKETBASKET_KEY,
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    MARKET_VOLUME
                FROM
                    (
                        SELECT
                            DIM_EXTERNAL_ACCOUNT_KEY,
                            DIM_PRODUCT_ALL_KEY,
                            DIM_METRIC_KEY,
                            DIM_MARKETBASKET_KEY,
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            MARKETBASKET_NAME,
                            METRIC_NAME,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            CASE
                                WHEN SUM(MARKET_VOLUME) < 0 THEN 0
                                ELSE SUM(MARKET_VOLUME)
                            END AS MARKET_VOLUME
                        FROM
                            SCD.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP fwr
                            /*
                             INNER JOIN (SELECT    COALESCE(pm.DIM_PRODUCT_ALL_KEY,-1) AS PRODUCT_ALL_KEY,
                             DIM_MARKETBASKET_KEY,
                             dpe.PRODUCT_NAME
                             FROM  DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm 
                             LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe 
                             ON    pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY
                             ) pm
                             ON      fwr.DIM_PRODUCT_ALL_KEY   = pm.PRODUCT_ALL_KEY
                             AND     fwr.PRODUCT_NAME          = pm.PRODUCT_NAME
                             INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                             */
                        WHERE
                            fwr.MARKET_METRIC_AS_OF_DATE <= (
                                SELECT
                                    MAX(MARKET_METRIC_AS_OF_DATE)
                                FROM
                                    DW_CENTRAL.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM
                            ) -- AND SALES_REFERENCE_ACCOUNT_ID = '2489e17'
                            -- AND MARKETBASKET_NAME = '***ntre*****'
                            -- AND MARKET_METRIC_AS_OF_DATE = '2020-01-10'
                            -- AND METRIC_NAME = 'TRx'
                        GROUP BY
                            DIM_EXTERNAL_ACCOUNT_KEY,
                            DIM_PRODUCT_ALL_KEY,
                            DIM_METRIC_KEY,
                            DIM_MARKETBASKET_KEY,
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            MARKETBASKET_NAME,
                            METRIC_NAME,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER
                    ) AS mkt_basket_inner
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp
            ) AS mkt_basket -- ORDER BY  SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,METRIC_NAME,PERIOD_NUMBER,MARKET_METRIC_AS_OF_DATE
    ) AS B ON A.DIM_EXTERNAL_ACCOUNT_KEY = B.DIM_EXTERNAL_ACCOUNT_KEY
    AND A.DIM_PRODUCT_ALL_KEY = B.DIM_PRODUCT_ALL_KEY
    AND A.DIM_METRIC_KEY = B.DIM_METRIC_KEY
    AND A.DIM_MARKETBASKET_KEY = B.DIM_MARKETBASKET_KEY
    AND A.MARKET_METRIC_AS_OF_DATE = B.MARKET_METRIC_AS_OF_DATE
    AND A.PERIOD_NUMBER = B.PERIOD_NUMBER
    /*
     ON      A.SALES_REFERENCE_ACCOUNT_ID = B.SALES_REFERENCE_ACCOUNT_ID
     AND     A.MARKETBASKET_NAME = B.MARKETBASKET_NAME
     AND     A.METRIC_NAME       = B.METRIC_NAME
     AND     A.MARKET_METRIC_AS_OF_DATE         = B.MARKET_METRIC_AS_OF_DATE
     AND     A.PERIOD_NUMBER     = B.PERIOD_NUMBER
     */
    -- ORDER BY A.ACCOUNT_ID,A.ACCOUNT_UID,A.ACCOUNT_NAME,A.SALES_REFERENCE_ACCOUNT_ID,A.MARKETBASKET_NAME,A.PRODUCT_ID,A.PRODUCT_NAME,A.METRIC_NAME,A.MARKET_METRIC_AS_OF_YEAR,A.PERIOD_NUMBER,A.MARKET_METRIC_AS_OF_DATE
;

CREATE VIEW VW_ACCOUNT_WEEKLY_MARKET_SHARE_CUSTOM AS
SELECT
    final.ACCOUNT_ID,
    final.ACCOUNT_UID,
    final.SALES_REFERENCE_ACCOUNT_ID,
    final.ACCOUNT_NAME,
    final.MARKETBASKET_NAME,
    final.PRODUCT_IDENTIFIER,
    final.PRODUCT_UID,
    final.PRODUCT_NAME,
    final.PRODUCT_NAME_ENGLISH,
    final.METRIC_NAME,
    final.MARKET_METRIC_AS_OF_DATE,
    final.PERIOD_NUMBER,
    final.MARKET_SHARE_VALUE,
    final.ROLLING_PRODUCT_MARKET_SHARE_SUM,
    vw.ROLLING_MARKETBASKET_SUM,
    final.ROLLING_PRODUCT_MARKET_SHARE_SUM / NULLIF(vw.ROLLING_MARKETBASKET_SUM, 0) * 100 AS ROLLING_MARKET_SHARE
FROM
    (
        SELECT
            rms.ACCOUNT_ID,
            rms.ACCOUNT_UID,
            rms.SALES_REFERENCE_ACCOUNT_ID,
            rms.ACCOUNT_NAME,
            rms.MARKETBASKET_NAME,
            rms.PRODUCT_IDENTIFIER,
            rms.PRODUCT_UID,
            rms.PRODUCT_NAME,
            rms.PRODUCT_NAME_ENGLISH,
            rms.METRIC_NAME,
            rms.MARKET_METRIC_AS_OF_DATE,
            rms.MARKET_METRIC_AS_OF_YEAR,
            rms.WEEK_NUMBER,
            rms.PERIOD_NUMBER,
            rms.MARKET_SHARE_VALUE,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 2 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 5 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.ACCOUNT_ID,
                    rms.ACCOUNT_UID,
                    rms.SALES_REFERENCE_ACCOUNT_ID,
                    rms.ACCOUNT_NAME,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.SALES_REFERENCE_ACCOUNT_ID,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_PRODUCT_MARKET_SHARE_SUM -- ROLLING_PRODUCT_SUM
        FROM
            (
                SELECT
                    ACCOUNT_ID,
                    ACCOUNT_UID,
                    SALES_REFERENCE_ACCOUNT_ID,
                    ACCOUNT_NAME,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    MARKET_SHARE_VALUE
                FROM
                    (
                        SELECT
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            MARKETBASKET_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            METRIC_NAME,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            -- SUM(QUANTITY) AS QUANTITY
                            CASE
                                WHEN SUM(MARKET_SHARE_VALUE) < 0 THEN 0
                                ELSE SUM(MARKET_SHARE_VALUE)
                            END AS MARKET_SHARE_VALUE
                        FROM
                            SCD.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP fwr
                        WHERE
                            fwr.MARKET_METRIC_AS_OF_DATE <= (
                                SELECT
                                    MAX(MARKET_METRIC_AS_OF_DATE)
                                FROM
                                    DW_CENTRAL.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM
                            ) -- AND     SALES_REFERENCE_ACCOUNT_ID = 'e580e33'
                            -- AND     PRODUCT_NAME LIKE '***L OT ACE%'
                            -- AND     YEAR = 2020
                        GROUP BY
                            ACCOUNT_ID,
                            ACCOUNT_UID,
                            SALES_REFERENCE_ACCOUNT_ID,
                            ACCOUNT_NAME,
                            MARKETBASKET_NAME,
                            METRIC_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER -- ORDER BY BRICK_HCO_CODE,mb.MARKETBASKET_NAME,METRIC_NAME,PRODUCT_ID,PRODUCT_UID,PRODUCT_NAME,MARKET_METRIC_AS_OF_DATE,YEAR,PERIOD_NUMBER
                    ) AS inner_result
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- order by BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,PRODUCT_ID,PRODUCT_NAME,PERIOD_NUMBER,MARKET_METRIC_AS_OF_DATE
            ) as rms
    ) AS final
    INNER JOIN SCD.VW_ACCOUNT_WEEKLY_MARKETBASKET_VOLUME_CUSTOM vw -- ON          COALESCE(vw.ACCOUNT_ID,-1)       = COALESCE(final.ACCOUNT_ID,-1)
    -- AND         COALESCE(vw.ACCOUNT_UID,'99999') = COALESCE(final.ACCOUNT_UID,'99999')
    ON vw.SALES_REFERENCE_ACCOUNT_ID = final.SALES_REFERENCE_ACCOUNT_ID
    AND vw.MARKETBASKET_NAME = final.MARKETBASKET_NAME
    AND vw.PRODUCT_IDENTIFIER = final.PRODUCT_IDENTIFIER
    AND COALESCE(vw.PRODUCT_UID, 'NA') = COALESCE(final.PRODUCT_UID, 'NA')
    AND vw.PRODUCT_NAME = final.PRODUCT_NAME
    AND vw.METRIC_NAME = final.METRIC_NAME
    AND vw.MARKET_METRIC_AS_OF_DATE = final.MARKET_METRIC_AS_OF_DATE
    AND vw.PERIOD_NUMBER = final.PERIOD_NUMBER
ORDER BY
    final.SALES_REFERENCE_ACCOUNT_ID,
    final.MARKETBASKET_NAME,
    final.METRIC_NAME,
    final.PRODUCT_NAME,
    PERIOD_NUMBER,
    MARKET_METRIC_AS_OF_DATE;

CREATE
OR REPLACE VIEW VW_BRICK_WEEKLY_MARKETBASKET_VOLUME_CUSTOM -- Market Basket Volume is the rolling sum for the Rx (US) or Units (non-US)
-- for all of the products in the Market Basket by Account or Brick.
-- Rows with negative quantity are treated as 0 quantity values.
-- max(MARKET_METRIC_AS_OF_DATE) aligns with max(MARKET_METRIC_AS_OF_DATE) from F_ACCOUNT_WEEKLY.
AS
SELECT
    A.BRICK_HCO_CODE,
    A.MARKETBASKET_NAME,
    A.PRODUCT_IDENTIFIER,
    A.PRODUCT_UID,
    A.PRODUCT_NAME,
    A.PRODUCT_NAME_ENGLISH,
    A.METRIC_NAME,
    A.MARKET_METRIC_AS_OF_DATE,
    A.PERIOD_NUMBER,
    -- A.QUANTITY AS PRODUCT_VOLUME_SUM,
    CASE
        WHEN A.PERIOD_NUMBER = 1 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 2 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 3 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 4 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 5 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 6 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 7 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 8 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 9 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 10 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 11 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 12 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                AND CURRENT ROW
        )
        WHEN A.PERIOD_NUMBER = 13 THEN SUM(A.MARKET_VOLUME) OVER (
            PARTITION BY A.BRICK_HCO_CODE,
            A.MARKETBASKET_NAME,
            A.PRODUCT_IDENTIFIER,
            A.PRODUCT_NAME,
            A.METRIC_NAME,
            A.MARKET_METRIC_AS_OF_YEAR,
            A.PERIOD_NUMBER
            ORDER BY
                A.BRICK_HCO_CODE,
                A.MARKETBASKET_NAME,
                A.PRODUCT_IDENTIFIER,
                A.PRODUCT_NAME,
                A.MARKET_METRIC_AS_OF_DATE,
                A.PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                AND CURRENT ROW
        )
        ELSE 0.00000
    END AS ROLLING_PRODUCT_VOLUME_SUM,
    ROLLING_MARKETBASKET_SUM
FROM
    (
        SELECT
            DIM_BRICK_KEY,
            DIM_PRODUCT_ALL_KEY,
            DIM_METRIC_KEY,
            DIM_MARKETBASKET_KEY,
            BRICK_HCO_CODE,
            MARKETBASKET_NAME,
            PRODUCT_IDENTIFIER,
            PRODUCT_UID,
            PRODUCT_NAME,
            PRODUCT_NAME_ENGLISH,
            METRIC_NAME,
            MARKET_METRIC_AS_OF_DATE,
            MARKET_METRIC_AS_OF_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            MARKET_VOLUME
        FROM
            (
                SELECT
                    DIM_BRICK_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    DIM_METRIC_KEY,
                    DIM_MARKETBASKET_KEY,
                    BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER,
                    -- SUM(QUANTITY) AS QUANTITY
                    CASE
                        WHEN SUM(MARKET_VOLUME) < 0 THEN 0
                        ELSE SUM(MARKET_VOLUME)
                    END AS MARKET_VOLUME
                FROM
                    SCD.F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP fwr
                    /*
                     INNER JOIN (SELECT  COALESCE(pm.DIM_PRODUCT_ALL_KEY,-1) AS PRODUCT_ALL_KEY,
                     DIM_MARKETBASKET_KEY,
                     dpe.PRODUCT_NAME
                     FROM  DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm 
                     LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe ON pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY
                     ) pm
                     ON      fwr.DIM_PRODUCT_ALL_KEY   = pm.PRODUCT_ALL_KEY
                     AND     fwr.PRODUCT_NAME          = pm.PRODUCT_NAME
                     INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                     */
                WHERE
                    MARKET_METRIC_AS_OF_DATE <= (
                        SELECT
                            MAX(MARKET_METRIC_AS_OF_DATE)
                        FROM
                            DW_CENTRAL.F_BRICK_WEEKLY_MARKETDATA_CUSTOM
                    ) -- AND SALES_REFERENCE_ACCOUNT_ID = '2489e17'
                    -- AND MARKETBASKET_NAME = '***ntre*****'
                    -- AND MARKET_METRIC_AS_OF_DATE = '2020-01-10'
                    -- AND METRIC_NAME = 'TRx'
                GROUP BY
                    DIM_BRICK_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    DIM_METRIC_KEY,
                    DIM_MARKETBASKET_KEY,
                    BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER --,PERIOD_NUMBER
                    -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,mb.MARKETBASKET_NAME,PRODUCT_NAME,METRIC_NAME,MARKET_METRIC_AS_OF_DATE,MARKET_METRIC_AS_OF_YEAR,WEEK_NUMBER --,PERIOD_NUMBER
            ) AS d
            CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,METRIC_NAME,YEAR,MARKET_METRIC_AS_OF_DATE,PERIOD_NUMBER
            -- ORDER BY SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,PRODUCT_ID,PRODUCT_NAME,METRIC_NAME,PERIOD_NUMBER,MARKET_METRIC_AS_OF_DATE
    ) AS A
    INNER JOIN (
        SELECT
            DIM_BRICK_KEY,
            DIM_PRODUCT_ALL_KEY,
            DIM_METRIC_KEY,
            DIM_MARKETBASKET_KEY,
            BRICK_HCO_CODE,
            MARKETBASKET_NAME,
            METRIC_NAME,
            MARKET_METRIC_AS_OF_DATE,
            MARKET_METRIC_AS_OF_YEAR,
            WEEK_NUMBER,
            PERIOD_NUMBER,
            MARKET_VOLUME AS MARKETVOLUME_SUM,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 2 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 5 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(MARKET_VOLUME) OVER (
                    PARTITION BY BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        BRICK_HCO_CODE,
                        MARKETBASKET_NAME,
                        MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_MARKETBASKET_SUM
        FROM
            (
                SELECT
                    DIM_BRICK_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    DIM_METRIC_KEY,
                    DIM_MARKETBASKET_KEY,
                    BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    MARKET_VOLUME
                FROM
                    (
                        SELECT
                            DIM_BRICK_KEY,
                            DIM_PRODUCT_ALL_KEY,
                            DIM_METRIC_KEY,
                            DIM_MARKETBASKET_KEY,
                            BRICK_HCO_CODE,
                            MARKETBASKET_NAME,
                            METRIC_NAME,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            CASE
                                WHEN SUM(MARKET_VOLUME) < 0 THEN 0
                                ELSE SUM(MARKET_VOLUME)
                            END AS MARKET_VOLUME
                        FROM
                            SCD.F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP fwr
                            /*
                             INNER JOIN (SELECT    COALESCE(pm.DIM_PRODUCT_ALL_KEY,-1) AS PRODUCT_ALL_KEY,
                             DIM_MARKETBASKET_KEY,
                             dpe.PRODUCT_NAME
                             FROM  DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING pm 
                             LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpe 
                             ON    pm.DIM_PRODUCT_ALL_KEY = dpe.DIM_PRODUCT_ALL_KEY
                             ) pm
                             ON      fwr.DIM_PRODUCT_ALL_KEY   = pm.PRODUCT_ALL_KEY
                             AND     fwr.PRODUCT_NAME          = pm.PRODUCT_NAME
                             INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mb ON pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
                             */
                        WHERE
                            fwr.MARKET_METRIC_AS_OF_DATE <= (
                                SELECT
                                    MAX(MARKET_METRIC_AS_OF_DATE)
                                FROM
                                    DW_CENTRAL.F_BRICK_WEEKLY_MARKETDATA_CUSTOM
                            ) -- AND SALES_REFERENCE_ACCOUNT_ID = '2489e17'
                            -- AND MARKETBASKET_NAME = '***ntre*****'
                            -- AND MARKET_METRIC_AS_OF_DATE = '2020-01-10'
                            -- AND METRIC_NAME = 'TRx'
                        GROUP BY
                            DIM_BRICK_KEY,
                            DIM_PRODUCT_ALL_KEY,
                            DIM_METRIC_KEY,
                            DIM_MARKETBASKET_KEY,
                            BRICK_HCO_CODE,
                            MARKETBASKET_NAME,
                            METRIC_NAME,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER
                    ) AS mkt_basket_inner
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp
            ) AS mkt_basket -- ORDER BY  SALES_REFERENCE_ACCOUNT_ID,MARKETBASKET_NAME,METRIC_NAME,PERIOD_NUMBER,MARKET_METRIC_AS_OF_DATE
    ) AS B ON A.DIM_BRICK_KEY = B.DIM_BRICK_KEY
    AND A.DIM_PRODUCT_ALL_KEY = B.DIM_PRODUCT_ALL_KEY
    AND A.DIM_METRIC_KEY = B.DIM_METRIC_KEY
    AND A.DIM_MARKETBASKET_KEY = B.DIM_MARKETBASKET_KEY
    AND A.MARKET_METRIC_AS_OF_DATE = B.MARKET_METRIC_AS_OF_DATE
    AND A.PERIOD_NUMBER = B.PERIOD_NUMBER
    /*
     ON      A.SALES_REFERENCE_ACCOUNT_ID = B.SALES_REFERENCE_ACCOUNT_ID
     AND     A.MARKETBASKET_NAME = B.MARKETBASKET_NAME
     AND     A.METRIC_NAME       = B.METRIC_NAME
     AND     A.MARKET_METRIC_AS_OF_DATE         = B.MARKET_METRIC_AS_OF_DATE
     AND     A.PERIOD_NUMBER     = B.PERIOD_NUMBER
     */
    -- ORDER BY A.ACCOUNT_ID,A.ACCOUNT_UID,A.ACCOUNT_NAME,A.SALES_REFERENCE_ACCOUNT_ID,A.MARKETBASKET_NAME,A.PRODUCT_ID,A.PRODUCT_NAME,A.METRIC_NAME,A.MARKET_METRIC_AS_OF_YEAR,A.PERIOD_NUMBER,A.MARKET_METRIC_AS_OF_DATE
;

CREATE
OR REPLACE VIEW VW_BRICK_WEEKLY_MARKET_SHARE_CUSTOM AS
SELECT
    final.BRICK_HCO_CODE,
    final.MARKETBASKET_NAME,
    final.PRODUCT_IDENTIFIER,
    final.PRODUCT_UID,
    final.PRODUCT_NAME,
    final.PRODUCT_NAME_ENGLISH,
    final.METRIC_NAME,
    final.MARKET_METRIC_AS_OF_DATE,
    final.PERIOD_NUMBER,
    final.MARKET_SHARE_VALUE,
    final.ROLLING_PRODUCT_MARKET_SHARE_SUM,
    vw.ROLLING_MARKETBASKET_SUM,
    final.ROLLING_PRODUCT_MARKET_SHARE_SUM / NULLIF(vw.ROLLING_MARKETBASKET_SUM, 0) * 100 AS ROLLING_MARKET_SHARE
FROM
    (
        SELECT
            rms.BRICK_HCO_CODE,
            rms.MARKETBASKET_NAME,
            rms.PRODUCT_IDENTIFIER,
            rms.PRODUCT_UID,
            rms.PRODUCT_NAME,
            rms.PRODUCT_NAME_ENGLISH,
            rms.METRIC_NAME,
            rms.MARKET_METRIC_AS_OF_DATE,
            rms.MARKET_METRIC_AS_OF_YEAR,
            rms.WEEK_NUMBER,
            rms.PERIOD_NUMBER,
            rms.MARKET_SHARE_VALUE,
            CASE
                WHEN PERIOD_NUMBER = 1 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 0 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 2 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 1 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 3 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 2 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 4 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 3 PRECEDING
                        AND CURRENT ROW
                ) -- /NULLIF(vw.ROLLING_WEEK_SUM,0)) * 100
                WHEN PERIOD_NUMBER = 5 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 4 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 6 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 5 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 7 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 6 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 8 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 7 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 9 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 8 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 10 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 9 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 11 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 10 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 12 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 11 PRECEDING
                        AND CURRENT ROW
                )
                WHEN PERIOD_NUMBER = 13 THEN SUM(rms.MARKET_SHARE_VALUE) OVER (
                    PARTITION BY rms.BRICK_HCO_CODE,
                    rms.MARKETBASKET_NAME,
                    rms.PRODUCT_IDENTIFIER,
                    rms.PRODUCT_NAME,
                    rms.METRIC_NAME,
                    MARKET_METRIC_AS_OF_YEAR,
                    PERIOD_NUMBER
                    ORDER BY
                        rms.BRICK_HCO_CODE,
                        rms.MARKETBASKET_NAME,
                        rms.METRIC_NAME,
                        rms.PRODUCT_NAME,
                        rms.MARKET_METRIC_AS_OF_DATE,
                        PERIOD_NUMBER ROWS BETWEEN 12 PRECEDING
                        AND CURRENT ROW
                )
                ELSE 0.00000
            END AS ROLLING_PRODUCT_MARKET_SHARE_SUM -- ROLLING_PRODUCT_SUM
        FROM
            (
                SELECT
                    BRICK_HCO_CODE,
                    MARKETBASKET_NAME,
                    PRODUCT_IDENTIFIER,
                    PRODUCT_UID,
                    PRODUCT_NAME,
                    PRODUCT_NAME_ENGLISH,
                    METRIC_NAME,
                    MARKET_METRIC_AS_OF_DATE,
                    MARKET_METRIC_AS_OF_YEAR,
                    WEEK_NUMBER,
                    PERIOD_NUMBER,
                    MARKET_SHARE_VALUE
                FROM
                    (
                        SELECT
                            BRICK_HCO_CODE,
                            MARKETBASKET_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            METRIC_NAME,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER,
                            -- PERIOD_NUMBER,
                            -- SUM(QUANTITY) AS QUANTITY
                            CASE
                                WHEN SUM(MARKET_SHARE_VALUE) < 0 THEN 0
                                ELSE SUM(MARKET_SHARE_VALUE)
                            END AS MARKET_SHARE_VALUE
                        FROM
                            SCD.F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP fwr
                        WHERE
                            fwr.MARKET_METRIC_AS_OF_DATE <= (
                                SELECT
                                    MAX(MARKET_METRIC_AS_OF_DATE)
                                FROM
                                    DW_CENTRAL.F_BRICK_WEEKLY_MARKETDATA_CUSTOM
                            ) -- AND     SALES_REFERENCE_ACCOUNT_ID = 'e580e33'
                            -- AND     PRODUCT_NAME LIKE '***L OT ACE%'
                            -- AND     YEAR = 2020
                        GROUP BY
                            BRICK_HCO_CODE,
                            MARKETBASKET_NAME,
                            METRIC_NAME,
                            PRODUCT_IDENTIFIER,
                            PRODUCT_UID,
                            PRODUCT_NAME,
                            PRODUCT_NAME_ENGLISH,
                            MARKET_METRIC_AS_OF_DATE,
                            MARKET_METRIC_AS_OF_YEAR,
                            WEEK_NUMBER -- ORDER BY BRICK_HCO_CODE,mb.MARKETBASKET_NAME,METRIC_NAME,PRODUCT_ID,PRODUCT_UID,PRODUCT_NAME,MARKET_METRIC_AS_OF_DATE,YEAR,PERIOD_NUMBER
                    ) AS inner_result
                    CROSS JOIN DW_CENTRAL.DIM_PERIOD dp -- order by BRICK_HCO_CODE,MARKETBASKET_NAME,METRIC_NAME,PRODUCT_ID,PRODUCT_NAME,PERIOD_NUMBER,MARKET_METRIC_AS_OF_DATE
            ) as rms
    ) AS final
    INNER JOIN SCD.VW_BRICK_WEEKLY_MARKETBASKET_VOLUME_CUSTOM vw -- ON          COALESCE(vw.ACCOUNT_ID,-1)       = COALESCE(final.ACCOUNT_ID,-1)
    -- AND         COALESCE(vw.ACCOUNT_UID,'99999') = COALESCE(final.ACCOUNT_UID,'99999')
    ON vw.BRICK_HCO_CODE = final.BRICK_HCO_CODE
    AND vw.MARKETBASKET_NAME = final.MARKETBASKET_NAME
    AND vw.PRODUCT_IDENTIFIER = final.PRODUCT_IDENTIFIER
    AND COALESCE(vw.PRODUCT_UID, 'NA') = COALESCE(final.PRODUCT_UID, 'NA')
    AND vw.PRODUCT_NAME = final.PRODUCT_NAME
    AND vw.METRIC_NAME = final.METRIC_NAME
    AND vw.MARKET_METRIC_AS_OF_DATE = final.MARKET_METRIC_AS_OF_DATE
    AND vw.PERIOD_NUMBER = final.PERIOD_NUMBER
ORDER BY
    final.BRICK_HCO_CODE,
    final.MARKETBASKET_NAME,
    final.METRIC_NAME,
    final.PRODUCT_NAME,
    PERIOD_NUMBER,
    MARKET_METRIC_AS_OF_DATE;

CREATE
OR REPLACE VIEW VW_F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP AS
SELECT
    fbw.DIM_BRICK_KEY,
    fbw.DIM_BRAND_KEY,
    fbw.DIM_PRODUCT_ALL_KEY AS DIM_PRODUCT_ALL_KEY,
    NULL AS DIM_PRODUCT_EXTENSION_KEY,
    fbw.DIM_CURRENCY_KEY,
    fbw.DIM_METRIC_KEY,
    fbw.DIM_FREQUENCY_KEY,
    fbw.DIM_MARKETBASKET_KEY,
    fbw.BRICK_HCO_CODE,
    dbr.BRAND_NAME,
    -- IFF(dse.ACCOUNT_KEY > 0, dse.ACCOUNT_NAME, daext.ACCOUNT_NAME) AS ACCOUNT_NAME,
    -- dse.ACCOUNT_ID AS ACCOUNT_ID,
    -- fbw.ACCOUNT_UID,
    -- COALESCE(dse.SALES_REFERENCE_ACCOUNT_ID,daext.SALES_REFERENCE_ACCOUNT_ID) AS SALES_REFERENCE_ACCOUNT_ID,
    dpd.PRODUCT_NAME,
    dpd.PRODUCT_NAME_ENGLISH,
    dpd.PRODUCT_IDENTIFIER,
    fbw.PRODUCT_UID,
    NULL AS PRODUCT_STRENGTH,
    NULL AS PRODUCT_PACKAGE,
    fbw.CURRENCY_CODE,
    dm.METRIC_NAME,
    df.FREQUENCY_NAME,
    mkt.MARKETBASKET_NAME,
    REFERENCE_SALE_DATE AS SALE_DATE,
    fbw.REFERENCE_SALE_YEAR AS SALE_YEAR,
    fbw.WEEK_NUMBER,
    COALESCE(qty.MARKET_SHARE_VALUE, 0.000000) AS MARKET_SHARE_VALUE,
    COALESCE(qty.MARKET_VOLUME, 0.000000) AS MARKET_VOLUME
FROM
    (
        SELECT
            DISTINCT fbw.DIM_BRICK_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            -- PRODUCT_EXTENSION_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            DIM_MARKETBASKET_KEY,
            fbw.BRICK_HCO_CODE,
            PRODUCT_UID,
            CURRENCY_CODE,
            dd.WEEK_NUMBER,
            dd.REFERENCE_SALE_DATE,
            dd.REFERENCE_SALE_YEAR
        FROM
            (
                SELECT
                    DISTINCT DIM_BRICK_KEY,
                    DIM_BRAND_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    -- PRODUCT_EXTENSION_KEY,
                    DIM_CURRENCY_KEY,
                    DIM_METRIC_KEY,
                    DIM_FREQUENCY_KEY,
                    DIM_MARKETBASKET_KEY,
                    BRICK_HCO_CODE,
                    PRODUCT_UID,
                    CURRENCY_CODE,
                    -- AKT_REFERENCE_ID,
                    YEAR(MARKET_METRIC_AS_OF_DATE) AS MARKET_METRIC_YEAR
                FROM
                    DW_CENTRAL.F_BRICK_WEEKLY_MARKETDATA_CUSTOM -- WHERE   DIM_EXTERNAL_ACCOUNT_KEY = 1154 
                    --             AND SALE_DATE = '2020-02-17'
                    -- 		    AND DIM_PRODUCT_ALL_KEY = 516
            ) AS fbw
            CROSS JOIN (
                SELECT
                    DATE AS REFERENCE_SALE_DATE,
                    YEAR AS REFERENCE_SALE_YEAR,
                    WEEK_NUMBER
                FROM
                    DW_CENTRAL.DIM_DATE dd
                    INNER JOIN (
                        SELECT
                            YEAR(MIN(MARKET_METRIC_AS_OF_DATE)) AS MIN_METRIC_DATE,
                            DAYNAME(MIN(MARKET_METRIC_AS_OF_DATE)) AS DAYNAME
                        FROM
                            DW_CENTRAL.F_BRICK_WEEKLY_MARKETDATA_CUSTOM
                    ) AS fbw
                WHERE
                    -- DAY_NAME = 'Saturday'
                    LEFT(dd.DAY_NAME, 3) = fbw.DAYNAME
                    AND (
                        dd.YEAR >= fbw.MIN_METRIC_DATE
                        AND dd.YEAR <= YEAR(CURRENT_DATE())
                    )
            ) AS dd
        WHERE
            fbw.MARKET_METRIC_YEAR <= dd.REFERENCE_SALE_YEAR -- ORDER BY REFERENCE_SALE_DATE
    ) AS fbw
    LEFT JOIN (
        SELECT
            DIM_BRICK_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            -- PRODUCT_EXTENSION_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            DIM_MARKETBASKET_KEY,
            BRICK_HCO_CODE,
            PRODUCT_UID,
            CURRENCY_CODE,
            MARKET_METRIC_AS_OF_DATE,
            YEAR(MARKET_METRIC_AS_OF_DATE) AS MARKET_METRIC_YEAR,
            WEEKOFYEAR(MARKET_METRIC_AS_OF_DATE) AS WEEK_NUMBER,
            SUM(
                CASE
                    WHEN MARKET_SHARE_VALUE <= 0 THEN 0
                    ELSE MARKET_SHARE_VALUE
                END
            ) AS MARKET_SHARE_VALUE,
            SUM(
                CASE
                    WHEN MARKET_VOLUME <= 0 THEN 0
                    ELSE MARKET_VOLUME
                END
            ) AS MARKET_VOLUME -- select *
        FROM
            DW_CENTRAL.F_BRICK_WEEKLY_MARKETDATA_CUSTOM -- WHERE   DIM_EXTERNAL_ACCOUNT_KEY = 1154 
            --    AND SALE_DATE = '2020-02-17'
            --    AND DIM_PRODUCT_ALL_KEY = 516
            --    AND     METRIC_KEY = 1
        GROUP BY
            DIM_BRICK_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            DIM_MARKETBASKET_KEY,
            BRICK_HCO_CODE,
            PRODUCT_UID,
            CURRENCY_CODE,
            MARKET_METRIC_AS_OF_DATE,
            MARKET_METRIC_YEAR,
            WEEK_NUMBER
    ) AS qty ON fbw.DIM_BRICK_KEY = qty.DIM_BRICK_KEY -- AND     fbw.ACCOUNT_UID              = qty.ACCOUNT_UID
    AND fbw.DIM_BRAND_KEY = qty.DIM_BRAND_KEY
    AND fbw.DIM_PRODUCT_ALL_KEY = qty.DIM_PRODUCT_ALL_KEY -- AND		fbw.PRODUCT_EXTENSION_KEY	 = COALESCE(qty.PRODUCT_EXTENSION_KEY,-1)
    -- AND     fbw.PRODUCT_UID              = qty.PRODUCT_UID
    -- AND     fbw.CURRENCY_KEY             = qty.CURRENCY_KEY
    AND fbw.DIM_METRIC_KEY = COALESCE(qty.DIM_METRIC_KEY, -1)
    AND fbw.DIM_FREQUENCY_KEY = qty.DIM_FREQUENCY_KEY
    AND fbw.DIM_MARKETBASKET_KEY = qty.DIM_MARKETBASKET_KEY -- AND     fbw.SALE_YEAR               = qty.SALE_YEAR
    AND fbw.REFERENCE_SALE_YEAR = qty.MARKET_METRIC_YEAR
    AND fbw.WEEK_NUMBER = qty.WEEK_NUMBER -- AND		 fbw.AKT_REFERENCE_ID			= qty.AKT_REFERENCE_ID
    LEFT JOIN DW_CENTRAL.DIM_BRICK db ON fbw.DIM_BRICK_KEY = db.DIM_BRICK_KEY
    LEFT JOIN DW_CENTRAL.DIM_BRAND dbr ON fbw.DIM_BRAND_KEY = dbr.DIM_BRAND_KEY
    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpd ON fbw.DIM_PRODUCT_ALL_KEY = dpd.DIM_PRODUCT_ALL_KEY -- LEFT JOIN	 	DW_CENTRAL.DIM_PRODUCT_EXTENSION pext ON COALESCE(fbw.PRODUCT_EXTENSION_KEY,-1) = COALESCE(pext.PRODUCT_EXTENSION_KEY,-1)
    LEFT JOIN DW_CENTRAL.DIM_METRIC dm ON COALESCE(fbw.DIM_METRIC_KEY, -1) = COALESCE(dm.DIM_METRIC_KEY, -1)
    INNER JOIN DW_CENTRAL.DIM_FREQUENCY df ON fbw.DIM_FREQUENCY_KEY = df.DIM_FREQUENCY_KEY
    INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mkt ON fbw.DIM_MARKETBASKET_KEY = mkt.DIM_MARKETBASKET_KEY
ORDER BY
    fbw.DIM_BRICK_KEY,
    fbw.DIM_MARKETBASKET_KEY,
    fbw.DIM_PRODUCT_ALL_KEY,
    -- fbw.PRODUCT_UID,
    -- fbw.CURRENCY_KEY,
    fbw.DIM_METRIC_KEY,
    -- fbw.REFERENCE_SALE_YEAR,
    --  fbw.WEEK_NUMBER,
    fbw.REFERENCE_SALE_DATE;

CREATE
OR REPLACE VIEW VW_F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP AS
SELECT
    COALESCE(faw.DIM_EXTERNAL_ACCOUNT_KEY, -1) AS DIM_EXTERNAL_ACCOUNT_KEY,
    faw.DIM_BRAND_KEY,
    faw.DIM_PRODUCT_ALL_KEY AS DIM_PRODUCT_ALL_KEY,
    NULL AS DIM_PRODUCT_EXTENSION_KEY,
    faw.DIM_CURRENCY_KEY,
    faw.DIM_METRIC_KEY,
    faw.DIM_FREQUENCY_KEY,
    faw.DIM_MARKETBASKET_KEY,
    daext.ACCOUNT_NAME,
    dse.ACCOUNT_ID,
    faw.ACCOUNT_UID,
    daext.SALES_REFERENCE_ACCOUNT_ID,
    dbr.BRAND_NAME,
    -- IFF(dse.ACCOUNT_KEY > 0, dse.ACCOUNT_NAME, daext.ACCOUNT_NAME) AS ACCOUNT_NAME,
    -- dse.ACCOUNT_ID AS ACCOUNT_ID,
    -- faw.ACCOUNT_UID,
    -- COALESCE(dse.SALES_REFERENCE_ACCOUNT_ID,daext.SALES_REFERENCE_ACCOUNT_ID) AS SALES_REFERENCE_ACCOUNT_ID,
    dpd.PRODUCT_NAME,
    dpd.PRODUCT_NAME_ENGLISH,
    dpd.PRODUCT_IDENTIFIER,
    faw.PRODUCT_UID,
    NULL AS PRODUCT_STRENGTH,
    NULL AS PRODUCT_PACKAGE,
    faw.CURRENCY_CODE,
    dm.METRIC_NAME,
    df.FREQUENCY_NAME,
    mkt.MARKETBASKET_NAME,
    REFERENCE_SALE_DATE AS SALE_DATE,
    faw.REFERENCE_SALE_YEAR AS SALE_YEAR,
    faw.WEEK_NUMBER,
    COALESCE(qty.MARKET_SHARE_VALUE, 0.000000) AS MARKET_SHARE_VALUE,
    COALESCE(qty.MARKET_VOLUME, 0.000000) AS MARKET_VOLUME -- faw.DSE_PRODUCT_KEY,
    -- faw.EXTERNAL_PRODUCT_KEY,
    -- dpd.PRODUCT_NAME,dpex.PRODUCT_NAME,
    -- IFF(dpd.PRODUCT_KEY > 0, dpd.PRODUCT_NAME, dpex.PRODUCT_NAME) AS PRODUCT_NAME,
    -- IFF(dpd.PRODUCT_KEY > 0, dpd.PRODUCT_NAME_ENGLISH, dpex.PRODUCT_NAME_ENGLISH) AS PRODUCT_NAME_ENGLISH,
    -- dpd.PRODUCT_ID AS PRODUCT_ID,
    -- faw.PRODUCT_UID,
    -- faw.PRODUCT_EXTENSION_KEY,
    -- pext.PRODUCT_STRENGTH,
    -- pext.PRODUCT_PACKAGE,        
    -- COALESCE(SALE_DATE,REFERENCE_SALE_DATE) AS SALE_DATE,       
    -- faw.SALE_YEAR,
    -- qty.AKT_REFERENCE_ID
FROM
    (
        SELECT
            DISTINCT faw.DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            -- PRODUCT_EXTENSION_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            DIM_MARKETBASKET_KEY,
            ACCOUNT_UID,
            PRODUCT_UID,
            CURRENCY_CODE,
            dd.WEEK_NUMBER,
            dd.REFERENCE_SALE_DATE,
            dd.REFERENCE_SALE_YEAR
        FROM
            (
                SELECT
                    DISTINCT COALESCE(DIM_EXTERNAL_ACCOUNT_KEY, -1) AS DIM_EXTERNAL_ACCOUNT_KEY,
                    DIM_BRAND_KEY,
                    DIM_PRODUCT_ALL_KEY,
                    -- PRODUCT_EXTENSION_KEY,
                    DIM_CURRENCY_KEY,
                    DIM_METRIC_KEY,
                    DIM_FREQUENCY_KEY,
                    DIM_MARKETBASKET_KEY,
                    ACCOUNT_UID,
                    PRODUCT_UID,
                    CURRENCY_CODE,
                    -- AKT_REFERENCE_ID,
                    YEAR(MARKET_METRIC_AS_OF_DATE) AS MARKET_METRIC_YEAR
                FROM
                    DW_CENTRAL.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM -- WHERE   DIM_EXTERNAL_ACCOUNT_KEY = 1154 
                    --             AND SALE_DATE = '2020-02-17'
                    -- 		    AND DIM_PRODUCT_ALL_KEY = 516
            ) AS faw
            CROSS JOIN (
                SELECT
                    DATE AS REFERENCE_SALE_DATE,
                    YEAR AS REFERENCE_SALE_YEAR,
                    WEEK_NUMBER
                FROM
                    DW_CENTRAL.DIM_DATE dd
                    INNER JOIN (
                        SELECT
                            YEAR(MIN(MARKET_METRIC_AS_OF_DATE)) AS MIN_METRIC_DATE,
                            DAYNAME(MIN(MARKET_METRIC_AS_OF_DATE)) AS DAYNAME
                        FROM
                            DW_CENTRAL.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM
                    ) AS faw
                WHERE
                    -- DAY_NAME = 'Saturday'
                    LEFT(dd.DAY_NAME, 3) = faw.DAYNAME
                    AND (
                        dd.YEAR >= faw.MIN_METRIC_DATE
                        AND dd.YEAR <= YEAR(CURRENT_DATE())
                    )
            ) AS dd
        WHERE
            faw.MARKET_METRIC_YEAR <= dd.REFERENCE_SALE_YEAR -- ORDER BY REFERENCE_SALE_DATE
    ) AS faw
    LEFT JOIN (
        SELECT
            COALESCE(DIM_EXTERNAL_ACCOUNT_KEY, -1) AS DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            -- PRODUCT_EXTENSION_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            DIM_MARKETBASKET_KEY,
            ACCOUNT_UID,
            PRODUCT_UID,
            CURRENCY_CODE,
            MARKET_METRIC_AS_OF_DATE,
            YEAR(MARKET_METRIC_AS_OF_DATE) AS MARKET_METRIC_YEAR,
            WEEKOFYEAR(MARKET_METRIC_AS_OF_DATE) AS WEEK_NUMBER,
            SUM(
                CASE
                    WHEN MARKET_SHARE_VALUE <= 0 THEN 0
                    ELSE MARKET_SHARE_VALUE
                END
            ) AS MARKET_SHARE_VALUE,
            SUM(
                CASE
                    WHEN MARKET_VOLUME <= 0 THEN 0
                    ELSE MARKET_VOLUME
                END
            ) AS MARKET_VOLUME -- select *
        FROM
            DW_CENTRAL.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM -- WHERE   DIM_EXTERNAL_ACCOUNT_KEY = 1154 
            --    AND SALE_DATE = '2020-02-17'
            --    AND DIM_PRODUCT_ALL_KEY = 516
            --    AND     METRIC_KEY = 1
        GROUP BY
            DIM_EXTERNAL_ACCOUNT_KEY,
            DIM_BRAND_KEY,
            DIM_PRODUCT_ALL_KEY,
            DIM_CURRENCY_KEY,
            DIM_METRIC_KEY,
            DIM_FREQUENCY_KEY,
            DIM_MARKETBASKET_KEY,
            ACCOUNT_UID,
            PRODUCT_UID,
            CURRENCY_CODE,
            MARKET_METRIC_AS_OF_DATE,
            MARKET_METRIC_YEAR,
            WEEK_NUMBER
    ) AS qty ON faw.DIM_EXTERNAL_ACCOUNT_KEY = qty.DIM_EXTERNAL_ACCOUNT_KEY -- AND     faw.ACCOUNT_UID              = qty.ACCOUNT_UID
    AND faw.DIM_BRAND_KEY = qty.DIM_BRAND_KEY
    AND faw.DIM_PRODUCT_ALL_KEY = qty.DIM_PRODUCT_ALL_KEY -- AND		faw.PRODUCT_EXTENSION_KEY	 = COALESCE(qty.PRODUCT_EXTENSION_KEY,-1)
    -- AND     faw.PRODUCT_UID              = qty.PRODUCT_UID
    -- AND     faw.CURRENCY_KEY             = qty.CURRENCY_KEY
    AND faw.DIM_METRIC_KEY = COALESCE(qty.DIM_METRIC_KEY, -1)
    AND faw.DIM_FREQUENCY_KEY = qty.DIM_FREQUENCY_KEY
    AND faw.DIM_MARKETBASKET_KEY = qty.DIM_MARKETBASKET_KEY -- AND     faw.SALE_YEAR               = qty.SALE_YEAR
    AND faw.REFERENCE_SALE_YEAR = qty.MARKET_METRIC_YEAR
    AND faw.WEEK_NUMBER = qty.WEEK_NUMBER -- AND		 faw.AKT_REFERENCE_ID			= qty.AKT_REFERENCE_ID
    LEFT JOIN DW_CENTRAL.DIM_EXTERNAL_ACCOUNT daext ON faw.DIM_EXTERNAL_ACCOUNT_KEY = daext.DIM_EXTERNAL_ACCOUNT_KEY
    LEFT JOIN DW_CENTRAL.DIM_ACCOUNT dse ON dse.ACCOUNT_UID = daext.ACCOUNT_UID
    AND dse.RECORD_END_DATE > '3000-01-01' -- AND da.SOURCE_SYSTEM_NAME IN ('DSE','dse')
    LEFT JOIN DW_CENTRAL.DIM_BRAND dbr ON faw.DIM_BRAND_KEY = dbr.DIM_BRAND_KEY
    LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL dpd ON faw.DIM_PRODUCT_ALL_KEY = dpd.DIM_PRODUCT_ALL_KEY -- LEFT JOIN	 	DW_CENTRAL.DIM_PRODUCT_EXTENSION pext ON COALESCE(faw.PRODUCT_EXTENSION_KEY,-1) = COALESCE(pext.PRODUCT_EXTENSION_KEY,-1)
    LEFT JOIN DW_CENTRAL.DIM_METRIC dm ON COALESCE(faw.DIM_METRIC_KEY, -1) = COALESCE(dm.DIM_METRIC_KEY, -1)
    INNER JOIN DW_CENTRAL.DIM_FREQUENCY df ON faw.DIM_FREQUENCY_KEY = df.DIM_FREQUENCY_KEY
    INNER JOIN DW_CENTRAL.DIM_MARKETBASKET mkt ON faw.DIM_MARKETBASKET_KEY = mkt.DIM_MARKETBASKET_KEY
ORDER BY
    faw.DIM_EXTERNAL_ACCOUNT_KEY,
    faw.DIM_MARKETBASKET_KEY,
    faw.DIM_PRODUCT_ALL_KEY,
    -- faw.PRODUCT_UID,
    -- faw.CURRENCY_KEY,
    faw.DIM_METRIC_KEY,
    -- faw.REFERENCE_SALE_YEAR,
    --  faw.WEEK_NUMBER,
    faw.REFERENCE_SALE_DATE;