/*
 SQL to retrieve DDL of existing tables in a schema
 select concat(concat('select get_ddl(''table'', ''SCD.', table_name), ''');') ddl from INFORMATION_SCHEMA.TABLES
 where TABLE_CATALOG = 'DATALAKEEUCUSTOMER1_DWQA'
 and TABLE_SCHEMA = 'SCD';
 
 */
-- SCD_IN_PRODUCT_CONFIG has the list of all products with Sales data for at least one metric
-- Could be an internal product (DIM_PRODUCT_KEY is not null) or external (DIM_EXTERNAL_PRODUCT_KEY is not null)
-- Internal products would have PRODUCT_UID populated
-- DSE products will also have PRODUCT_ID populated
-- IS_SCD_ENABLED and IS_MARKET_CALC_CUSTOM are flags managed via the Learning UI for SCD enabled products
-- What does IS_COMPETITOR mean?
create
or replace TABLE SCD.SCD_IN_PRODUCT_CONFIG (
    SCD_IN_PRODUCT_CONFIG_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_PRODUCT_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    PRODUCT_NAME VARCHAR(255),
    PRODUCT_UID VARCHAR(20),
    PRODUCT_ID NUMBER(38, 0),
    IS_COMPETITOR BOOLEAN,
    IS_SCD_ENABLED BOOLEAN,
    IS_MARKET_CALC_CUSTOM BOOLEAN DEFAULT '0',
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    PRODUCT_ALL_PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_ALL_SOURCE_SYSTEM_NAME VARCHAR(100),
    primary key (SCD_IN_PRODUCT_CONFIG_KEY)
);

-- SCD_IN_SALES_FACT_SUMMARY captures the min/max sale dates for every product, metric for each source (BRICK/ACCOUNT or WEEKLY/MONTHLY)
create
or replace TABLE SCD.SCD_IN_SALES_FACT_SUMMARY (
    SCD_IN_SALES_FACT_SUMMARY_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_PRODUCT_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    PRODUCT_UID VARCHAR(20),
    PRODUCT_ALL_PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_ALL_SOURCE_SYSTEM_NAME VARCHAR(100),
    SCD_IN_PRODUCT_CONFIG_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    SCD_IN_REPORTING_LEVEL_KEY NUMBER(38, 0),
    MIN_SALE_DATE DATE,
    MAX_SALE_DATE DATE,
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_SALES_FACT_SUMMARY_KEY)
);

-- SCD_IN_PRODUCT_METRIC_SUMMARY captures the products and metrics that are useable. 
-- If DIM_MARKETBASKET_KEY is null, the useability flag is for Sales-change and Ordergap
-- Otherwise, the useability flag is for that market-basket for Market-basket and market-share use-cases.
-- Even though REPORTING_LEVEL_KEY and DIM_FREQUENCY_KEY are not displayed in the product-metric matrix, 
-- we need to store them as well to filter the dropdowns in the UI correctly
create
or replace TABLE SCD.SCD_IN_PRODUCT_METRIC_SUMMARY (
    SCD_IN_PRODUCT_METRIC_SUMMARY_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_IN_PRODUCT_CONFIG_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    SCD_IN_REPORTING_LEVEL_KEY NUMBER(38, 0),
    DIM_MARKETBASKET_KEY NUMBER(38, 0),
    IS_USEABLE BOOLEAN,
    REASON_TEXT VARCHAR(255),
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_PRODUCT_METRIC_SUMMARY_KEY)
);

-- SCD configuration table to setup parameters differently by client.
create
or replace TABLE SCD.SCD_IN_PARAM (
    SCD_IN_PARAM_KEY NUMBER(38, 0) NOT NULL autoincrement,
    PARAM_NAME VARCHAR(255),
    PARAM_VALUE VARCHAR(255),
    PARAM_TYPE VARCHAR(255),
    PARAM_DESC VARCHAR(500),
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_PARAM_KEY)
);

-- Table to capture the possible reporting levels (BRICK or ACCOUNT)
create
or replace TABLE SCD.SCD_IN_REPORTING_LEVEL (
    SCD_IN_REPORTING_LEVEL_KEY NUMBER(38, 0) NOT NULL autoincrement,
    REPORTING_LEVEL_NAME VARCHAR(255),
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_REPORTING_LEVEL_KEY)
);

-- Table to capture the list of SCD use-cases (4 of them)
create
or replace TABLE SCD.SCD_IN_USE_CASE (
    SCD_IN_USE_CASE_KEY NUMBER(38, 0) NOT NULL autoincrement,
    USE_CASE_NAME VARCHAR(255),
    USE_CASE_NAME_DESC VARCHAR(1000),
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_USE_CASE_KEY)
);

-- Table to capture the SCD views to use for each SCD use-case, reporting-level, frequency, market-basket
create
or replace TABLE SCD.SCD_IN_TARGET_VIEW_MAPPING (
    SCD_IN_TARGET_VIEW_MAPPING_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_IN_USE_CASE_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    SCD_IN_REPORTING_LEVEL_KEY NUMBER(38, 0),
    TARGET_VIEW_NAME VARCHAR(500),
    IS_MARKET_CALC_CUSTOM BOOLEAN,
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_TARGET_VIEW_MAPPING_KEY)
);

create
or replace TABLE SCD.SCD_IN_REPORTING_LEVEL (
    SCD_IN_REPORTING_LEVEL_KEY NUMBER(38, 0) NOT NULL autoincrement,
    REPORTING_LEVEL_NAME VARCHAR(255),
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_IN_REPORTING_LEVEL_KEY)
);

-- Table to capture the datapoints created in the Learning UI
create
or replace TABLE SCD.SCD_OUT_DATAPOINT (
    SCD_OUT_DATAPOINT_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_IN_PRODUCT_CONFIG_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0) DEFAULT 3,
    DIM_MARKETBASKET_KEY NUMBER(38, 0),
    SCD_IN_USE_CASE_KEY NUMBER(38, 0),
    SCD_IN_REPORTING_LEVEL_KEY NUMBER(38, 0),
    PRODUCT_NAME VARCHAR(255),
    METRIC_NAME VARCHAR(100),
    FREQUENCY_NAME VARCHAR(100),
    MARKET_BASKET_NAME VARCHAR(255),
    PERIOD_NUMBER NUMBER(38, 0),
    USE_CASE_NAME VARCHAR(255),
    REPORTING_LEVEL_NAME VARCHAR(255),
    DATAPOINT_NAME VARCHAR(255),
    IS_USEABLE BOOLEAN DEFAULT TRUE,
    REASON_TEXT VARCHAR(255),
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_OUT_DATAPOINT_KEY)
);

/*
ALTER TABLE SCD.SCD_OUT_DATAPOINT ADD COLUMN IS_USEABLE BOOLEAN DEFAULT TRUE;
ALTER TABLE SCD.SCD_OUT_DATAPOINT ADD COLUMN REASON_TEXT VARCHAR(255);
*/

-- Table to capture the output of the SCD detection models
create
or replace TABLE SCD.SCD_POST_PROCESS_OUTPUT (
    SCD_POST_PROCESS_OUTPUT_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_NAME VARCHAR(255),
    REPORTING_LEVEL_NAME VARCHAR(50),
    REPORTING_LEVEL_VALUE VARCHAR(25),
    PRODUCT_NAME VARCHAR(100),
    CURRENT_WEEK_DATE DATE,
    SCD_ACTUAL_VALUE NUMBER(20, 6),
    SCD_PREDICTED_VALUE NUMBER(20, 6),
    SCD_SCORE NUMBER(20, 6),
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    SCD_OUT_DATAPOINT_KEY NUMBER(38, 0),
    CHANGE_DIRECTION VARCHAR(100),
    primary key (SCD_POST_PROCESS_OUTPUT_KEY)
);

create
or replace TABLE SCD.SCD_POST_PROCESS_OUTPUT_ARCHIVE (
    SCD_POST_PROCESS_OUTPUT_ARCH_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_NAME VARCHAR(255),
    REPORTING_LEVEL_NAME VARCHAR(50),
    REPORTING_LEVEL_VALUE VARCHAR(25),
    PRODUCT_NAME VARCHAR(100),
    CURRENT_WEEK_DATE DATE,
    SCD_ACTUAL_VALUE NUMBER(20, 6),
    SCD_PREDICTED_VALUE NUMBER(20, 6),
    SCD_SCORE NUMBER(38, 0),
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    SCD_OUT_DATAPOINT_KEY NUMBER(38, 0),
    CHANGE_DIRECTION VARCHAR(100),
    primary key (SCD_POST_PROCESS_OUTPUT_ARCH_KEY)
);

create
or replace TABLE SCD.SCD_POST_PROCESS_BI_METRIC_OUTPUT (
    SCD_POST_PROCESS_BI_METRIC_OUTPUT_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_NAME VARCHAR(255),
    REPORTING_LEVEL_NAME VARCHAR(50),
    PRODUCT_NAME VARCHAR(100),
    CURRENT_WEEK_DATE DATE,
    ELIGIBILITY_PERCENT NUMBER(20, 6),
    ANOMALY_THRESHOLD NUMBER(20, 6),
    REPORTING_LEVEL_COUNT NUMBER(38, 0),
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    SCD_OUT_DATAPOINT_KEY NUMBER(38, 0),
    CHANGE_DIRECTION VARCHAR(100),
    primary key (SCD_POST_PROCESS_BI_METRIC_OUTPUT_KEY)
);

create
or replace TABLE SCD.SCD_POST_PROCESS_BI_METRIC_OUTPUT_ARCHIVE (
    SCD_POST_PROCESS_BI_METRIC_OUTPUT_ARCH_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_NAME VARCHAR(255),
    REPORTING_LEVEL_NAME VARCHAR(50),
    PRODUCT_NAME VARCHAR(100),
    CURRENT_WEEK_DATE DATE,
    ELIGIBILITY_PERCENT NUMBER(20, 6),
    ANOMALY_THRESHOLD NUMBER(20, 6),
    REPORTING_LEVEL_COUNT NUMBER(38, 0),
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    SCD_OUT_DATAPOINT_KEY NUMBER(38, 0),
    CHANGE_DIRECTION VARCHAR(100),
    primary key (SCD_POST_PROCESS_BI_METRIC_OUTPUT_ARCH_KEY)
);

-- View to see the list of products in each market-basket-definition
-- TODO: Rename DIM_ tables and primary keys after new tables are available
create
or replace view SCD.vw_MARKET_BASKET_PRODUCT AS (
    select
        bskt.DIM_MARKETBASKET_KEY,
        bskt.MARKETBASKET_NAME,
        COALESCE(
            sp1.SCD_IN_PRODUCT_CONFIG_KEY,
            sp2.SCD_IN_PRODUCT_CONFIG_KEY
        ) SCD_IN_PRODUCT_CONFIG_KEY,
        COALESCE(sp1.PRODUCT_NAME, sp2.PRODUCT_NAME) PRODUCT_NAME,
        p.PRODUCT_UID,
        sp1.PRODUCT_ALL_PRODUCT_IDENTIFIER,
        sp1.PRODUCT_ALL_SOURCE_SYSTEM_NAME
    from
        DW_CENTRAL.DIM_PRODUCT_MARKET_MAPPING map
        INNER JOIN DW_CENTRAL.DIM_MARKETBASKET bskt ON map.DIM_MARKETBASKET_KEY = bskt.DIM_MARKETBASKET_KEY
        LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL pa ON map.DIM_PRODUCT_ALL_KEY = pa.DIM_PRODUCT_ALL_KEY
        LEFT JOIN SCD.SCD_IN_PRODUCT_CONFIG sp1 on sp1.PRODUCT_ALL_PRODUCT_IDENTIFIER = pa.PRODUCT_NAME
        and sp1.PRODUCT_ALL_SOURCE_SYSTEM_NAME = pa.SOURCE_SYSTEM_NAME
        LEFT JOIN DW_CENTRAL.DIM_PRODUCT p ON map.DIM_PRODUCT_KEY = p.DIM_PRODUCT_KEY
        LEFT JOIN SCD.SCD_IN_PRODUCT_CONFIG sp2 on sp2.PRODUCT_UID = p.PRODUCT_UID
);

--For each product, shows the mkt-baskets that it is part of and the list of other-products in each of those baskets
create
or replace view SCD.vw_OTHER_BASKET_PRODUCTS AS (
    select
        p.SCD_IN_PRODUCT_CONFIG_KEY,
        p.PRODUCT_NAME,
        bskt.DIM_MARKETBASKET_KEY,
        bskt.MARKETBASKET_NAME,
        other_products.PRODUCT_NAME OTHER_PRODUCT_NAME,
        other_product_config.SCD_IN_PRODUCT_CONFIG_KEY OTHER_SCD_IN_PRODUCT_CONFIG_KEY
    from
        SCD.SCD_IN_PRODUCT_CONFIG p,
        SCD.vw_MARKET_BASKET_PRODUCT bskt,
        SCD.vw_MARKET_BASKET_PRODUCT other_products,
        SCD.SCD_IN_PRODUCT_CONFIG other_product_config
    where
        p.SCD_IN_PRODUCT_CONFIG_KEY = bskt.SCD_IN_PRODUCT_CONFIG_KEY
        and bskt.DIM_MARKETBASKET_KEY = other_products.DIM_MARKETBASKET_KEY
        and other_products.SCD_IN_PRODUCT_CONFIG_KEY = other_product_config.SCD_IN_PRODUCT_CONFIG_KEY --and p.PRODUCT_NAME != other_products.PRODUCT_NAME
);

-- For each sales-dimension, shows whether it is useable or not (DIM_MARKETBASKET_KEY is null)
-- For products that are part of market-baskets, it shows whether the market-basket is useable or not (DIM_MARKETBASKET_KEY is not null)
-- TODO: remove the +1 after talking to Amol about why there is mismatch in DIM_FREQUENCY_KEY
create
or replace view SCD.vw_SCD_IN_USEABLE_SALES_DIM AS (
    select
        p.SCD_IN_PRODUCT_CONFIG_KEY,
        p.PRODUCT_NAME,
        m.DIM_METRIC_KEY,
        m.METRIC_NAME,
        f.DIM_FREQUENCY_KEY,
        f.FREQUENCY_NAME,
        r.SCD_IN_REPORTING_LEVEL_KEY,
        r.REPORTING_LEVEL_NAME,
        mb.DIM_MARKETBASKET_KEY,
        mb.MARKETBASKET_NAME,
        MIN(pr.PERIOD_NUMBER) MIN_PERIOD_NUMBER,
        MAX(pr.PERIOD_NUMBER) MAX_PERIOD_NUMBER,
        IS_USEABLE,
        REASON_TEXT
    from
        SCD.SCD_IN_PRODUCT_METRIC_SUMMARY pm
        INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG p on pm.SCD_IN_PRODUCT_CONFIG_KEY = p.SCD_IN_PRODUCT_CONFIG_KEY
        INNER JOIN DW_CENTRAL.DIM_METRIC m on pm.DIM_METRIC_KEY = m.DIM_METRIC_KEY
        INNER JOIN DW_CENTRAL.DIM_FREQUENCY f on pm.DIM_FREQUENCY_KEY = f.DIM_FREQUENCY_KEY
        INNER JOIN DW_CENTRAL.DIM_PERIOD pr on pm.DIM_FREQUENCY_KEY = pr.DIM_FREQUENCY_KEY
        INNER JOIN SCD.SCD_IN_REPORTING_LEVEL r on pm.SCD_IN_REPORTING_LEVEL_KEY = r.SCD_IN_REPORTING_LEVEL_KEY
        LEFT JOIN DW_CENTRAL.DIM_MARKETBASKET mb on pm.DIM_MARKETBASKET_KEY = mb.DIM_MARKETBASKET_KEY
    group by
        p.SCD_IN_PRODUCT_CONFIG_KEY,
        p.PRODUCT_NAME,
        m.DIM_METRIC_KEY,
        m.METRIC_NAME,
        f.DIM_FREQUENCY_KEY,
        f.FREQUENCY_NAME,
        r.SCD_IN_REPORTING_LEVEL_KEY,
        r.REPORTING_LEVEL_NAME,
        mb.DIM_MARKETBASKET_KEY,
        mb.MARKETBASKET_NAME,
        IS_USEABLE,
        REASON_TEXT
);

-- For each product-metric, shows whether it is useable or not 
--(for sales-volume/order-gap detection when IS_MARKET is null, and for mkt-basket/mkt-share detection when IS_MARKET = 1)
create
or replace view SCD.vw_SCD_IN_USEABLE_PRODUCT_METRIC AS (
    SELECT
        P.SCD_IN_PRODUCT_CONFIG_KEY,
        P.PRODUCT_NAME,
        P.IS_SCD_ENABLED,
        P.IS_MARKET_CALC_CUSTOM,
        P.IS_COMPETITOR,
        M.DIM_METRIC_KEY,
        M.METRIC_NAME,
        CASE
            WHEN PR.PERIOD_NUMBER = 1 THEN 0
            ELSE 1
        END FOR_MARKET,
        COALESCE(pm.IS_USEABLE, FALSE) IS_USEABLE,
        COALESCE(pm.REASON_TEXT, 'No data available') REASON_TEXT
    FROM
        SCD.SCD_IN_PRODUCT_CONFIG P
        CROSS JOIN DW_CENTRAL.DIM_METRIC M -- explode for every metric
        CROSS JOIN DW_CENTRAL.DIM_PERIOD PR -- to explode to 2 rows (one for product usability and 2nd for mkt-basket usability)
        LEFT JOIN (
            select
                SCD_IN_PRODUCT_CONFIG_KEY,
                PRODUCT_NAME,
                DIM_METRIC_KEY,
                METRIC_NAME,
                CASE
                    WHEN DIM_MARKETBASKET_key IS null THEN 1
                    ELSE 2
                END FOR_MARKET,
                SUM(
                    CASE
                        WHEN IS_USEABLE = TRUE THEN 1
                        ELSE 0
                    END
                ) > 0 AS IS_USEABLE,
                LISTAGG(REASON_TEXT, ',<b>') AS REASON_TEXT
            from
                SCD.vw_SCD_IN_USEABLE_SALES_DIM
            group by
                DIM_METRIC_KEY,
                PRODUCT_NAME,
                SCD_IN_PRODUCT_CONFIG_KEY,
                METRIC_NAME,
                CASE
                    WHEN DIM_MARKETBASKET_key IS null THEN 1
                    ELSE 2
                END
        ) pm on P.SCD_IN_PRODUCT_CONFIG_KEY = pm.SCD_IN_PRODUCT_CONFIG_KEY
        and M.DIM_METRIC_KEY = pm.DIM_METRIC_KEY
        and PR.PERIOD_NUMBER = pm.FOR_MARKET
    WHERE
        PR.PERIOD_NUMBER BETWEEN 1 AND 2
        and P.IS_DELETED = FALSE
        --and P.IS_COMPETITOR = FALSE
);

--
-- Map Brick/HCO to Account level data for post process output
-- 
create
or replace view SCD.vw_SCD_POST_PROCESS_OUTPUT_ACCOUNT AS (
    (
        select
            SCD_POST_PROCESS_OUTPUT_KEY,
            SCD_NAME,
            po.REPORTING_LEVEL_NAME,
            po.REPORTING_LEVEL_VALUE,
            cast(m.DIM_BRICK_KEY as integer) REPORTING_KEY,
            cast(m.DIM_ACCOUNT_KEY as integer) ACCOUNT_KEY,
            m.ACCOUNT_UID ACCOUNT_UID,
            pc.PRODUCT_NAME,
            pc.PRODUCT_UID,
            CURRENT_WEEK_DATE,
            SCD_ACTUAL_VALUE,
            SCD_PREDICTED_VALUE,
            SCD_SCORE,
            CHANGE_DIRECTION,
            po.SCD_OUT_DATAPOINT_KEY,
            po.CREATED_TS,
            po.UPDATED_TS
        from
            SCD.SCD_POST_PROCESS_OUTPUT po
            INNER JOIN DW_CENTRAL.F_BRICK_ACCOUNT_MAPPING m on po.reporting_level_value = m.brick_hco_code
            INNER JOIN SCD.SCD_OUT_DATAPOINT d on d.SCD_OUT_DATAPOINT_KEY = po.SCD_OUT_DATAPOINT_KEY
            INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG pc on d.SCD_IN_PRODUCT_CONFIG_KEY = pc.SCD_IN_PRODUCT_CONFIG_KEY
        where
            po.REPORTING_LEVEL_NAME = 'BRICK'
            and m.DW_DELETED_FLAG = 'FALSE'
    )
    UNION
    (
        select
            SCD_POST_PROCESS_OUTPUT_KEY,
            SCD_NAME,
            po.REPORTING_LEVEL_NAME,
            po.REPORTING_LEVEL_VALUE,
            cast(m.HCO_ACCOUNT_KEY as integer) REPORTING_KEY,
            cast(m.HCP_ACCOUNT_KEY as integer) ACCOUNT_KEY,
            m.HCP_ACCOUNT_UID ACCOUNT_UID,
            pc.PRODUCT_NAME,
            pc.PRODUCT_UID,
            CURRENT_WEEK_DATE,
            SCD_ACTUAL_VALUE,
            SCD_PREDICTED_VALUE,
            SCD_SCORE,
            CHANGE_DIRECTION,
            po.SCD_OUT_DATAPOINT_KEY,
            po.CREATED_TS,
            po.UPDATED_TS
        from
            SCD.SCD_POST_PROCESS_OUTPUT po
            INNER JOIN DW_CENTRAL.F_HCO_HCP_MAPPING m on po.reporting_level_value = m.hco_account_key
            INNER JOIN SCD.SCD_OUT_DATAPOINT d on d.SCD_OUT_DATAPOINT_KEY = po.SCD_OUT_DATAPOINT_KEY
            INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG pc on d.SCD_IN_PRODUCT_CONFIG_KEY = pc.SCD_IN_PRODUCT_CONFIG_KEY
        where
            po.REPORTING_LEVEL_NAME = 'HCO'
            and m.DW_DELETED_FLAG = 'FALSE'
    )
    UNION
    (
        select
            SCD_POST_PROCESS_OUTPUT_KEY,
            SCD_NAME,
            po.REPORTING_LEVEL_NAME,
            po.REPORTING_LEVEL_VALUE,
            cast(po.REPORTING_LEVEL_VALUE as integer) REPORTING_KEY,
            cast(po.REPORTING_LEVEL_VALUE as integer) ACCOUNT_KEY,
            a.ACCOUNT_UID ACCOUNT_UID,
            pc.PRODUCT_NAME,
            pc.PRODUCT_UID,
            CURRENT_WEEK_DATE,
            SCD_ACTUAL_VALUE,
            SCD_PREDICTED_VALUE,
            SCD_SCORE,
            CHANGE_DIRECTION,
            po.SCD_OUT_DATAPOINT_KEY,
            po.CREATED_TS,
            po.UPDATED_TS
        from
            SCD.SCD_POST_PROCESS_OUTPUT po
            INNER JOIN DW_CENTRAL.DIM_ACCOUNT a on cast(reporting_level_value as integer) = a.ACCOUNT_ID
            INNER JOIN SCD.SCD_OUT_DATAPOINT d on d.SCD_OUT_DATAPOINT_KEY = po.SCD_OUT_DATAPOINT_KEY
            INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG pc on d.SCD_IN_PRODUCT_CONFIG_KEY = pc.SCD_IN_PRODUCT_CONFIG_KEY
        where
            po.REPORTING_LEVEL_NAME = 'ACCOUNT'
    )
);

create
or replace TABLE SCD.SCD_BI_CONTROL_RANGE (
    SCD_BI_CONTROL_RANGE_KEY NUMBER(38, 0) NOT NULL autoincrement,
    SCD_BI_CONTROL_RANGE_NAME VARCHAR(255),
    SCD_IN_PRODUCT_CONFIG_KEY NUMBER(38, 0),
    RANGE_MIN NUMBER(20, 6),
    RANGE_MAX NUMBER(20, 6),
    IS_DELETED BOOLEAN DEFAULT '0',
    CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    UPDATED_TS TIMESTAMP_NTZ(9),
    primary key (SCD_BI_CONTROL_RANGE_KEY)
);

/* 
 SQL to generate the table and view definitions for SCD rollup tables/views

select get_ddl('TABLE', 'SCD.F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP');
select get_ddl('TABLE', 'SCD.F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP');
select get_ddl('TABLE', 'SCD.F_ACCOUNT_WEEKLY_ROLLUP');
select get_ddl('TABLE', 'SCD.F_BRICK_WEEKLY_ROLLUP');

select get_ddl('VIEW', 'SCD.VW_F_ACCOUNT_WEEKLY_ROLLUP');
select get_ddl('VIEW', 'SCD.VW_F_BRICK_WEEKLY_ROLLUP');
select get_ddl('VIEW', 'SCD.VW_F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP');
select get_ddl('VIEW', 'SCD.VW_F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP');

select get_ddl('VIEW', 'SCD.VW_ACCOUNT_WEEKLY_MARKETBASKET_VOLUME');
select get_ddl('VIEW', 'SCD.VW_ACCOUNT_WEEKLY_MARKET_SHARE');
select get_ddl('VIEW', 'SCD.VW_ACCOUNT_WEEKLY_ORDER_GAP_DETECTION');
select get_ddl('VIEW', 'SCD.VW_ACCOUNT_WEEKLY_SALES_VOLUME');

select get_ddl('VIEW', 'SCD.VW_BRICK_WEEKLY_MARKETBASKET_VOLUME');
select get_ddl('VIEW', 'SCD.VW_BRICK_WEEKLY_MARKET_SHARE');
select get_ddl('VIEW', 'SCD.VW_BRICK_WEEKLY_ORDER_GAP_DETECTION');
select get_ddl('VIEW', 'SCD.VW_BRICK_WEEKLY_SALES_VOLUME');

*/

-- Keep the following commented out until the insert statements are in the preprocessor

create or replace TABLE F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP (
	F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP_KEY NUMBER(38,0) NOT NULL autoincrement,
	DIM_BRICK_KEY NUMBER(38,0),
	DIM_BRAND_KEY NUMBER(38,0),
	DIM_PRODUCT_ALL_KEY NUMBER(38,0),
	DIM_PRODUCT_EXTENSION_KEY NUMBER(38,0),
	DIM_CURRENCY_KEY NUMBER(38,0),
	DIM_METRIC_KEY NUMBER(38,0),
	DIM_FREQUENCY_KEY NUMBER(38,0),
	DIM_MARKETBASKET_KEY NUMBER(38,0),
	BRICK_HCO_CODE VARCHAR(100),
	BRAND_NAME VARCHAR(100),
	PRODUCT_NAME VARCHAR(100),
	PRODUCT_NAME_ENGLISH VARCHAR(100),
	PRODUCT_IDENTIFIER VARCHAR(100),
	PRODUCT_UID VARCHAR(40),
	PRODUCT_STRENGTH VARCHAR(100),
	PRODUCT_PACKAGE VARCHAR(100),
	CURRENCY_CODE VARCHAR(5),
	METRIC_NAME VARCHAR(100),
	FREQUENCY_NAME VARCHAR(100),
	MARKETBASKET_NAME VARCHAR(255),
	MARKET_METRIC_AS_OF_DATE DATE,
	MARKET_METRIC_AS_OF_YEAR NUMBER(38,0),
	WEEK_NUMBER NUMBER(38,0),
	MARKET_SHARE_VALUE NUMBER(20,6),
	MARKET_VOLUME NUMBER(20,6),
	DW_CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
	primary key (F_BRICK_WEEKLY_MARKETDATA_CUSTOM_ROLLUP_KEY)
);

create
or replace TABLE F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP (
    F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_EXTERNAL_ACCOUNT_KEY NUMBER(38, 0),
    DIM_BRAND_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    DIM_PRODUCT_EXTENSION_KEY NUMBER(38, 0),
    DIM_CURRENCY_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    ACCOUNT_NAME VARCHAR(255),
    ACCOUNT_ID NUMBER(38, 0),
    ACCOUNT_UID VARCHAR(40),
    SALES_REFERENCE_ACCOUNT_ID VARCHAR(255),
    BRAND_NAME VARCHAR(100),
    PRODUCT_NAME VARCHAR(100),
    PRODUCT_NAME_ENGLISH VARCHAR(100),
    PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_UID VARCHAR(40),
    PRODUCT_STRENGTH VARCHAR(100),
    PRODUCT_PACKAGE VARCHAR(100),
    CURRENCY_CODE VARCHAR(5),
    METRIC_NAME VARCHAR(100),
    FREQUENCY_NAME VARCHAR(100),
    SALE_DATE DATE,
    SALE_YEAR NUMBER(38, 0),
    WEEK_NUMBER NUMBER(38, 0),
    QUANTITY NUMBER(20, 6),
    NET_SALE_AMOUNT NUMBER(20, 6),
    DW_CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    primary key (F_ACCOUNT_WEEKLY_MARKETDATA_CUSTOM_ROLLUP_KEY)
);

create
or replace TABLE F_ACCOUNT_WEEKLY_ROLLUP (
    F_ACCOUNT_WEEKLY_ROLLUP_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_EXTERNAL_ACCOUNT_KEY NUMBER(38, 0),
    DIM_BRAND_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    DIM_PRODUCT_EXTENSION_KEY NUMBER(38, 0),
    DIM_CURRENCY_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    ACCOUNT_NAME VARCHAR(255),
    ACCOUNT_ID NUMBER(38, 0),
    ACCOUNT_UID VARCHAR(40),
    SALES_REFERENCE_ACCOUNT_ID VARCHAR(255),
    BRAND_NAME VARCHAR(100),
    PRODUCT_NAME VARCHAR(100),
    PRODUCT_NAME_ENGLISH VARCHAR(100),
    PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_UID VARCHAR(40),
    PRODUCT_STRENGTH VARCHAR(100),
    PRODUCT_PACKAGE VARCHAR(100),
    CURRENCY_CODE VARCHAR(5),
    METRIC_NAME VARCHAR(100),
    FREQUENCY_NAME VARCHAR(100),
    SALE_DATE DATE,
    SALE_YEAR NUMBER(38, 0),
    WEEK_NUMBER NUMBER(38, 0),
    QUANTITY NUMBER(20, 6),
    NET_SALE_AMOUNT NUMBER(20, 6),
    AKT_REFERENCE_ID VARCHAR(100),
    DW_CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    primary key (F_ACCOUNT_WEEKLY_ROLLUP_KEY)
);

create
or replace TABLE F_BRICK_WEEKLY_ROLLUP (
    F_BRICK_WEEKLY_ROLLUP_KEY NUMBER(38, 0) NOT NULL autoincrement,
    DIM_BRICK_KEY NUMBER(38, 0),
    DIM_BRAND_KEY NUMBER(38, 0),
    DIM_PRODUCT_ALL_KEY NUMBER(38, 0),
    DIM_PRODUCT_EXTENSION_KEY NUMBER(38, 0),
    DIM_CURRENCY_KEY NUMBER(38, 0),
    DIM_METRIC_KEY NUMBER(38, 0),
    DIM_FREQUENCY_KEY NUMBER(38, 0),
    BRICK_HCO_CODE VARCHAR(100),
    BRAND_NAME VARCHAR(100),
    PRODUCT_NAME VARCHAR(100),
    PRODUCT_NAME_ENGLISH VARCHAR(100),
    PRODUCT_IDENTIFIER VARCHAR(100),
    PRODUCT_UID VARCHAR(40),
    PRODUCT_STRENGTH VARCHAR(100),
    PRODUCT_PACKAGE VARCHAR(100),
    CURRENCY_CODE VARCHAR(5),
    METRIC_NAME VARCHAR(100),
    FREQUENCY_NAME VARCHAR(100),
    SALE_DATE DATE,
    SALE_YEAR NUMBER(38, 0),
    WEEK_NUMBER NUMBER(38, 0),
    QUANTITY NUMBER(20, 6),
    NET_SALE_AMOUNT NUMBER(20, 6),
    AKT_REFERENCE_ID VARCHAR(100),
    DW_CREATED_TS TIMESTAMP_NTZ(9) DEFAULT CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9)),
    primary key (F_BRICK_WEEKLY_ROLLUP_KEY)
);

--
-- Map Brick/HCO to Account level data for post process output
-- This view uses archive output table, and only select rows with SCD_SCORE > 0
-- 
create
or replace view SCD.vw_SCD_POST_PROCESS_OUTPUT_ACCOUNT_ARCHIVE AS (
    (select SCD_POST_PROCESS_OUTPUT_ARCH_KEY, 
           SCD_NAME, 
           uc.USE_CASE_NAME,
           po.REPORTING_LEVEL_NAME, 
           po.REPORTING_LEVEL_VALUE, 
           cast(m.DIM_BRICK_KEY as integer)  REPORTING_KEY, 
           cast(m.DIM_ACCOUNT_KEY as integer) ACCOUNT_KEY,
           m.ACCOUNT_UID ACCOUNT_UID, 
           pc.PRODUCT_NAME,
           pc.PRODUCT_UID, 
           CURRENT_WEEK_DATE, 
           SCD_ACTUAL_VALUE, 
           SCD_PREDICTED_VALUE, 
           SCD_SCORE,
           CHANGE_DIRECTION,
           po.SCD_OUT_DATAPOINT_KEY,
           po.CREATED_TS, 
           po.UPDATED_TS
    from
        SCD.SCD_POST_PROCESS_OUTPUT_ARCHIVE po
        INNER JOIN DW_CENTRAL.F_BRICK_ACCOUNT_MAPPING m on po.reporting_level_value = m.brick_hco_code
        INNER JOIN SCD.SCD_OUT_DATAPOINT d on d.SCD_OUT_DATAPOINT_KEY = po.SCD_OUT_DATAPOINT_KEY
        INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG pc on d.SCD_IN_PRODUCT_CONFIG_KEY = pc.SCD_IN_PRODUCT_CONFIG_KEY
        INNER JOIN SCD.SCD_IN_USE_CASE uc on uc.SCD_IN_USE_CASE_KEY = d.SCD_IN_USE_CASE_KEY
    where po.REPORTING_LEVEL_NAME = 'BRICK' and m.DW_DELETED_FLAG = 'FALSE' and SCD_SCORE > 0)
  UNION
    (select SCD_POST_PROCESS_OUTPUT_ARCH_KEY, 
           SCD_NAME, 
           uc.USE_CASE_NAME,
           po.REPORTING_LEVEL_NAME, 
           po.REPORTING_LEVEL_VALUE, 
           cast(m.HCO_ACCOUNT_KEY as integer) REPORTING_KEY, 
           cast(m.HCP_ACCOUNT_KEY as integer) ACCOUNT_KEY,
           m.HCP_ACCOUNT_UID  ACCOUNT_UID, 
           pc.PRODUCT_NAME, 
           pc.PRODUCT_UID,
           CURRENT_WEEK_DATE, 
           SCD_ACTUAL_VALUE, 
           SCD_PREDICTED_VALUE, 
           SCD_SCORE, 
           CHANGE_DIRECTION,
           po.SCD_OUT_DATAPOINT_KEY,
           po.CREATED_TS, 
           po.UPDATED_TS
    from
        SCD.SCD_POST_PROCESS_OUTPUT_ARCHIVE po
        INNER JOIN DW_CENTRAL.F_HCO_HCP_MAPPING m on po.reporting_level_value = m.hco_account_key
        INNER JOIN SCD.SCD_OUT_DATAPOINT d on d.SCD_OUT_DATAPOINT_KEY = po.SCD_OUT_DATAPOINT_KEY
        INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG pc on d.SCD_IN_PRODUCT_CONFIG_KEY = pc.SCD_IN_PRODUCT_CONFIG_KEY
        INNER JOIN SCD.SCD_IN_USE_CASE uc on uc.SCD_IN_USE_CASE_KEY = d.SCD_IN_USE_CASE_KEY
    where po.REPORTING_LEVEL_NAME = 'HCO' and m.DW_DELETED_FLAG = 'FALSE' and SCD_SCORE > 0)  
  UNION
    (select SCD_POST_PROCESS_OUTPUT_ARCH_KEY, 
           SCD_NAME, 
           uc.USE_CASE_NAME,
           po.REPORTING_LEVEL_NAME, 
           po.REPORTING_LEVEL_VALUE, 
           cast(po.REPORTING_LEVEL_VALUE as integer) REPORTING_KEY, 
           cast(po.REPORTING_LEVEL_VALUE as integer) ACCOUNT_KEY,
           a.ACCOUNT_UID ACCOUNT_UID, 
           pc.PRODUCT_NAME, 
           pc.PRODUCT_UID,
           CURRENT_WEEK_DATE, 
           SCD_ACTUAL_VALUE, 
           SCD_PREDICTED_VALUE, 
           SCD_SCORE, 
           CHANGE_DIRECTION,
           po.SCD_OUT_DATAPOINT_KEY,
           po.CREATED_TS, 
           po.UPDATED_TS
    from
        SCD.SCD_POST_PROCESS_OUTPUT_ARCHIVE po
        INNER JOIN DW_CENTRAL.DIM_ACCOUNT a on cast(reporting_level_value as integer) = a.ACCOUNT_ID
        INNER JOIN SCD.SCD_OUT_DATAPOINT d on d.SCD_OUT_DATAPOINT_KEY = po.SCD_OUT_DATAPOINT_KEY
        INNER JOIN SCD.SCD_IN_PRODUCT_CONFIG pc on d.SCD_IN_PRODUCT_CONFIG_KEY = pc.SCD_IN_PRODUCT_CONFIG_KEY
        INNER JOIN SCD.SCD_IN_USE_CASE uc on uc.SCD_IN_USE_CASE_KEY = d.SCD_IN_USE_CASE_KEY
    where po.REPORTING_LEVEL_NAME = 'ACCOUNT' and SCD_SCORE > 0)
);    

---
--- Create view to count accounts by product/use-case/sale-date-week
---
create
or replace view SCD.vw_SCD_TRIGGERED AS (
    (select count(distinct ACCOUNT_UID) as "TOTAL NUMBER OF ACCOUNTS", 
           PRODUCT_NAME, 
           USE_CASE_NAME, 
           SCD_NAME,
           CURRENT_WEEK_DATE
    from
        SCD.vw_SCD_POST_PROCESS_OUTPUT_ACCOUNT_ARCHIVE
    group by grouping sets((CURRENT_WEEK_DATE, PRODUCT_NAME), (CURRENT_WEEK_DATE, SCD_NAME), (CURRENT_WEEK_DATE, USE_CASE_NAME), (CURRENT_WEEK_DATE)))
);
  
  
--  
-- Create view 
--
or replace view SCD.vw_MARKET_BASKET_PRODUCT AS (
    select
        bskt.DIM_MARKETBASKET_KEY,
        bskt.MARKETBASKET_NAME,
        COALESCE(sp1.SCD_IN_PRODUCT_CONFIG_KEY, sp2.SCD_IN_PRODUCT_CONFIG_KEY) SCD_IN_PRODUCT_CONFIG_KEY,
        COALESCE(sp1.PRODUCT_NAME, sp2.PRODUCT_NAME) PRODUCT_NAME,
        p.PRODUCT_UID,
        sp1.PRODUCT_ALL_PRODUCT_IDENTIFIER, 
        sp1.PRODUCT_ALL_SOURCE_SYSTEM_NAME 
    from
        DW_RAW.RAW_SPARKDSERUNREPDATESUGGESTIONDETAIL srdsd
        INNER JOIN DW_CENTRAL.DIM_MARKETBASKET bskt ON map.DIM_MARKETBASKET_KEY = bskt.DIM_MARKETBASKET_KEY
        LEFT JOIN DW_CENTRAL.DIM_PRODUCT_ALL pa ON map.DIM_PRODUCT_ALL_KEY = pa.DIM_PRODUCT_ALL_KEY
        LEFT JOIN SCD.SCD_IN_PRODUCT_CONFIG sp1 on sp1.PRODUCT_ALL_PRODUCT_IDENTIFIER = pa.PRODUCT_NAME and sp1.PRODUCT_ALL_SOURCE_SYSTEM_NAME = pa.SOURCE_SYSTEM_NAME
        LEFT JOIN DW_CENTRAL.DIM_PRODUCT p ON map.DIM_PRODUCT_KEY = p.DIM_PRODUCT_KEY
        LEFT JOIN SCD.SCD_IN_PRODUCT_CONFIG sp2 on sp2.PRODUCT_UID = p.PRODUCT_UID
);

---
--- Create a view of suggestions and flattened configSpecJson
---
create or replace view SCD.vw_SCD_LABEL_SUGG as (
    with sugg as
       ( select cff.productUID, srs.accountUID, srs.factorUID, sl.suggestionReferenceId, cff.factorName as factorNameId, 
                isSuggestionCompleted, isSuggestionDismissed, isSuggestionActioned, isSuggestionCompletedInfer,
                r.seConfigId, sl.publishedAt as suggestionDate, sl.reportedInteractionUID as interactionUID, configspecJson
        from DW_RAW.RAW_DSERUNREPDATESUGGESTION srs 
             inner join DW_RAW.RAW_DSERUNCONFIGFACTOR cff on srs.factorUID = cff.factorUID
             inner join DW_RAW.RAW_DSERun r on r.runId = cff.runId
             inner join DW_RAW.RAW_DSEConfig cf on cf.seConfigId = r.seConfigId
             inner join DW_RAW.RAW_DSESuggestionLifecycle sl on sl.suggestionReferenceId = srs.suggestionReferenceId
        where srs.factorUID is not null
       )
    select f.value:rules:labelValueMetricTypeId as labelTypeExternalId, 
           product_name, accountUID, factorUID, suggestionReferenceId, factorNameId, 
           factorType, runConfigFactorId, isSuggestionCompleted, isSuggestionDismissed, isSuggestionActioned, isSuggestionCompletedInfer,
           seConfigId, suggestionDate, interactionUID,
           case when labelTypeExternalId like 'ACCOUNT_SALES%' or labelTypeExternalId like 'BRICK_SALES%' or
                     labelTypeExternalId like 'ACCOUNT_MARKET%' or labelTypeExternalId like 'BRICK_MARKET%' or
                     labelTypeExternalId like 'ACCOUNT_ORDER_GAP%' or labelTypeExternalId like 'BRICK_ORDER_GAP%' or
                then 1 else 0 
                end as is_scd_sugg
    from sugg s, 
         lateral flatten(input => parse_json(configSpecJson), path => 'factorList') f,
         DW_CENTRAL.DIM_PRODUCT p
    where s.productUID = p.product_UID 
)

---
--- count number of accounts per product_name/factor/suggestion/suggestion-date-week/factor-with-tag/product/action-status/
---
create or replace view SCD.vw_SCD_PUB_SUGG as (
    select count(distinct accountUID) as "TOTAL NUMBER OF ACCOUNTS", 
           Product_Name, factorNameId, suggestionReferenceId,
           isSuggestionCompleted, isSuggestionDismissed, 
           isSuggestionActioned, isSuggestionCompletedInfer,
           suggestionDate     
    from SCD.vw_SCD_LABEL_SUGG
    where is_scd_sugg > 0
    group by grouping sets((suggestionDate, PRODUCT_NAME), (suggestionDate, factorNameId), (suggestionDate, suggestionReferenceId), 
                           (suggestionDate, isSuggestionCompleted), (suggestionDate, isSuggestionDismissed),
                           (suggestionDate, isSuggestionActioned), (suggestionDate, isSuggestionCompletedInfer),
                           (suggestionDate))
);



--
-- Grant privileges to ml_admin_rl
--
GRANT USAGE ON SCHEMA SCD TO ROLE ML_ADMIN_RL;
GRANT SELECT ON ALL TABLES IN SCHEMA SCD TO ROLE ML_ADMIN_RL;
GRANT INSERT, UPDATE, DELETE, TRUNCATE ON ALL TABLES IN SCHEMA SCD TO ROLE ML_ADMIN_RL;
GRANT SELECT ON ALL VIEWS IN SCHEMA SCD TO ROLE ML_ADMIN_RL;

--GRANT USAGE ON SCHEMA SCD TO ROLE READ_ONLY_RL;
--GRANT SELECT ON ALL TABLES IN SCHEMA SCD TO ROLE READ_ONLY_RL;
--GRANT SELECT ON ALL VIEWS IN SCHEMA SCD TO ROLE READ_ONLY_RL;
