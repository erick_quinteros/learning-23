## SCD 2.0 Build using Snowflake 
## Customer: Novartis AUS
## written by: Amy Baer 
## task: Translate R Prototype to Python
## current date: 20 July 2020
## updated date: 24 August 2020

import snowflake.connector
import snowflake as sf
import pandas as pd
import numpy as np
import logging
from statsmodels.distributions.empirical_distribution import ECDF
from statsmodels.tsa.statespace.exponential_smoothing import ExponentialSmoothing

# define logger
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# snowflake parameters
username = 'amyb'
pwd = 'jdytg&0946'
acct = 'aktanapartner'
db = 'RPT_DWPREPROD'
schema = 'SCD'

# validate snowflake connection and version
con = sf.connector.connect(
  user=username,
  password=pwd,
  account=acct,
  database=db, 
  schema=schema
)
cur = con.cursor()
try:
    cur.execute("SELECT current_version()")
    one_row = cur.fetchone()
    print(one_row[0])
finally:
    cur.close()
con.close()

# open connection to Snowflake
con = sf.connector.connect(
  user=username,
  password=pwd,
  account=acct,
  database=db, 
  schema=schema
)
sfq = con.cursor()

### FUNCTIONS to ingest data and supporting UI parameters ###
# bring in SCD_OUT tables from snowflake with parameters selected from the UI 
def ingest_scd_tables():
    query = """SELECT a.*, 1 change_direction,
            a.scd_in_use_case_key use_case_name_key,
            a.market_basket_name marketbasket_name,
            e.dim_frequency_key frequency_key,
            a.scd_in_reporting_level_key reporting_level_key,
            e.target_view_name 
            from SCD_OUT_DATAPOINT a 
            left join SCD_IN_USE_CASE b 
            on a.scd_in_use_case_key = b.scd_in_use_case_key 
            left join SCD_IN_PRODUCT_CONFIG c 
            on a.SCD_IN_PRODUCT_CONFIG_KEY=c.SCD_IN_PRODUCT_CONFIG_KEY 
            left join SCD_IN_TARGET_VIEW_MAPPING e 
            on b.scd_in_use_case_key=e.scd_in_use_case_key
            and a.dim_frequency_key = e.dim_frequency_key
            and a.scd_in_reporting_level_key = e.scd_in_reporting_level_key
            and e.is_market_calc_custom = false
            where scd_out_datapoint_key < 61
            """
    sfq.execute(query)
    data = sfq.fetchall()
    column_names = [i[0] for i in sfq.description]
    df = pd.DataFrame(data, columns = column_names)
    df.columns = map(str.lower, df.columns)
    logger.info(f'Dimensions for data ingested from %s = ({df.shape[0]}, {df.shape[1]})' % df)
    return(df)

# bring in use case specific parameters 
def get_parameters(df, freq_key, use_case_name_key, level):
    period_number = df[df['use_case_name_key']==use_case_name_key].period_number.to_list()[-1]
    metric_name = df[df['use_case_name_key']==use_case_name_key].metric_name.iloc[-1]
    product_name = df[df['use_case_name_key']==use_case_name_key].product_name.iloc[-1]
    marketbasket_name = df[df['use_case_name_key']==use_case_name_key].marketbasket_name.iloc[-1]
    change_direction = df[df['use_case_name_key']==use_case_name_key].change_direction.iloc[-1]
    target_view_name = df[(df.frequency_key==freq_key) & (df.use_case_name_key==use_case_name_key) & (df.reporting_level_key==level)].target_view_name.iloc[-1]  
    return(period_number, metric_name, product_name, marketbasket_name, change_direction, target_view_name)

# bring in views from snowflake 
def ingest_scd_views(target_view_name, period_number, metric_name, product_name):
    if target_view_name.find('GAP') != -1:
        query = f"SELECT * from {target_view_name} where metric_name = '{metric_name}' and product_name = '{product_name}'" 
        #logger.info(f'Query********= {query}')
        sfq.execute(query)
        data = sfq.fetchall()
        column_names = [i[0] for i in sfq.description]
        df = pd.DataFrame(data, columns = column_names)
        df.columns = map(str.lower, df.columns)
        logger.info(f'Dimensions for data ingested from {target_view_name} = ({df.shape[0]}, {df.shape[1]})')
        return(df)      
    else:
        query = f"SELECT * from {target_view_name} where PERIOD_NUMBER = {period_number} and metric_name = '{metric_name}' and product_name = '{product_name}'" 
        #logger.info(f'Query********= {query}')
        sfq.execute(query)
        data = sfq.fetchall()
        column_names = [i[0] for i in sfq.description]
        df = pd.DataFrame(data, columns = column_names)
        df.columns = map(str.lower, df.columns)
        logger.info(f'Dimensions for data ingested from {target_view_name} = ({df.shape[0]}, {df.shape[1]})')
        return(df)

### FUNCTIONS to prepare data for modeling ###
# find first and final purchase dates by reporting_level_name 
def first_final_purchase_date(df):

    # initialize first and final purchase as df subsets
    first_purchase_date = []
    final_purchase_date = []

    # create subsets of the data and add columns back to original data
    temp_filter = df[df[target]>0]

    first_purchase_date = pd.DataFrame(temp_filter.groupby([reporting_level_name])['sale_date'].min())
    first_purchase_date = first_purchase_date.rename(columns={'sale_date':'first_delivery'})

    final_purchase_date = pd.DataFrame(temp_filter.groupby([reporting_level_name])['sale_date'].max())
    final_purchase_date = final_purchase_date.rename(columns={'sale_date':'final_delivery'})

    output = pd.merge(df, first_purchase_date, on=reporting_level_name, how='left')
    output = pd.merge(output, final_purchase_date, on=reporting_level_name, how='left')

    return(output)

# make sure each reporting level has at least one purchase in the last year and at least six months of history
def purchase_timing_requirement(df):
    
    year_prior_date = df['sale_date'].max() - pd.to_timedelta(365, unit='d')
    
    # require at least one purchase within the last year
    df = df[df.final_delivery >= year_prior_date]

    # require at least six months total duration of purchase history per reporting_level_name
    df.insert(len(df.columns)-1, "time_flag", df.final_delivery - df.first_delivery, True) 
    df = df[df.loc[:,'time_flag'] >= '180 days']

    return(df)

# make sure each reporting level has enough purchase history for the model
def purchase_count_requirement(df, count_threshold):

    # initialize empty dataframe to hold datasubset
    nonzero_counts = []    

    # find non zero counts per reporting_level_name
    if target_view_name.find('GAP') == -1:
        nonzero_counts = df[df[target]!=0]
    else:
        nonzero_counts = df[df.sales_metric_value!=0]

    temp = pd.DataFrame(nonzero_counts.groupby([reporting_level_name], as_index=False)[target].count())
    temp = temp.rename(columns={target:'purchase_count'})

    nonzero_counts_merge = pd.merge(df, temp, on=reporting_level_name, how='left')

    # criteria 3 subset to reporting_level_name with at least the required number of counts
    counts = nonzero_counts_merge[nonzero_counts_merge['purchase_count'] >= count_threshold]

    return(counts)

### FUNCTIONS to model, score, and create data points and metrics ###
# fit a time series model to volume and share data and score for outliers
def anomaly_detect(df, alpha_up, alpha_down):
  
  # set up time series model where x is the historic vector of sales values for one reference type
  x = df[target].astype(float).tolist()
      
  min_date = df['sale_date'].min()
  series_date = pd.date_range(min_date, periods=len(x), freq='W-SAT')
  data = pd.Series(x, series_date)

  #  build model
  model = ExponentialSmoothing(data, trend=True, seasonal=4)
  results = model.fit(low_memory=True, method='powell', disp=0)

  # build prediciton data frame
  pred = pd.DataFrame(df.loc[:,[reporting_level_name,'sale_date']])
  pred['Predicted'] = np.array(results.fittedvalues)
  pred['Observed'] = x
  pred['resid'] = np.array(results.resid)
  pred['lower'] = results.resid.quantile(q=.25)
  pred['upper'] = results.resid.quantile(q=.75)
  pred['iqr'] = pred['upper'] - pred['lower']
  
  # find most recent date for output
  prob = pred.tail(1)
  
  # control minimum predictions 
  check = prob['Predicted'] < 0
  if check.bool():
      prob['Predicted'] = 0
      
  # score for Up use cases 
  check  = (prob['resid'] > prob['upper'])
  if check.bool():
      prob.loc[:,'SCD_up'] = 1 - ((alpha_up * prob['iqr'] ) / (prob['resid'] - prob['upper']))
  else:
      prob.loc[:,'SCD_up'] = 0  
  
  # score for Down use cases 
  check  = (prob['resid'] < prob['lower'])
  if check.bool():
      prob.loc[:,'SCD_down'] = 1 + ((alpha_down * prob['iqr'] ) / (prob['resid'] - prob['lower']))
  else:
      prob.loc[:,'SCD_down'] = 0 
                             
  prob = prob.loc[:,[reporting_level_name,'sale_date','Observed','Predicted','SCD_up','SCD_down']]
    
  return(prob)

# control minimum and maximum scores and predictions where appropriate
def control_scores(df):
   
    # control minimum scores
    check = df['SCD_up'] < 0
    if check.bool():
        df['SCD_up'] = 0 
  
    check = df['SCD_down'] < 0
    if check.bool():
        df['SCD_down'] = 0  
      
    # control maximum scores
    check = df['SCD_up'] >= 100
    if check.bool():
        df['SCD_up'] = 99.99
  
    check = df['SCD_down'] >= 100
    if check.bool():
        df['SCD_down'] = 99.99  
    return(df)

# create data points from scored model
def create_data_points(df):
 
    # create column for scd_name   
    up = ["SCD_",product_name.title(),"_",target_view_name.split("_", 3)[-1].title(),"_",metric_name,"_Increasing_",str(period_number),"w"]
    up = "".join(up)

    down = ["SCD_",product_name.title().lower(),"_",target_view_name.split("_", 3)[-1].title(),"_",metric_name,"_Decreasing_",str(period_number),"w"]
    down = "".join(down)

    mapping = {df.columns[4]:up, df.columns[5]:down}
    df.rename(columns=mapping, inplace=True)    

    # pull data long before appending
    score = pd.melt(df, id_vars=[reporting_level_name,'sale_date','Observed','Predicted'], 
                    value_vars=[up,down],
                    var_name='scd_name', value_name='Score')

    score.Score = round(score.Score*100,2)
    score['product_name'] = product_name
    return(score)

# model for order gap use case
def gap_detect(df):

  # initialize empty data frame to incorporate output for probability of increased gap
  prob_up = pd.DataFrame(columns=[reporting_level_name,'sale_date','Observed','Predicted','scd_name','Score','product_name'])
  
  # for Order Gap increase lastDiff is the number of weeks since the last order
  lastDiff = df[target].iloc[-1]
  
  # estimate the ecdf function fn() using order_gap_week by creating vector of purchase times
  x = df[df.sales_metric_value!=0][target][1:len(df[df.sales_metric_value!=0])]
  fn = ECDF(x)
  
  # probability of an observation in an interval in the fn() ecdf
  prob_up = pd.DataFrame(df.loc[:,[reporting_level_name,'sale_date']]).iloc[[-1]]
  prob_up['Observed'] = lastDiff
  prob_up['Predicted']= np.array(x.quantile())
  sub_string = "SCD_",product_name.title(),"_",target_view_name.split("_", 3)[-1].title(),"_",metric_name,"_Increasing_",str(period_number),"w"
  prob_up['scd_name'] = "".join(sub_string)
  prob_up['Score'] = fn(lastDiff).round(4)*100
  prob_up['product_name'] = product_name
  
  # adjust for rounding issues
  check  = (prob_up['Observed'] == prob_up['Predicted'])
  if check.bool():
      prob_up['Score'] = 0
  elif check.bool(): 
      prob_up['Score'] = prob_up['Score']
      
 # adjust for too much confidence
  check = (prob_up['Score'] == 100)
  if check.bool():
      prob_up['Score'] = 99
  elif check.bool(): 
      prob_up['Score'] = prob_up['Score']    
  
  # initialize empty data frame to incorporate output for probability of decreased gap
  prob_down = pd.DataFrame(columns=[reporting_level_name,'sale_date','Observed','Predicted','scd_name','Score','product_name'])
  
  # for Order Gap decrease lastDiff is the number of weeks between the last two orders
  last_order_gap = df[target][df.sales_metric_value!=0].iloc[-1]
   
  # Fit the data to survival model
  x = df[df.sales_metric_value!=0][target][1:len(df[df.sales_metric_value!=0])]
  
  # probability of an observation in an interval for the complementary ECDF
  prob_down = pd.DataFrame(df.loc[:,[reporting_level_name,'sale_date']]).iloc[[-1]]
  prob_down['Observed'] = last_order_gap
  prob_down['Predicted']= np.array(x.quantile())
  sub_string = "SCD_",product_name.title(),"_",target_view_name.split("_", 3)[-1].title(),"_",metric_name,"_Decreasing_",str(period_number),"w"
  prob_down['scd_name'] = "".join(sub_string)
  prob_down['Score'] = (1-fn(last_order_gap)).round(4)*100
  prob_down['product_name'] = product_name
  
  # adjust for rounding issues
  check  = (prob_down['Observed'] == prob_down['Predicted'])
  if check.bool():
      prob_down['Score'] = 0
  elif check.bool(): 
      prob_down['Score'] = prob_down['Score']
      
 # adjust for too much confidence
  check  = (prob_down['Score'] == 100)
  if check.bool():
      prob_down['Score'] = 99
  elif check.bool(): 
      prob_down['Score'] = prob_down['Score']  
  
  # combine up and down use cases
  prob = pd.concat([prob_up, prob_down])
  
  return(prob)

# create table of HCP proportions (output metrics) 
def create_metrics(df):
 
  # data for anomalies up
  x = df[df['scd_name']==df['scd_name'].unique()[0]]['Score']
  
  # table for anomalies up
  df_up = ()
  df_up = pd.DataFrame({'sale_date':df['sale_date'].unique().repeat(5),
                        'use_case':df['scd_name'].unique()[0],
                        'percent':[(x[x>=75].count() / x.count()*100).round(0),
                                   (x[x>=80].count() / x.count()*100).round(0),
                                   (x[x>=85].count() / x.count()*100).round(0),
                                   (x[x>=90].count() / x.count()*100).round(0),
                                   (x[x>=95].count() / x.count()*100).round(0)],
                        'threshold':[.75,.80,.85,.90,.95],
                        'count':len(df[reporting_level_name].unique())
                       })

  # data for anomalies down
  #if target_view_name.find('GAP') == -1:
  y = df[df['scd_name']==df['scd_name'].unique()[1]]['Score']
  
  # table for anomalies down
  df_down = ()
  df_down = pd.DataFrame({'sale_date':df['sale_date'].unique().repeat(5),
                          'use_case':df['scd_name'].unique()[1],
                          'percent':[(y[y>=75].count() / y.count()*100).round(0),
                                     (y[y>=80].count() / y.count()*100).round(0),
                                     (y[y>=85].count() / y.count()*100).round(0),
                                     (y[y>=90].count() / y.count()*100).round(0),
                                     (y[y>=95].count() / y.count()*100).round(0)],
                            'threshold':[.75,.80,.85,.90,.95],
                            'count':len(df[reporting_level_name].unique())
                          })
      # combine up and down for full use case metrics
  df =  pd.concat([df_up, df_down])
  #else :
  #    df =  df_up
 
  return(df)
   
## Bring in SCD_OUT table with required parameters ---------------------------------------------
scd_out_param = ingest_scd_tables()

## Part 1: Sales Volume Change Use Case ---------------------------------------------
# get parameters for volume use case
# freq_key 3 = weekly; use_case_name_key 1 = sales volume view; reporting_level_key 1 = brick level
### These tables are populated ONLY FOR NOVARTIS AUSTRALIA as of 07-15-2020 other clients will need these parameters manually assigned ### 
period_number, metric_name, product_name, marketbasket_name, change_direction, target_view_name = get_parameters(scd_out_param, freq_key=3, use_case_name_key=1, level=1)  

# ingest data from views after assigning parameters
vol = ingest_scd_views(target_view_name, period_number, metric_name, product_name)

# assign final parameters for use case
if target_view_name.find('BRICK') != -1:  # use brick_hco_code for BRICK
    reporting_level_name = vol.columns[0]
else:
    reporting_level_name = vol.columns[3]  # use sales_reference_account_id for ACCOUNT
target = vol.columns[-1]

# convert sale_date to date
vol['sale_date'] = pd.to_datetime(vol['sale_date'], format='%Y-%m-%d')

# call function to create columns for first and final deliveries 
vol_first_final = first_final_purchase_date(vol)

# cutoff values by reporting_level_name to ensure no values are counted in the model prior to the first purchase
vol_first_final = vol_first_final[vol_first_final['sale_date'] >= vol_first_final['first_delivery']]

# call function to subset data according to purchase timing requirements
vol_time_flag = purchase_timing_requirement(vol_first_final)

# call function to subset data according to minimum count requirement
vol_clean = purchase_count_requirement(vol_time_flag, 40)

# clean columns before model
vol_clean = vol_clean.loc[:,[reporting_level_name,'product_name','sale_date',target]]

# max_level = number of bricks_hco_codes feeding into the model
max_level = len(vol_clean[reporting_level_name].unique())  

# call the model
vol_model = vol_clean.groupby(reporting_level_name).apply(anomaly_detect, .25, .15)  # higher alpha = lower metric percentages 
logger.info(f'Dimensions for data ingested from volume model = ({vol_model.shape[0]}, {vol_model.shape[1]})')

# reset index
vol_model.reset_index(drop=True, inplace=True)

# adjust for negative scores
vol_model_control = vol_model.groupby(reporting_level_name).apply(control_scores)

# create data points for the model
vol_score  = create_data_points(vol_model_control)

# create metrics for BI output
vol_metric = create_metrics(vol_score)

## Part 2: Order Gap Detectiom Use Case ------------------------------------------------------
# pass parameters from the UI into use case
### These tables are populated ONLY FOR NOVARTIS AUSTRALIA as of 07-15-2020 other clients will need these parameters manually assigned ### 
#freq_key 3 = weekly; use_case_number 2 = gap view; reporting_level_key 1 = brick level
#period_number, metric_name, product_name, marketbasket_name, change_direction, target_view_name = get_parameters(scd_out_param, freq_key=3, use_case_name_key=2, level=1)  

period_number = 1
metric_name = 'Units' 
product_name = '**NTRE****' 
target_view_name = 'VW_BRICK_WEEKLY_ORDER_GAP_DETECTION'

# ingest data from views
gap = ingest_scd_views(target_view_name, period_number, metric_name, product_name)

# set parameters based on use case
if target_view_name.find('BRICK') != -1:  # use brick_hco_code for BRICK
    reporting_level_name = gap.columns[0]
else:
    reporting_level_name = gap.columns[3]  # use sales_reference_account_id for ACCOUNT
target = gap.columns[-1]

# convert sale_date to date
gap['sale_date'] = pd.to_datetime(gap['sale_date'], format='%Y-%m-%d')

# keep only necessary rows and columns
gap = gap.loc[ :,[reporting_level_name,'product_name','sale_date','sales_metric_value',target]]

# call function to create columns for first and final deliveries 
gap_first_final = first_final_purchase_date(gap)

# cutoff values by level to ensure no values are counted in the model prior to the first purchase
gap_first_final = gap_first_final[gap_first_final.order_gap_week.notnull()]

# call function to subset data according to purchase requirements
gap_time_flag = purchase_timing_requirement(gap_first_final)
          
# call function to subset data accirding to use case requirements
gap_clean = purchase_count_requirement(gap_time_flag, 10)
               
# capture results by reproting level 
max_level = len(gap_clean[reporting_level_name].unique()) # total number of levels for use case

# call model for order gap use cases
gap_model = gap_clean.groupby(reporting_level_name).apply(gap_detect)
logger.info(f'Dimensions for data ingested from gap model = ({gap_model.shape[0]}, {gap_model.shape[1]})')

# create metrics for BI output
gap_metric = create_metrics(gap_model)

## Part 3: Market Basket Volume Use Case ---------------------------------------------
# pass parameters from the UI into use case
### These tables are populated ONLY FOR NOVARTIS AUSTRALIA as of 07-15-2020 other clients will need these parameters manually assigned ### 
# freq_key 3 = weekly; use_case_number 3 = marketbasket view; reporting_level_key 1 = brick level
period_number, metric_name, product_name, marketbasket_name, change_direction, target_view_name = get_parameters(scd_out_param, freq_key=3, use_case_name_key=3, level=1)  

# ingest data from views
mktbskt = ingest_scd_views(target_view_name, period_number, metric_name, product_name)  

# set parameters for use case
if target_view_name.find('BRICK') != -1:  # use brick_hco_code for BRICK
    reporting_level_name = mktbskt.columns[0]
else:
    reporting_level_name = mktbskt.columns[3]  # use sales_reference_account_id for ACCOUNT
target = mktbskt.columns[-1]

# convert sale_date to date
mktbskt['sale_date'] = pd.to_datetime(mktbskt['sale_date'], format='%Y-%m-%d')

# Important for MKTBSKT and MKT_SHARE subset data to market basket of interest  
mktbskt = mktbskt[mktbskt.marketbasket_name==marketbasket_name]

# call function to create columns for first and final deliveries  
mktbskt_first_final = first_final_purchase_date(mktbskt)

# cutoff values by reporting_level_name to ensure no values are counted in the model prior to the first purchase
mktbskt_first_final = mktbskt_first_final[mktbskt_first_final['sale_date'] >= mktbskt_first_final['first_delivery']]

# call function to subset data according to purchase requirements
mktbskt_time_flag = purchase_timing_requirement(mktbskt_first_final)

# call function to subset data according to minimum count requirements
mktbskt_clean = purchase_count_requirement(mktbskt_time_flag, 40)
 
# clean columns before model
mktbskt_clean = mktbskt_clean.loc[:,[reporting_level_name,'product_name','sale_date',target]]

# max_level = number of bricks_hco_codes feeding into the model
max_level = len(mktbskt_clean[reporting_level_name].unique())  

# call the model
mktbskt_model = mktbskt_clean.groupby(reporting_level_name).apply(anomaly_detect, .25, .05)  # higher alpha = lower metric percentages 
logger.info(f'Dimensions for data ingested from market basket model = ({mktbskt_model.shape[0]}, {mktbskt_model.shape[1]})')

# reset index
mktbskt_model.reset_index(drop=True, inplace=True)

# adjust for negative scores
mktbskt_model_control = mktbskt_model.groupby(reporting_level_name).apply(control_scores)

# create data points for the model
mktbskt_score = create_data_points(mktbskt_model_control)

# create metrics for BI output
mktbskt_metric = create_metrics(mktbskt_score)

## Part 4: Market Share Use Case ----------------------------------------------------
# pass parameters from the UI into use case
### These tables are populated ONLY FOR NOVARTIS AUSTRALIA as of 07-15-2020 other clients will need these parameters manually assigned ### 
# freq_key 3 = weekly; use_case_number 4 = market share view; reporting_level_key 1 = brick level
period_number, metric_name, product_name, marketbasket_name, change_direction, target_view_name = get_parameters(scd_out_param, freq_key=3, use_case_name_key=4, level=1)  

# ingest data from views
mkt_share = ingest_scd_views(target_view_name, period_number, metric_name, product_name)

# assign parameters for use case
if target_view_name.find('BRICK') != -1:  # use brick_hco_code for BRICK
    reporting_level_name = mkt_share.columns[0]
else:
    reporting_level_name = mkt_share.columns[3]  # use sales_reference_account_id for ACCOUNT
target = mktbskt.columns[-1]
target = mkt_share.columns[-1]

# convert sale_date to date
mkt_share['sale_date'] = pd.to_datetime(mkt_share['sale_date'], format='%Y-%m-%d')

# Important for MKTBSKT and MKT_SHARE subset data to market basket of interest  
mkt_share = mkt_share[mkt_share.marketbasket_name==marketbasket_name]

# call function to create columns for first and final deliveries 
mkt_share_first_final = first_final_purchase_date(mkt_share)

# cutoff values by reporting_level_name to ensure no values are counted in the model prior to the first purchase
mkt_share_first_final = mkt_share_first_final[mkt_share_first_final['sale_date'] >= mkt_share_first_final['first_delivery']]

# call function to subset data according to purchase requirements
mkt_share_time_flag = purchase_timing_requirement(mkt_share_first_final)

# call function to subset data according to  minimum purchase count requirement
mkt_share_clean = purchase_count_requirement(mkt_share_time_flag, 40)

# clean columns before model
mkt_share_clean = mkt_share_clean.loc[:,[reporting_level_name,'product_name','sale_date',target]]

# max_level = number of bricks_hco_codes feeding into the model
max_level = len(mkt_share_clean[reporting_level_name].unique())  

# call the model
mkt_share_model = mkt_share_clean.groupby(reporting_level_name).apply(anomaly_detect, .05, .25)  # higher alpha = lower metric percentages 
logger.info(f'Dimensions for data ingested from market share model = ({mkt_share_model.shape[0]}, {mkt_share_model.shape[1]})')

# reset index
mkt_share_model.reset_index(drop=True, inplace=True)

# adjust for negative scores
mkt_share_model_control = mkt_share_model.groupby(reporting_level_name).apply(control_scores)

# market share only--adjust for predicted scores over 100  
for i in range(len(mkt_share_model_control)):
    if mkt_share_model_control.iloc[i].Predicted > 100:
        mkt_share_model_control.iloc[i]['Predicted'] = 100 

# create data points for the model
mkt_share_score = create_data_points(mkt_share_model_control)

# create metrics for BI output
mkt_share_metric = create_metrics(mkt_share_score)

## create process output tables -----------------------------------------------------------
# create final output dataset and adjust for Marketbasket and Marketshare availibility 
if str(scd_out_param.target_view_name).find('MARKETBASKET') != -1:
    scd_post_process_output = pd.concat([vol_score, gap_model, mktbskt_score, mkt_share_score]) 
else:
    scd_post_process_output = pd.concat([vol_score, gap_model]) 

# rename and add columns to match post process output tables
scd_post_process_output.columns = ['reporting_level_value','current_week_date','scd_actual_value','scd_predicted_value','scd_name','scd_score','product_name']
scd_post_process_output['reporting_level_name'] = reporting_level_name.split("_", 1)[0].upper()
scd_post_process_output['scd_predicted_value'] = round(scd_post_process_output['scd_predicted_value'],2)
scd_post_process_output['product_name'] = product_name
scd_post_process_output = scd_post_process_output.loc[:,['scd_name','reporting_level_name','reporting_level_value','product_name','current_week_date','scd_actual_value','scd_predicted_value','scd_score']]

# create final metrics dataset and adjust for Marketbasket and Marketshare availibility 
if str(scd_out_param.target_view_name).find('MARKETBASKET') != -1:
    scd_post_process_bi_metric_output = pd.concat([vol_metric, gap_metric, mktbskt_metric, mkt_share_metric])
else:
    scd_post_process_bi_metric_output = pd.concat([vol_metric, gap_metric])

# rename and add columns to match bi metric output tables
scd_post_process_bi_metric_output.columns = ['current_week_date','scd_name','eligibility_percent','anomaly_threshold','reporting_level_count']
scd_post_process_bi_metric_output['reporting_level_name'] = reporting_level_name.split("_", 1)[0].upper()
scd_post_process_bi_metric_output['product_name'] = product_name
scd_post_process_bi_metric_output = scd_post_process_bi_metric_output.loc[:,['scd_name','reporting_level_name','product_name','current_week_date','eligibility_percent','anomaly_threshold','reporting_level_count']]



